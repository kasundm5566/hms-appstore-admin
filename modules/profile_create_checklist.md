## checklist for appstore-admin new profile creation

#### Add the profile to (AppstoreAdminBuild.scala)[#] file the name should be the same as the folder name you are going to create
* ProjectProfile - current active profile
* ProjectProfiles - all available profiles    
```     
val ProjectProfile   = "operator"
val ProjectProfiles  = Seq("default", "dialog-da", "dialog-live", "vodafone-da", "operator")
```

#### create the profile folder with operator name (copy files from operator profile and customize)

 * ex : if operator is 'stream' under profile folder you should have following files.
```
stream/
├── resources
│   ├── application.conf
│   ├── cas.properties
│   └── messages_en.properties
└── webapp
    ├── resources
    │   ├── css
    │   │   ├── ace.min.css
    │   │   └── hms-appstore-custom.css
    │   ├── font
    │   │   ├── VodafoneRg.eot
    │   │   ├── VodafoneRg.ttf
    │   │   └── VodafoneRg.woff
    │   ├── img
    │   │   ├── page-loader.gif
    │   │   ├── vf_logo_lrg.png
    │   │   └── vodafone_favicon.ico
    │   └── js
    │       └── appstore-admin-config.js
    └── WEB-INF
        ├── decorators
        │   ├── layout.jsp
        │   └── sidebar.jsp
        └── web.xml
```

#### style guide

###### if you are started from [operator](#) profile using a text editor or idea you can search and replace following color codes
* profiled css files are ace.min.css , hms-appstore-custom.css
```
Primary Color: #2ecc71
Primary Hover Color: #27ae60

Secondary Color (Buttons) - #A7CF3B
Secondary Hover Color: #93B732
```
###### otherwise please change following properties as necessary (most of the changes are covered below)

* top-navbar color
```
ace.min.css#1192
.navbar .navbar-inner {}
```

* top-table header bar
```
ace.min.css#6183
.table-header{}
hms-appstore-custom.css#588
.ace-nav > li:hover{}
```

* search button
```
hms-appstore-custom.css#593
form.form-search button.hmsSearchButton{}
hms-appstore-custom.css#601
form.form-search button.hmsSearchButton:hover, form.form-search button.hmsSearchButton:active, form.form-search button.hmsSearchButton:focus {}
```

* panel save button
```
hms-appstore-custom.css#570
.btn-danger#savePanels{}
hms-appstore-custom.css#574
.btn-danger#savePanels:hover{}
```

* left navigation
```
ace.min.css#1649
.nav-list > li > a:hover::before{}
```
