import SbtProfilePlugin.SbtProfileKeys._
import SbtProfilePlugin._
import com.github.siasia.PluginKeys._
import com.github.siasia.WebPlugin._
import repositories._
import sbt.Keys._
import sbt._


object AppstoreAdminBuild extends Build {
  val ProjectVersion   = "vdf-1.0.41-SNAPSHOT"
  val Organization     = "hms.appstore.web"
  val ProjectProfile   = "vodafone-da"
  val ProjectProfiles  = Seq("default", "dialog-da", "dialog-live", "vodafone-da", "vodafone-live", "robi-bd", "robi-bd-live", "stream", "vcity", "operator", "mtn")

  val ScalaVersion          = "2.10.0"
  val DiscoveryApiVersion   = "vdf-1.0.63"
  val SpringVersion         = "3.2.3.RELEASE"
  val SpringSecurityVersion = "3.0.5.RELEASE"
  val SalatVersion          = "1.9.2"
  val JettyVersion          = "8.1.13.v20130916"

  val TypesafeConfig  = "com.typesafe"  % "config"             % "1.0.0"
  val TypesafeLogging = "com.typesafe" %% "scalalogging-slf4j" % "1.0.1"

  val ServletApi      = "javax.servlet" % "javax.servlet-api" % "3.0.1" % "provided->default"
  val Slf4jLog4j      = "org.slf4j"     % "slf4j-log4j12"     % "1.7.2"
  val JclOverSlf4j    = "org.slf4j"     % "jcl-over-slf4j"    % "1.7.2"
  val Log4j           = "log4j"         % "log4j"             % "1.2.17"

  val DiscoveryClient = "hms.appstore.api" %% "discovery-client" % DiscoveryApiVersion

  val Subcut          = "com.escalatesoft.subcut" %% "subcut"     % "2.0"
  val Salat           = "com.novus"               %% "salat-core" % SalatVersion
  val SalatUtil       = "com.novus"               %% "salat-util" % SalatVersion
  val Casbah          = "org.mongodb"             %% "casbah"     % "2.5.1" pomOnly()

  val userAgentUtil= "eu.bitwalker" % "UserAgentUtils" % "1.14"

  val JasigCas             = "org.jasig.cas.client" % "cas-client-core"  % "3.2.1" excludeAll(ExclusionRule(organization = "javax.servlet"), ExclusionRule(organization = "commons-logging"))
  val HMSRegistrationApi   = "hms"                  % "registration-api" % "1.1.3" excludeAll(ExclusionRule(organization = "javax.servlet"), ExclusionRule(organization = "org.apache.geronimo.specs"))
  val HttpServletComponents= "hms.scala.http"       %% "http-servlet-components"    % "1.0.0"

  val SpringCore           = "org.springframework" % "spring-core"            % SpringVersion excludeAll(ExclusionRule(organization = "commons-logging"))
  val SpringExpression     = "org.springframework" % "spring-expression"      % SpringVersion excludeAll(ExclusionRule(organization = "commons-logging"))
  val SpringBeans          = "org.springframework" % "spring-beans"           % SpringVersion excludeAll(ExclusionRule(organization = "commons-logging"))
  val SpringAop            = "org.springframework" % "spring-aop"             % SpringVersion excludeAll(ExclusionRule(organization = "commons-logging"))
  val SpringContext        = "org.springframework" % "spring-context"         % SpringVersion excludeAll(ExclusionRule(organization = "commons-logging"))
  val SpringContextSupport = "org.springframework" % "spring-context-support" % SpringVersion excludeAll(ExclusionRule(organization = "commons-logging"))
  val SpringOxm            = "org.springframework" % "spring-oxm"             % SpringVersion excludeAll(ExclusionRule(organization = "commons-logging"))
  val SpringWeb            = "org.springframework" % "spring-web"             % SpringVersion excludeAll(ExclusionRule(organization = "commons-logging"))
  val SpringMvc            = "org.springframework" % "spring-webmvc"          % SpringVersion excludeAll(ExclusionRule(organization = "commons-logging"))
  val SpringTest           = "org.springframework" % "spring-test"            % SpringVersion excludeAll(ExclusionRule(organization = "commons-logging"))

  val SpringSecurityCore   = "org.springframework.security" % "spring-security-core"       % SpringSecurityVersion excludeAll(ExclusionRule(organization = "commons-logging"))
  val SpringSecurityWeb    = "org.springframework.security" % "spring-security-web"        % SpringSecurityVersion excludeAll(ExclusionRule(organization = "commons-logging"))
  val SpringSecurityConfig = "org.springframework.security" % "spring-security-config"     % SpringSecurityVersion excludeAll(ExclusionRule(organization = "commons-logging"))
  val SpringCasClient      = "org.springframework.security" % "spring-security-cas-client" % SpringSecurityVersion excludeAll(ExclusionRule(organization = "commons-logging"))

  val SiteMesh             = "org.sitemesh" % "sitemesh" % "3.0-alpha-2"
  val Jstl                 = "jstl" % "jstl" % "1.2"

  val JettyWebApp          = "org.eclipse.jetty" % "jetty-webapp" % JettyVersion % "container"
  val JettyWebAppContainer = "org.eclipse.jetty" % "jetty-webapp" % JettyVersion % "container"
  val JettyJspContainer    = "org.eclipse.jetty" % "jetty-jsp"    % JettyVersion % "container" excludeAll(ExclusionRule(organization = "org.slf4j"))
  val jettyContainer       = "org.eclipse.jetty" % "jetty-webapp" % JettyVersion % "container"
  val jettyOrbit           = "org.eclipse.jetty.orbit" % "javax.servlet" % "3.0.0.v201112011016" % "container,compile" artifacts Artifact("javax.servlet", "jar", "jar")

  val Jettison             = "org.codehaus.jettison" % "jettison"         % "1.3.4"
  val HmsRestCommon        = "hms.common"            % "rest-util"        % "1.0.6"
  val CommonRegApi         = "hms"                   % "user-service"     % "1.0.2"
  val HmsCommonUtil        = "hms.common"            % "hms-common-util"  % "1.0.6"
  val commonsFileUpload    = "commons-fileupload" % "commons-fileupload" % "1.3.3"

  val jettyConf = config("container")

  val jettyPluginSettings = Seq(
    libraryDependencies ++= Seq(
      JettyWebAppContainer,
      JettyJspContainer,
      commonsFileUpload,
      jettyOrbit
    ),
    port in jettyConf := 8080
  )


  val adminWebDependencies = Seq(
      SpringCore,
      SpringExpression,
      SpringBeans,
      SpringAop,
      SpringContext,
      SpringContextSupport,
      SpringOxm,
      SpringWeb,
      SpringMvc,
      SpringTest,
      SpringSecurityCore,
      SpringSecurityWeb,
      SpringSecurityConfig,
      SiteMesh,
      Jstl,
      ServletApi,
      Subcut,
      Casbah,
      Salat,
      SalatUtil,
      Slf4jLog4j,
      JclOverSlf4j,
      Log4j,
      TypesafeLogging,
      DiscoveryClient,
      HttpServletComponents,
      CommonRegApi,
      HmsRestCommon,
      HmsCommonUtil,
      SpringCasClient,
      Jettison,
      userAgentUtil
  )

  val testDependencies = Seq(
    "org.scalamock" %% "scalamock-specs2-support" % "3.0.1" % "test",
    "hms.specs" %% "specs-matchers" % "0.1.0" % "test"
  )

  val IvyCredentialFile = if (ProjectVersion.endsWith("SNAPSHOT")) {
    Path.userHome / ".ivy2" / ".credentials-snapshot"
  } else {
    Path.userHome / ".ivy2" / ".credentials-release"
  }

  val moduleLookupConfigurations = DefaultResolver.moduleConfig

  val excludedFilesInJar: NameFilter = (s: String) => """(.*?)\.(properties|props|conf|dsl|txt|xml)$""".r.pattern.matcher(s).matches

  lazy val baseSettings = {
    Defaults.defaultSettings ++ profileSettings ++ Seq(
      version := ProjectVersion,
      organization := Organization,
      scalaVersion := ScalaVersion,
      scalacOptions += "-deprecation",
      scalacOptions += "-unchecked",
      moduleConfigurations ++= moduleLookupConfigurations,
      publishTo <<= (version) {
        version: String =>
          val repo = "http://archiva.hsenidmobile.com/repository/"
          if (version.trim.endsWith("SNAPSHOT"))
            Some("Archiva Managed snapshots Repository" at repo + "snapshots/")
          else
            Some("Archiva Managed internal Repository" at repo + "internal/")
      },
      credentials += Credentials(IvyCredentialFile),
      logBuffered := false,
      parallelExecution in Test := false,
      buildProfile := ProjectProfile,
      buildProfiles := ProjectProfiles,
      classpathTypes += "orbit"
    )
  }

  lazy val modules = Project(id = "appstore-admin", base = file("."),
    settings = baseSettings ++ Seq(
      name := "appstore-admin"
    )
  ) aggregate(adminWeb)


  lazy val adminWeb = Project(id = "admin-web", base = file("admin-web"),
    settings = baseSettings ++ webSettings ++ jettyPluginSettings ++ Seq(
      name := "admin-web",
      artifactName := {
        (config: ScalaVersion, module: ModuleID, artifact: Artifact) => "appstore-admin" + "." + "war"
      },
      libraryDependencies ++= adminWebDependencies,
      libraryDependencies ++= testDependencies,
      webappResources in Compile <<= (webappResources in Compile, baseDirectory, buildProfile) {
        (wrs, bd, p) => {
          val resourceDir = bd / "src" / "main" / "profile" / p / "webapp"
          if (resourceDir.exists) (wrs ++ Seq(resourceDir)).reverse else wrs
        }
      }

    )

  )
}


