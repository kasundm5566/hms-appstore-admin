package hms.appstore.admin.controller

import com.escalatesoft.subcut.inject.Injectable
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{ResponseBody, RequestMethod, RequestMapping}
import org.springframework.ui.ModelMap
import javax.servlet.http.{HttpServletResponse, HttpServletRequest}
import org.slf4j.LoggerFactory

import hms.appstore.api.json._
import hms.appstore.api.client.domain._
import hms.appstore.api.client.DiscoveryInternalService
import hms.appstore.admin.service.{WebConfig, ServiceModule}


import scala.concurrent.{Await, ExecutionContext}
import scala.collection.JavaConverters._
import com.typesafe.scalalogging.slf4j.Logging
import hms.appstore.admin.util.{AuditLogStatus, AuditLogging}

@Controller
class AdUrlConfigController extends Injectable with WebConfig with Logging with AuditLogging{

  val bindingModule = ServiceModule

  private val discoveryInternalService = inject[DiscoveryInternalService]
  private implicit val executionContext = inject[ExecutionContext]

  @RequestMapping(value = Array("/advertisements"), method = Array(RequestMethod.GET))
  def toAdvertisements(modelMap: ModelMap, request: HttpServletRequest): String = {

    onPageAccess(request,"Advertisements",AuditLogStatus.SUCCESS)

    val pendingCountFuture = discoveryInternalService.countPendingApps()
    val pendingCountResult = Await.result(pendingCountFuture, discoveryApiTimeOut)
    val pendingCount = pendingCountResult.getResult.count
    val addURLFuture = discoveryInternalService.findAdUrls
    val addURLResults = Await.result(addURLFuture, discoveryApiTimeOut)
    val addURLs = addURLResults.getResults.map(AdUrlView(_)).toList

    logger.debug(s"$addURLs")

    modelMap.addAllAttributes(
      Map(
        "pendingCount" -> pendingCount,
        "addURLS" -> addURLs.asJava,
        "userId" -> UserDetails.getUserId(request)
      ).asJava
    )
    "advertisements"
  }

  @ResponseBody
  @RequestMapping(value = Array("/advertisements"), method = Array(RequestMethod.POST))
  def updateAdvertisements(request: HttpServletRequest, response: HttpServletResponse) = {

    val prefixes = List("first", "second", "third")
    val updateList = for {prefix <- prefixes} yield getAddUrl(prefix, request)
    val statusFuture = discoveryInternalService.createOrUpdateAdUrls(updateList)
    val status = Await.result(statusFuture, discoveryApiTimeOut)
    status.status
  }

  def getAddUrl(prefix: String, request: HttpServletRequest): AdUrl = {
    AdUrl(
      request.getParameter(prefix + "_id").trim,
      request.getParameter(prefix + "_name").trim,
      request.getParameter(prefix + "_image_url").trim,
      request.getParameter(prefix + "_target_url").trim
    )
  }

}
