package hms.appstore.admin.controller

import java.nio.file.{Files, Paths}
import javax.servlet.http.{HttpServletRequest, HttpServletResponse}

import com.escalatesoft.subcut.inject.Injectable
import hms.appstore.admin.service._
import hms.appstore.admin.util.AuditLogging
import hms.appstore.api.client.domain.{ApplicationView, CategoryView}
import hms.appstore.api.client.{DiscoveryInternalService, DiscoveryService}
import hms.appstore.api.json._
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Controller
import org.springframework.ui.{Model, ModelMap}
import org.springframework.web.bind.annotation.{RequestMapping, RequestMethod, RequestParam, ResponseBody}
import org.springframework.web.multipart.MultipartFile

import scala.collection.JavaConverters._
import scala.concurrent.{Await, ExecutionContext}
import java.net.URLDecoder
import hms.appstore.api.client.util.URLEncoder
import javax.ws.rs.QueryParam

//import com.typesafe.scalalogging.slf4j.Logging


@Controller
class CategoriesController extends Injectable with WebConfig with AuditLogging {

  val bindingModule = ServiceModule

  //  private var path = "/home/kausik/hmsProjects/appstore-admin/modules/admin-web/src/main/webapp/resources/img/"

  private var path = "/hms/data/discovery-api/images/category/"

  private val discoveryService = inject[DiscoveryService]
  private val discoveryInternalService = inject[DiscoveryInternalService]
  private lazy val logger = LoggerFactory.getLogger("CategoryController")

  private implicit val executionContext = inject[ExecutionContext]

  @RequestMapping(value = Array("/category"), method = Array(RequestMethod.GET))
  def Categories(model: Model, request: HttpServletRequest): String = {
    val categoriesFuture = discoveryService.categories()
    val pendingCountFuture = discoveryInternalService.countPendingApps()
    val categoryPanel = "Category"
    val result = for {
      categories <- categoriesFuture
      pendingCount <- pendingCountFuture
    } yield (categories.getResults.map(CategoryView(_)), pendingCount.getResult.count)

    val (categories, pendingCount) = Await.result(result, discoveryApiTimeOut)

    model.addAllAttributes(
      Map(
        "categories" -> categories.asJava,
        "pendingCount" -> pendingCount,
        "iconWidth" -> categoryIconWidth,
        "iconHeight" -> categoryIconHeight,
        "iconSize" -> categoryIconSize,
        "categoryPanel" -> categoryPanel,
        "userId" -> UserDetails.getUserId(request)
      ).asJava
    )
    "category"
  }

  @ResponseBody
  @RequestMapping(value = Array("/editCategory"), method = Array(RequestMethod.POST))
  def categoriesEdit(model: Model, modelMap: ModelMap, request: HttpServletRequest, response: HttpServletResponse, @RequestParam multipartFile: MultipartFile): String = {
    val id = request.getParameter("category_id")
    val name = request.getParameter("name")
    val description = request.getParameter("cat-description")
    val imageUrl = request.getParameter("image_url")
    val imageUrlDel = request.getParameter("image_url_del")

    import java.io.File
    val file = new File(imageUrlDel)
    logger.debug("Editing category icon name is  : " + file.getName)
    logger.debug("Adding category icon name is  :  " + imageUrl)
    logger.debug("delete Image URL is : " + imageUrlDel)

    if (imageUrl != file.getName && imageUrl == multipartFile.getOriginalFilename) {
      if (new java.io.File(path + file.getName).exists) {
        Files.deleteIfExists(Paths.get(path + file.getName))
      }
    }

    import java.nio.file.{Files, Paths, StandardCopyOption}
    if (multipartFile.getOriginalFilename == imageUrl) {
      if (!multipartFile.isEmpty) {
        val fileName = multipartFile.getOriginalFilename
        val is = multipartFile.getInputStream
        Files.copy(is, Paths.get(path + fileName.replaceAll(" ", "")), StandardCopyOption.REPLACE_EXISTING)
      }

    }
    val resp = discoveryInternalService.editCategory(id = URLEncoder.encode(id), CategoryEditReq(
      name = name,
      Option(description),
      Option(imageUrl.replaceAll(" ", ""))
    ))

    val result = Await.result(resp, discoveryApiTimeOut)
    logger.debug("Category field of  " + id + " is edited Successfully")
    result.status
  }

  @ResponseBody
  @RequestMapping(value = Array("/deleteCategory"), method = Array(RequestMethod.POST))
  def categoriesDelete(model: Model, modelMap: ModelMap, request: HttpServletRequest) = {

    val id = request.getParameter("category_id")
    val imageUrl = request.getParameter("image_url")

    import java.io.File
    val file = new File(imageUrl)
    logger.debug("Deleting category icon name is : " + file.getName)

    if (new java.io.File(path + file.getName).exists) {
      Files.deleteIfExists(Paths.get(path + file.getName))
    }

    val resp = discoveryInternalService.removeCategory(URLEncoder.encode(id))
    val result = Await.result(resp, discoveryApiTimeOut)
    logger.debug("Category field of  " + id + " is deleted Successfully")
    result.status
  }


  @RequestMapping(value = Array("/categoryApps"), method = Array(RequestMethod.GET))
  def appStoreFeaturedApps(modelMap: ModelMap, response: HttpServletResponse, request: HttpServletRequest): String = {
    val category = Option(request.getParameter("category")).getOrElse("")
    val appsFuture = discoveryService.appsByCategoryWithoutSorting(category)
    val pendingCountFuture = discoveryInternalService.countPendingApps()
    val categoryAddAppsPanel = "CategoryAddApps"

    val (apps, pendingCount) =
      Await.result(
        for {
          appsResult <- appsFuture
          pendingCount <- pendingCountFuture
        } yield (appsResult.getResults, pendingCount.getResult.count),
        discoveryApiTimeOut
      )

    modelMap.addAttribute("appList", apps.map(ApplicationView(_)).asJava)
    modelMap.addAttribute("category", category)
    modelMap.addAttribute("pendingCount", pendingCount)
    modelMap.addAttribute("userId", UserDetails.getUserId(request))
    modelMap.addAttribute("imageBasePath", imageBasePath)
    modelMap.addAttribute("categoryAddAppsPanel", categoryAddAppsPanel)
    "category-apps"
  }

  @RequestMapping(value = Array("/addAppsToCategory"), method = Array(RequestMethod.POST))
  @ResponseBody
  def addAppsToCategory(@RequestParam(value = "category") category: String, @RequestParam(value = "appIds[]") appIds: Array[String]) = {
    val req = AddOrRemoveAppsOfCategoryReq(appIds.toList)
    val resp = discoveryInternalService.addOrRemoveAppsOfCategory(URLEncoder.encode(URLDecoder.decode(category, "UTF-8")), req)
    val result = Await.result(resp, discoveryApiTimeOut)
    logger.debug("Apps added to the category: " + URLDecoder.decode(category, "UTF-8"))
    result.status
  }


  @ResponseBody
  @RequestMapping(value = Array("/addCategory"), method = Array(RequestMethod.POST))
  def addCategory(request: HttpServletRequest, @RequestParam multipartFile: MultipartFile): String = {
    val name = request.getParameter("name")
    val description = request.getParameter("description")
    val image_url = request.getParameter("image_url")


    import java.nio.file.{Files, Paths}

    if (!new java.io.File(path).exists) {
      Files.createDirectories(Paths.get(path))
    }

    import java.nio.file.{Files, Paths, StandardCopyOption}

    val fileName = multipartFile.getOriginalFilename
    logger.debug("Adding category icon name is : " + fileName)
    val is = multipartFile.getInputStream
    Files.copy(is, Paths.get(path + fileName.replaceAll(" ", "")), StandardCopyOption.REPLACE_EXISTING)

    val resp = discoveryInternalService.addCategory(CategoryAddReq(
      name = name,
      description = description,
      fileName.replaceAll(" ", "")
    ))

    val result = Await.result(resp, discoveryApiTimeOut)
    logger.debug("Category field of  " + name + " is added Successfully")
    result.status

  }

}

