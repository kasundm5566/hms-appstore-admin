/*
 * (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.admin.controller

import com.escalatesoft.subcut.inject.Injectable

import hms.appstore.api.client.{DiscoveryService, DiscoveryInternalService}
import hms.appstore.api.client.domain.ApplicationView
import hms.appstore.api.json.{AppStatus, SearchQuery}
import hms.appstore.admin.util.{AuditLogStatus, AuditLogging, WebUtil}
import hms.appstore.admin.service._

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{RequestMethod, RequestMapping}
import org.springframework.ui.Model
import org.slf4j.LoggerFactory

import scala.collection.JavaConverters._
import concurrent.{Await, ExecutionContext}
import javax.servlet.http.{HttpServletResponse, HttpServletRequest}
import com.typesafe.scalalogging.slf4j.Logging


@Controller
class HomeController extends Injectable with WebConfig with Logging with AuditLogging{

  val bindingModule = ServiceModule

  private val discoveryInternalService = inject[DiscoveryInternalService]
  private val discoveryService = inject[DiscoveryService]


  private implicit val executionContext = inject[ExecutionContext]

  private val searchStatues = List("Any", "Published", "Unpublished", "New")

  @RequestMapping(value = Array("/"), method = Array(RequestMethod.GET))
  def adminHome(model: Model, request: HttpServletRequest): String = {

    onPageAccess(request,"Home Page",AuditLogStatus.SUCCESS)
    val start = Option(request.getParameter("skip")).getOrElse("0").toInt
    val limit = Option(request.getParameter("limit")).getOrElse(numberOfResultPerPage).toString.toInt

    logger.debug(s"HomeController | getting applications | start $start limit $limit")

    val appsFuture = discoveryInternalService.findAllApps(start = start, limit = limit)
    val categoriesFuture = discoveryService.categories()
    val pendingCountFuture = discoveryInternalService.countPendingApps()
    val allAppsCountFuture = discoveryInternalService.countAllApps()

    val result = for {
      apps <- appsFuture
      categories <- categoriesFuture
      pendingCount <- pendingCountFuture
      allAppCount <- allAppsCountFuture
    } yield (apps.getResults.map(ApplicationView(_)), categories.getResults.map(_.name), pendingCount.getResult.count, allAppCount.getResult.count)

    val (allApps, categories, pendingCount, allAppsCount) = Await.result(result, discoveryApiTimeOut)
    val activePage = (start / numberOfResultPerPage)
    val urlObjList = WebUtil.getPaginationUrls("appstore-admin", allAppsCount.toInt)

    model.addAllAttributes(
      Map(
        "skip" -> start,
        "userId" -> UserDetails.getUserId(request),
        "urlList" -> urlObjList.asJava,
        "appList" -> allApps.asJava,
        "pageSize" -> numberOfResultPerPage,
        "categories" -> categories.asJava,
        "activePage" -> activePage,
        "pendingCount" -> pendingCount,
        "searchStatuses" -> searchStatues.asJava,
        "pageType" -> "All"
      ).asJava
    )
    "index"
  }

  private def resolveOptionParam(p: String) = {
    logger.debug("resolving parameter [{}]",p)
    p match {
      case "" => None
      case _ => Option(p)
    }
  }

  @RequestMapping(value = Array("/search"))
  def homeSearch(modelMap: Model, request: HttpServletRequest, response: HttpServletResponse): String = {

    onPageAccess(request,"Application search",AuditLogStatus.SUCCESS)

    def getRequestParameters(request: HttpServletRequest) = {
      val searchSp = resolveOptionParam(request.getParameter("search-sp"))
      val searchId = resolveOptionParam(request.getParameter("search-id"))
      val searchType = resolveOptionParam(request.getParameter("search-type"))
      val searchName = resolveOptionParam(request.getParameter("search-name"))
      val searchStatus = resolveOptionParam(Option(request.getParameter("search-status")).getOrElse(""))
      val checkFunJokeCat = Option(request.getParameter("search-category")).getOrElse("")
      val searchCategory = resolveOptionParam(if(checkFunJokeCat.contains("Fun")){"Fun & Jokes"}
                           else{checkFunJokeCat})

      val status = searchStatus match {
        case Some("Unpublished") => Some(AppStatus.UnPublished)
        case Some("Published") => Some(AppStatus.Published)
        case Some("New") => Some(AppStatus.New)
        case _ => None
      }

      val start = Option(request.getParameter("skip")).getOrElse("0").toInt
      val limit = Option(request.getParameter("limit")).getOrElse(numberOfResultPerPage).toString.toInt

      (searchName, searchCategory, searchStatus, status, searchId, searchType, searchSp, start, limit)
    }

    def getDiscoveryApiResults(query: SearchQuery) = {

      val discoveryApiResults = for {
        appResults <- discoveryInternalService.searchApps(query)
        categories <- discoveryService.categories()
        appResultsCount <- discoveryInternalService.count(query)
        pendingCountResult <- discoveryInternalService.countPendingApps()
      } yield (appResults.getResults.map(ApplicationView(_)), categories.getResults.map(_.name), appResultsCount.getResult.count, pendingCountResult.getResult.count)
      Await.result(discoveryApiResults, discoveryApiTimeOut)

    }

    val (searchName, searchCategory, searchStatus, status, searchId, searchType, serviceProvider, start, limit) = getRequestParameters(request) //request parameters

    val query = SearchQuery(
      searchText = searchName,
      start = start,
      limit = limit,
      status = status,
      appId = searchId,
      appType = searchType,
      category = searchCategory,
      spName = serviceProvider
    )

    logger.debug("searching applications [{}]",query)

    val (allApps, categories, allAppsCount, pendingCount) = getDiscoveryApiResults(query) // discovery api results
    val urlObjList = WebUtil.getPaginationUrlsForSearching("search", allAppsCount.toInt - 1, searchName.getOrElse(""), searchCategory.getOrElse(""), searchStatus.getOrElse("")) //paging urls

    val activePage = (start / numberOfResultPerPage)

    modelMap.addAllAttributes(
      Map(
        "skip" -> start,
        "userId" -> UserDetails.getUserId(request),
        "urlList" -> urlObjList.asJava,
        "appList" -> allApps.sortBy(a => a.getRequestedDate).reverse.asJava,
        "pageSize" -> numberOfResultPerPage,
        "searchKw" -> searchName.orNull,
        "activePage" -> activePage,
        "categories" -> categories.asJava,
        "pendingCount" -> pendingCount,
        "searchStatuses" -> searchStatues.asJava,
        "searchFilterId" -> searchId.orNull,
        "searchFilterType" -> searchType.orNull,
        "searchFilterSp" -> serviceProvider.orNull,
        "searchFilterCat" -> searchCategory.orNull,
        "searchFilterStatus" -> searchStatus.orNull
      ).asJava
    )

    "index"
  }

  @RequestMapping(value = Array("/login"), method = Array(RequestMethod.GET))
  def login(request: HttpServletRequest, response: HttpServletResponse) {
    onAuthentication(request,"Login",AuditLogStatus.SUCCESS)
    logger.debug(s"HomeController | signing in")
    response.sendRedirect(casLoginUrl)
  }

  @RequestMapping(value = Array("/logout"), method = Array(RequestMethod.GET))
  def logout(request: HttpServletRequest, response: HttpServletResponse) {
    onAuthentication(request,"Logout",AuditLogStatus.SUCCESS)
    logger.debug(s"HomeController | signing out")
    response.sendRedirect(casLogoutUrl)
  }

  @RequestMapping(value = Array("/casfailedError"), method = Array(RequestMethod.GET))
  def casFailedError(request: HttpServletRequest, response: HttpServletResponse) = {
    onAuthentication(request,"Authentication failed",AuditLogStatus.FAILED)
    "casfailed-error"
  }
}
