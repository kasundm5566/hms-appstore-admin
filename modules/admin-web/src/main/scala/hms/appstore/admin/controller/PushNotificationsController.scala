package hms.appstore.admin.controller

import org.springframework.stereotype.Controller
import com.escalatesoft.subcut.inject.{BindingModule, Injectable}
import hms.appstore.admin.service.{ServiceModule, WebConfig}
import hms.appstore.admin.util.{AuditLogStatus, AuditLogging}
import hms.appstore.api.client.DiscoveryInternalService
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.{ResponseBody, RequestMapping, RequestMethod}
import org.springframework.ui.{Model, ModelMap}
import javax.servlet.http.HttpServletRequest
import scala.collection.JavaConverters._
import scala.concurrent.{ExecutionContext, Await}
import hms.appstore.api.client.domain.ApplicationView
import hms.appstore.api.json.{Recipient, NotifyReq}

/**
 * Created by kasun on 6/21/18.
 */

@Controller
class PushNotificationsController extends Injectable with WebConfig with AuditLogging {
  val bindingModule = ServiceModule

  private val discoveryInternalService = inject[DiscoveryInternalService]
  private lazy val logger = LoggerFactory.getLogger("PushNotificationsController")
  private implicit val executionContext = inject[ExecutionContext]

  @RequestMapping(value = Array("/push-notifications"), method = Array(RequestMethod.GET))
  def pushNotifications(model: Model, request: HttpServletRequest): String = {
    val downloadableAppsFuture = discoveryInternalService.findAllDownloadableApps(start = 0, limit = Int.MaxValue)
    val pendingCountFuture = discoveryInternalService.countPendingApps()
    val pushNotificationsPanel = "Push-notifications"

    val result = for {
      apps <- downloadableAppsFuture
      pendingCount <- pendingCountFuture
    } yield (apps.getResults.map(ApplicationView(_)), pendingCount.getResult.count)

    val (allApps, pendingCount) = Await.result(result, discoveryApiTimeOut)

    model.addAllAttributes(
      Map(
        "appList" -> allApps.asJava,
        "pendingCount" -> pendingCount,
        "userId" -> UserDetails.getUserId(request),
        "pushNotificationsPanel" -> pushNotificationsPanel,
        "notificationMessageTitleCharacterLimit" -> notificationMessageTitleCharacterLimit,
        "notificationMessageBodyCharacterLimit" -> notificationMessageBodyCharacterLimit
      ).asJava
    )

    "push-notifications"
  }

  @RequestMapping(value = Array("/sendPushNotification"), method = Array(RequestMethod.POST))
  @ResponseBody
  def sendPushNotification(model: Model, request: HttpServletRequest) = {
    val notificationMessageTitle = request.getParameter("notificationMessageTitle")
    val notificationMessage = request.getParameter("notificationMessage")
    val notificationType = request.getParameter("notificationType")
    val requestId = System.currentTimeMillis().toString

    if (notificationType == "All") {
      val recipient = Recipient(notificationType, None)
      val resp = discoveryInternalService.notifyAll(NotifyReq(recipient, notificationMessageTitle, notificationMessage, requestId, UserDetails.getUserId(request), PushNotificationDispatchType.NEW.toString))
      val result = Await.result(resp, discoveryApiTimeOut)
      result.status
    } else if (notificationType == "AppSpecific") {
      val selectedApps = request.getParameter("selectedApps")
      val selectedAppsList = selectedApps.split(",").toList
      val selectedAppsCount = selectedAppsList.size
      var successCalls = 0
      selectedAppsList.foreach(app => {
        val recipient = Recipient(notificationType, Some(app))
        val resp = discoveryInternalService.notifyAppSpecific(NotifyReq(recipient, notificationMessageTitle, notificationMessage, requestId, UserDetails.getUserId(request), PushNotificationDispatchType.NEW.toString))
        val result = Await.result(resp, discoveryApiTimeOut)
        if (result.statusCode == "S1000") {
          successCalls += 1
        }
      })
      if (successCalls == selectedAppsCount) {
        "S1000"
      } else {
        "Error"
      }
    }
  }
}
