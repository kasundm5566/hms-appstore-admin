package hms.appstore.admin.controller

/**
 * Created by kasun on 8/16/18.
 */
object PushNotificationDispatchType extends Enumeration {
  type PushNotificationDispatchType = Value
  val NEW = Value("new")
  val LEGACY = Value("legacy")
}
