package hms.appstore.admin.util

import org.slf4j.LoggerFactory
import javax.servlet.http.HttpServletRequest
import hms.appstore.admin.service.WebConfig
import hms.appstore.api.json.ParentCategory
import org.joda.time.format.DateTimeFormat
import org.joda.time.DateTime
import javax.ws.rs.core.HttpHeaders
import _root_.eu.bitwalker.useragentutils.UserAgent


trait AuditLogging extends WebConfig {
  private lazy val auditLogger = LoggerFactory.getLogger("audit")

    private def getUserName(req: HttpServletRequest) = Option(req.getUserPrincipal).map(_.getName).getOrElse("")
  private val formatter=DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss,SSS")

  private def auditLogCommonInfo(request: HttpServletRequest) = {
    val userAgent: UserAgent = UserAgent.parseUserAgentString(request.getHeader(HttpHeaders.USER_AGENT))
    Map(
      "userName" -> getUserName(request),
      "msisdn" -> AuditLogStatus.NOT_AVAILABLE,
      "ip" -> request.getHeader("X-Forwarded-For").split(",").last,
      "bType" -> userAgent.getBrowser.getName,
      "bVersion" -> userAgent.getBrowserVersion.getMajorVersion,
      "sessionID" -> request.getSession.getId,
      "module" -> module,
      "timeStamp" ->formatter.print(new DateTime())
    )
  }

  private def getValue(v: Option[Any]):String = v match {
    case item:Some[String]=> item.getOrElse(AuditLogStatus.NOT_AVAILABLE)
    case _ => AuditLogStatus.NOT_AVAILABLE
  }

  protected def onApproved(httpRequest:HttpServletRequest, appId: String, status: String) {
    val info = auditLogCommonInfo(httpRequest)
    auditLogger.info("{}|{}|{}|{}|{}|{}|{}|{}|{}|{}|{}"
      , getValue(info.get("userName"))
      , getValue(info.get("msisdn"))
      , getValue(info.get("ip"))
      , getValue(info.get("bType"))
      , getValue(info.get("bVersion"))
      , getValue(info.get("sessionID"))
      , getValue(info.get("module"))
      , "Application APPROVE"
      , "App State Change PENDING => PUBLISHED"
      , getValue(info.get("timeStamp"))
      , status
    )
  }

  protected def onReject(httpRequest: HttpServletRequest, appId: String, status: String) {
    val info = auditLogCommonInfo(httpRequest)
    auditLogger.info("{}|{}|{}|{}|{}|{}|{}|{}|{}|{}|{}"
      , getValue(info.get("userName"))
      , getValue(info.get("msisdn"))
      , getValue(info.get("ip"))
      , getValue(info.get("bType"))
      , getValue(info.get("bVersion"))
      , getValue(info.get("sessionID"))
      , getValue(info.get("module"))
      , "Application Reject"
      , "Delete Pending approval application "
      , getValue(info.get("timeStamp"))
      , status
    )
  }

  protected def onPublished(httpRequest: HttpServletRequest, appId: String, status: String) {
    val info = auditLogCommonInfo(httpRequest)
    auditLogger.info("{}|{}|{}|{}|{}|{}|{}|{}|{}|{}|{}"
      , getValue(info.get("userName"))
      , getValue(info.get("msisdn"))
      , getValue(info.get("ip"))
      , getValue(info.get("bType"))
      , getValue(info.get("bVersion"))
      , getValue(info.get("sessionID"))
      , getValue(info.get("module"))
      , "Application PUBLISH"
      , "App State Change UNPUBLISHED => PUBLISHED"
      , getValue(info.get("timeStamp"))
      , status
    )
  }

  protected def onUnPublished(httpRequest: HttpServletRequest, appId: String, status: String) {
    val info = auditLogCommonInfo(httpRequest)
    auditLogger.info("{}|{}|{}|{}|{}|{}|{}|{}|{}|{}|{}"
      , getValue(info.get("userName"))
      , getValue(info.get("msisdn"))
      , getValue(info.get("ip"))
      , getValue(info.get("bType"))
      , getValue(info.get("bVersion"))
      , getValue(info.get("sessionID"))
      , getValue(info.get("module"))
      , "Application UNPUBLISHED"
      , "App State Change PUBLISHED => UNPUBLISHED"
      , getValue(info.get("timeStamp"))
      , status
    )
  }

  protected def onEdit(httpRequest: HttpServletRequest, appId: String, status: String) {
    val info = auditLogCommonInfo(httpRequest)
    auditLogger.info("{}|{}|{}|{}|{}|{}|{}|{}|{}|{}|{}"
      , getValue(info.get("userName"))
      , getValue(info.get("msisdn"))
      , getValue(info.get("ip"))
      , getValue(info.get("bType"))
      , getValue(info.get("bVersion"))
      , getValue(info.get("sessionID"))
      , getValue(info.get("module"))
      , "Application EDIT"
      , "Application details modified"
      , getValue(info.get("timeStamp"))
      , status
    )
  }

  protected def onFeaturedAppsEdit(httpRequest: HttpServletRequest, appId: String, status: String) {
    val info = auditLogCommonInfo(httpRequest)
    auditLogger.info("{}|{}|{}|{}|{}|{}|{}|{}|{}|{}|{}"
      , getValue(info.get("userName"))
      , getValue(info.get("msisdn"))
      , getValue(info.get("ip"))
      , getValue(info.get("bType"))
      , getValue(info.get("bVersion"))
      , getValue(info.get("sessionID"))
      , getValue(info.get("module"))
      , "Featured Application Selection"
      , "Update featured application list"
      , getValue(info.get("timeStamp"))
      , status
    )
  }

  protected def onPageAccess(httpRequest: HttpServletRequest, page: String, status: String) {
    val info = auditLogCommonInfo(httpRequest)
    auditLogger.info("{}|{}|{}|{}|{}|{}|{}|{}|{}|{}|{}"
      , getValue(info.get("userName"))
      , getValue(info.get("msisdn"))
      , getValue(info.get("ip"))
      , getValue(info.get("bType"))
      , getValue(info.get("bVersion"))
      , getValue(info.get("sessionID"))
      , getValue(info.get("module"))
      , "Page View"
      , page
      , getValue(info.get("timeStamp"))
      , status
    )
  }

  protected def onAuthentication(httpRequest: HttpServletRequest, action: String, status: String) {
    val info = auditLogCommonInfo(httpRequest)
    auditLogger.info("{}|{}|{}|{}|{}|{}|{}|{}|{}|{}|{}"
      , getValue(info.get("userName"))
      , getValue(info.get("msisdn"))
      , getValue(info.get("ip"))
      , getValue(info.get("bType"))
      , getValue(info.get("bVersion"))
      , getValue(info.get("sessionID"))
      , getValue(info.get("module"))
      , "Authentication"
      , action
      , getValue(info.get("timeStamp"))
      , status
    )
  }

  protected def onParentCategoryUpdate(httpRequest: HttpServletRequest, panels: List[ParentCategory], status: String) {
    val info = auditLogCommonInfo(httpRequest)
    auditLogger.info("{}|{}|{}|{}|{}|{}|{}|{}|{}|{}|{}"
      , getValue(info.get("userName"))
      , getValue(info.get("msisdn"))
      , getValue(info.get("ip"))
      , getValue(info.get("bType"))
      , getValue(info.get("bVersion"))
      , getValue(info.get("sessionID"))
      , getValue(info.get("module"))
      , "Panel Selection"
      , "Chang app panels"
      , getValue(info.get("timeStamp"))
      , status
    )
  }

}
object AuditLogStatus {
  val TRUE = "True"
  val FALSE = "False"
  val FAILED = "Failed"
  val SUCCESS = "Success"
  val NOT_AVAILABLE = ""

  def sCode2AuditStatus(statusCode: String) = statusCode match {
    case "S1000" => AuditLogStatus.SUCCESS
    case _ => AuditLogStatus.FAILED
  }
}
