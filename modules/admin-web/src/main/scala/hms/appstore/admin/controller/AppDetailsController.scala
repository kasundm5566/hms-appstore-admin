package hms.appstore.admin.controller

import com.escalatesoft.subcut.inject.Injectable
import hms.appstore.admin.service.{WebConfig, ServiceModule}
import hms.appstore.api.client.{DiscoveryService, DiscoveryInternalService}
import hms.appstore.api.client.domain.{ChargingDescriptionMessageView, ChargingDataView, ApplicationView}

import org.springframework.stereotype.Controller
import org.springframework.ui.ModelMap
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.slf4j.LoggerFactory

import scala.collection.JavaConverters._
import scala.concurrent.ExecutionContext
import scala.concurrent.Await
import javax.servlet.http.HttpServletRequest
import hms.appstore.admin.util.{AuditLogStatus, AuditLogging}


@Controller
class AppDetailsController extends Injectable with WebConfig with AuditLogging {

  val bindingModule = ServiceModule
  private lazy val logger = LoggerFactory.getLogger("AppDetailsController")
  private val discoveryService = inject[DiscoveryService]
  private val discoveryInternalService = inject[DiscoveryInternalService]

  private implicit val executionContext = inject[ExecutionContext]

  @RequestMapping(value = Array("/appView"), method = Array(RequestMethod.GET))
  def appDetailsView(modelMap: ModelMap, request: HttpServletRequest): String = {

    onPageAccess(request, "Application details", AuditLogStatus.SUCCESS)

    val appId = Option(request.getParameter("app-id")).getOrElse("")
    val appResultFuture = discoveryInternalService.findApp(appId)
    val appResult = Await.result(appResultFuture, discoveryApiTimeOut)
    if (appResult.isSuccess) {
      modelMap.addAttribute("app", ApplicationView(appResult.getResult))
      modelMap.addAttribute("appBannerFeatureEnable", appBannerFeatureEnable)
      "app-view"
    } else {
      logger.error("Error occurred while fetching the application details Error[{}]", appResult.description)
      "error-500"
    }
  }


  @RequestMapping(value = Array("/appEdit"), method = Array(RequestMethod.GET))
  def appDetailsEdit(modelMap: ModelMap, request: HttpServletRequest): String = {

    onPageAccess(request, "Application edit form", AuditLogStatus.SUCCESS)

    val appId = Option(request.getParameter("app-id")).getOrElse("")
    val appResultFuture = discoveryInternalService.findApp(appId)
    val categoriesFuture = discoveryService.categories()
    val appChargingDetailsFuture = discoveryInternalService.findAppChargingDetails(appId)
    val appChargingDescriptionMessageFormatsFuture = discoveryInternalService.findChargingDescriptionMessageFormats(appId)

    val (app, categories, chargingData, chargingMessageFormats) =
      Await.result(
        for {
          appsResult <- appResultFuture
          categoriesResults <- categoriesFuture
          chargingResults <- appChargingDetailsFuture
          chargingDescriptionMessageFormatsResults <- appChargingDescriptionMessageFormatsFuture
        } yield (appsResult.getResult, categoriesResults.getResults, chargingResults.getResults, chargingDescriptionMessageFormatsResults.getResults),
        discoveryApiTimeOut
      )

    modelMap.addAttribute("app", ApplicationView(app))
    modelMap.addAttribute("categories", categories.map(_.name).asJava)
    modelMap.addAttribute("chargingData", chargingData.map(ChargingDataView(_)).asJava)
    modelMap.addAttribute("chargingMessageFormats", chargingMessageFormats.map(ChargingDescriptionMessageView(_)).asJava)
    modelMap.addAttribute("appBannerFeatureEnable", appBannerFeatureEnable)
    "app-edit"
  }
}