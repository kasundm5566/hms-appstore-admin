package hms.appstore.admin.controller

import java.io.File
import javax.servlet.http.HttpServletRequest

import com.escalatesoft.subcut.inject.Injectable
import hms.appstore.admin.service._
import hms.appstore.admin.util.{AuditLogStatus, AuditLogging, WebUtil}
import hms.appstore.api.client.domain.{BannerLocationView, ApplicationView, BannerView}
import hms.appstore.api.client.{DiscoveryInternalService, DiscoveryService}
import hms.appstore.api.json._
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Controller
import org.springframework.ui.{Model, ModelMap}
import org.springframework.web.bind.annotation.{RequestMapping, RequestMethod, RequestParam, ResponseBody}
import org.springframework.web.multipart.MultipartFile

import scala.collection.JavaConverters._
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.collection.mutable.ListBuffer
import org.apache.commons.io.FilenameUtils


//import com.typesafe.scalalogging.slf4j.Logging


@Controller
class BannersController extends Injectable with WebConfig with AuditLogging {

  val bindingModule = ServiceModule

  //  private var path = "/home/kausik/hmsProjects/appstore-admin/modules/admin-web/src/main/webapp/resources/img/"

  private var path = "/hms/data/discovery-api/images/banners-"
  private val discoveryService = inject[DiscoveryService]
  private val discoveryInternalService = inject[DiscoveryInternalService]
  private lazy val logger = LoggerFactory.getLogger("BannersController")

  private val searchStatues = List("New")

  private implicit val executionContext = inject[ExecutionContext]

  @RequestMapping(value = Array("/banners"), method = Array(RequestMethod.GET))
  def Banners(model: Model, request: HttpServletRequest): String = {

    onPageAccess(request, "New application requests page", AuditLogStatus.SUCCESS)
    val start = Option(request.getParameter("skip")).getOrElse("0").toInt
    val limit = Option(request.getParameter("limit")).getOrElse(numberOfResultPerPage.toString).toInt
    var bannerPanel = "Banner"
    val appsFuture = discoveryInternalService.findPendingApps(start = start, limit = limit)
    val pendingCountFuture = discoveryInternalService.countPendingApps()
    val categoriesFuture = discoveryService.categories()

    var displayLocation = request.getParameter("display_location")
    request.setAttribute("display_location", displayLocation)

    if (displayLocation == null || displayLocation.isEmpty) {
      displayLocation = "all"
    }

    var bannersFuture: Future[QueryResults[Banner]] = null
    var countBannersFuture: Future[QueryResult[BannersCount]] = null
    if (displayLocation == "all") {
      bannersFuture = discoveryInternalService.getBanners(start = start, limit = limit)
      countBannersFuture = discoveryInternalService.getBannersCount()
    }
    else {
      bannersFuture = discoveryInternalService.getBannersByLocation(displayLocation, start = start, limit = limit)
      countBannersFuture = discoveryInternalService.getBannersCountByLocation(displayLocation)
    }

    val displayLocations = discoveryInternalService.getBannerLocations()


    val result = for {
      apps <- appsFuture
      countBanners <- countBannersFuture
      pendingCount <- pendingCountFuture
      categories <- categoriesFuture
      banners <- bannersFuture
      bannerDisplayLocations <- displayLocations
    } yield (apps.getResults.map(ApplicationView(_)), countBanners.getResult.count, banners.getResults.map(BannerView(_)), categories.getResults.map(_.name), pendingCount.getResult.count, bannerDisplayLocations.getResults.map(BannerLocationView(_)))


    val (pendingApps, bannersCount, banners, categories, pendingCount, bannerDisplay) = Await.result(result, discoveryApiTimeOut)
    val urlObjList = WebUtil.getPaginationUrls("appstore-admin/banners", bannersCount.toInt)
    val activePage = (start / numberOfResultPerPage)

    bannerDisplay.foreach(b => {

    });

    model.addAllAttributes(
      Map(
        "displayLocations" -> bannerDisplay.asJava,
        "displayLocation" -> displayLocation,
        "bannersCount" -> bannersCount,
        "banners" -> banners.asJava,
        "urlList" -> urlObjList.asJava,
        "activePage" -> activePage,
        "pendingCount" -> pendingCount,
        "skip" -> start,
        "pageSize" -> numberOfResultPerPage,
        "appList" -> pendingApps.asJava,
        "activePage" -> activePage,
        "categories" -> categories.asJava,
        "searchStatuses" -> searchStatues.asJava,
        "userId" -> UserDetails.getUserId(request),
        "bannerPanel" -> bannerPanel,
        "bannerImagesCharacterCount" -> bannerImageCharacterLimit
      ).asJava
    )

    "banners"
  }

  @ResponseBody
  @RequestMapping(value = Array("/addBanner"), method = Array(RequestMethod.POST))
  def addCategory(request: HttpServletRequest, @RequestParam multipartFile: MultipartFile): String = {
    val displayLocation = request.getParameter("display_location")
    val link = request.getParameter("link")
    val description = request.getParameter("description")
    val imageUrl = request.getParameter("image_url")

    import java.nio.file.{Files, Paths}

    val bannerPath = path + displayLocation + "/"
    if (!new java.io.File(bannerPath).exists) {
      Files.createDirectories(Paths.get(bannerPath))
    }

    import java.nio.file.{Files, Paths, StandardCopyOption}

    var fileName = multipartFile.getOriginalFilename
    if (fileName.indexOf(".") > 0) {
      fileName = fileName.substring(0, fileName.lastIndexOf("."));
    }
    fileName = fileName.replaceAll(" ", "") + "-" + System.currentTimeMillis().toString

    val is = multipartFile.getInputStream
    Files.copy(is, Paths.get(bannerPath + fileName), StandardCopyOption.REPLACE_EXISTING)
    val resp = discoveryInternalService.addBanner(BannerAddReq(
      displayLocation,
      link,
      description,
      fileName
    ))

    val result = Await.result(resp, discoveryApiTimeOut)
    logger.debug("Banner for the display Location :  '" + displayLocation + "' and the description is : '" + description + "' is added Successfully")
    result.status
  }


  @ResponseBody
  @RequestMapping(value = Array("/editBanner"), method = Array(RequestMethod.POST))
  def bannersEdit(model: Model, modelMap: ModelMap, request: HttpServletRequest, @RequestParam multipartFile: MultipartFile): String = {
    try {
      val id = request.getParameter("banner_id").toInt
      val displayLocation = request.getParameter("display_location")
      val link = request.getParameter("link")
      val description = request.getParameter("description")
      var imageUrl = request.getParameter("image_url")
      val oldImageUrl = request.getParameter("old_image_url")
      val imageUrlDel = request.getParameter("image_url_del")

      import java.nio.file.{Files, Paths}

      val bannerPath = path + displayLocation + "/"

      val file = new File(imageUrlDel)

      if (imageUrl != file.getName && imageUrl == multipartFile.getOriginalFilename) {
        if (new java.io.File(bannerPath + file.getName).exists) {
          Files.deleteIfExists(Paths.get(bannerPath + file.getName))
        }
      }

      import java.nio.file.{Files, Paths, StandardCopyOption}

      if (!multipartFile.isEmpty) {
        var fileName = multipartFile.getOriginalFilename
        if (fileName.indexOf(".") > 0) {
          fileName = fileName.substring(0, fileName.lastIndexOf("."));
        }
        fileName = fileName.replaceAll(" ", "") + "-" + System.currentTimeMillis().toString
        imageUrl = fileName
        val is = multipartFile.getInputStream
        Files.copy(is, Paths.get(bannerPath + fileName), StandardCopyOption.REPLACE_EXISTING)

        val resp = discoveryInternalService.editBanner(id, BannerAddReq(
          displayLocation,
          link,
          description,
          imageUrl
        ))
        val result = Await.result(resp, discoveryApiTimeOut)
        logger.debug("Banner for the display Location :  ' " + displayLocation + " ' and the description is : '" + description + "' is edited Successfully")
        result.status
      }else{
        val resp = discoveryInternalService.editBanner(id, BannerAddReq(
          displayLocation,
          link,
          description,
          oldImageUrl
        ))
        val result = Await.result(resp, discoveryApiTimeOut)
        logger.debug("Banner for the display Location :  ' " + displayLocation + " ' and the description is : '" + description + "' is edited Successfully")
        result.status
      }
    } catch {
      case e: Exception => e.toString
    }
  }

  @ResponseBody
  @RequestMapping(value = Array("/deleteBanner"), method = Array(RequestMethod.POST))
  def bannersDelete(model: Model, modelMap: ModelMap, request: HttpServletRequest) = {
    val id = request.getParameter("banner_id").toInt
    val imageUrl = request.getParameter("image_url")
    val displayLocation = request.getParameter("display_location")

    import java.io.File
    import java.nio.file.{Files, Paths}


    var file = new File(imageUrl)
    logger.debug("Deleting banner image name is : " + file.getName)

    val bannerPath = path + displayLocation + "/"

    if (new java.io.File(bannerPath + file.getName).exists) {
      Files.deleteIfExists(Paths.get(bannerPath + file.getName))
    }


    val resp = discoveryInternalService.removeBanner(id)
    val result = Await.result(resp, discoveryApiTimeOut)
    logger.debug("Banner for the display Location :  " + displayLocation + " is deleted Successfully")
    result.status
  }

}

