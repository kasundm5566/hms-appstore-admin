package hms.appstore.admin.controller

import com.escalatesoft.subcut.inject.Injectable

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{RequestMethod, RequestMapping}
import org.springframework.ui.ModelMap
import javax.servlet.http.{HttpServletRequest, HttpServletResponse}

import hms.appstore.admin.service.{WebConfig, ServiceModule}
import hms.appstore.api.client.DiscoveryInternalService
import org.slf4j.LoggerFactory

import scala.concurrent.{Await, ExecutionContext}
import hms.appstore.admin.util.{AuditLogStatus, AuditLogging}

@Controller
class FeaturedAppsController extends Injectable with WebConfig with AuditLogging{

  val bindingModule = ServiceModule

  private val discoveryInternalService = inject[DiscoveryInternalService]
  private implicit val executionContext = inject[ExecutionContext]
  private lazy val logger = LoggerFactory.getLogger("FeaturedAppsController")

  @RequestMapping(value = Array("/featured"), method = Array(RequestMethod.GET))
  def appStoreFeaturedApps(model: ModelMap, response: HttpServletResponse, request: HttpServletRequest): String = {
    onPageAccess(request,"Featured application",AuditLogStatus.SUCCESS)

    val pendingCountFuture = discoveryInternalService.countPendingApps()
    val pendingCountResult = Await.result(pendingCountFuture, discoveryApiTimeOut)
    if (pendingCountResult.isSuccess) {
      model.addAttribute("pendingCount", pendingCountResult.getResult.count)

    } else {
      model.addAttribute("pendingCount", 0)
      logger.error("Error occurred while fetching the pending application count. Error[{}]", pendingCountResult.description)
    }
    model.addAttribute("userId", UserDetails.getUserId(request))
    model.addAttribute("numOffApps",numOfFeaturedApps)
    "featured-apps"
  }
}
