package hms.appstore.admin.listner

import hms.commons.SnmpLogUtil
import hms.appstore.admin.service.WebConfig
import javax.servlet.{ServletContextEvent, ServletContextListener}

class AdminContextListner extends ServletContextListener with WebConfig {


  def contextInitialized(p1: ServletContextEvent) {
    SnmpLogUtil.log(snmpStartupTrap)
  }

  def contextDestroyed(p1: ServletContextEvent) {
    SnmpLogUtil.log(snmpShutdownTrap)
  }
}
