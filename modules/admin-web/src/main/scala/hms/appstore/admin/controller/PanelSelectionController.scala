package hms.appstore.admin.controller

import com.escalatesoft.subcut.inject.Injectable

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{RequestBody, ResponseBody, RequestMethod, RequestMapping}
import org.springframework.ui.ModelMap
import javax.servlet.http.{HttpServletResponse, HttpServletRequest}

import hms.appstore.api.json._
import hms.appstore.api.client.{DiscoveryService, DiscoveryInternalService}
import hms.appstore.api.client.domain.ParentCategoryView
import hms.appstore.admin.service._

import scala.collection.JavaConverters._
import hms.appstore.admin.util._
import scala.concurrent.{Await, ExecutionContext}
import scala.concurrent.duration.Duration
import com.typesafe.scalalogging.slf4j.Logging


@Controller
class PanelSelectionController extends Injectable with WebConfig with Logging with AuditLogging{


  val bindingModule = ServiceModule

  private val discoveryInternalService = inject[DiscoveryInternalService]
  private val discoveryService = inject[DiscoveryService]
  private implicit val executionContext = inject[ExecutionContext]

  @RequestMapping(value = Array("/panels"), method = Array(RequestMethod.POST))
  def updatePanels(request: HttpServletRequest, response: HttpServletResponse, model: ModelMap) = {

    val panelTop = request.getParameter("panelTop")
    val panelBottom = request.getParameter("panelBottom")
    val panelMiddle = request.getParameter("panelMiddle")

    logger.debug("saving panelConfiguration panelTop [{}]  panelBottom [{}] panelMiddle [{}]",
      panelTop, panelBottom, panelMiddle)
    val parentCategories = List(ParentCategory("panelTop", panelTop)
      , ParentCategory("panelBottom", panelBottom)
      , ParentCategory("panelMiddle", panelMiddle))

    logger.debug("parentCategories [{}]", parentCategories)
    discoveryInternalService.createOrUpdateParentCategories(parentCategories)
    onParentCategoryUpdate(request,parentCategories,AuditLogStatus.SUCCESS)
    getPageParam(model,request)
    "panel-selection"
  }

  @RequestMapping(value = Array("/panels"), method = Array(RequestMethod.GET))
  def getPanelPage(request: HttpServletRequest, response: HttpServletResponse, model: ModelMap) = {
    
    onPageAccess(request,"Panel selection",AuditLogStatus.SUCCESS)
    getPageParam(model,request)
    "panel-selection"
  }


  private def getPageParam(modelMap: ModelMap, request: HttpServletRequest) {
    val pendingCountFuture = discoveryInternalService.countPendingApps()
    val pendingCountResult = Await.result(pendingCountFuture, discoveryApiTimeOut)
    var pendingCount: Long = 0
    if (pendingCountResult.isSuccess) {
      pendingCount = pendingCountResult.getResult.count
    }
    val panelConfigResp = Await.result(discoveryService.parentCategories(),discoveryApiTimeOut)
    modelMap.addAttribute("currentConfig",panelConfigResp.getResults.map(ParentCategoryView(_)).asJava)

    modelMap.addAllAttributes(
      Map(
        "pendingCount" -> pendingCount,
        "panelCategories" -> panelCategories.asJava,
        "userId" -> UserDetails.getUserId(request)
      ).asJava)
  }
}
