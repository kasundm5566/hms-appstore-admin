package hms.appstore.admin.controller

import java.util

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{RequestMapping, ResponseBody, RequestMethod}
import com.escalatesoft.subcut.inject.Injectable
import hms.appstore.admin.service.{WebConfig, ServiceModule}
import hms.appstore.api.client.{DiscoveryService, DiscoveryInternalService}
import scala.concurrent.{Await, ExecutionContext}
import scala.collection.JavaConverters._
import javax.servlet.http.HttpServletRequest
import hms.appstore.api.json.{ChargingData, ChargingEditReq, UpdateAppAndChargingReq, UpdateReq}
import scala.util.Random
import java.net.URLDecoder
import hms.appstore.admin.util.{AuditLogStatus, AuditLogging}
import com.typesafe.config.ConfigFactory
import hms.appstore.api.client.util.URLEncoder
import com.typesafe.scalalogging.slf4j.Logging

@Controller
class AppStateMgmtController extends Injectable with AuditLogging with WebConfig with Logging {

  val bindingModule = ServiceModule

  private val internalService = inject[DiscoveryInternalService]
  private implicit val executionContext = inject[ExecutionContext]
  private val _messageProperties = ConfigFactory.load("messages_en.properties")

  @ResponseBody
  @RequestMapping(value = Array("/approve"), method = Array(RequestMethod.POST))
  def approve(request: HttpServletRequest) = {

    val appId = request.getParameter("appId")
    val remarks = request.getParameter("remarks")
    val pubName = request.getParameter("appName")
    val resp = internalService.approveApp(appId = appId, remarks = remarks, publishName = pubName)
    val result = Await.result(resp, discoveryApiTimeOut)
    if (result.isSuccess) onApproved(request, appId, AuditLogStatus.sCode2AuditStatus(result.status))
    logger.debug(s"approving app | appId $appId | displayName $pubName | status ${result.status} | response ${result.description}")

    result.status
  }

  @ResponseBody
  @RequestMapping(value = Array("/reject"), method = Array(RequestMethod.POST))
  def reject(request: HttpServletRequest) = {

    val appId = request.getParameter("appId")
    val remarks = request.getParameter("remarks").replace("+", " ")
    val resp = internalService.rejectApp(appId = appId, remarks = remarks)
    val result = Await.result(resp, discoveryApiTimeOut)

    if (result.isSuccess) onReject(request, appId, AuditLogStatus.sCode2AuditStatus(result.status))

    logger.debug(s"rejecting app | appId $appId | status ${result.status} | response ${result.description}")

    result.status
  }

  @ResponseBody
  @RequestMapping(value = Array("/publish"), method = Array(RequestMethod.POST))
  def publish(request: HttpServletRequest) = {

    val appId = request.getParameter("appId")
    val remarks = request.getParameter("remarks").replace("+", " ")
    val pubName = request.getParameter("appName")
    val resp = internalService.publishApp(appId = appId, remarks = remarks, publishName = pubName)
    val result = Await.result(resp, discoveryApiTimeOut)
    if (result.isSuccess) onPublished(request, appId, AuditLogStatus.sCode2AuditStatus(result.status))

    logger.debug(s"publishing app | appId $appId | displayName $pubName | status ${result.status} | response ${result.description}")

    result.status
  }

  @ResponseBody
  @RequestMapping(value = Array("/unpublish"), method = Array(RequestMethod.POST))
  def unPublish(request: HttpServletRequest) = {

    val appId = request.getParameter("appId")
    val remarks = request.getParameter("remarks").replace("+", " ")
    val resp = internalService.unpublishApp(appId = appId, remarks = remarks)
    val result = Await.result(resp, discoveryApiTimeOut)
    if (result.isSuccess) onUnPublished(request, appId, AuditLogStatus.sCode2AuditStatus(result.status))

    logger.debug(s"unpublishing app | appId $appId | status ${result.status} | response ${result.description}")

    result.status
  }

  @ResponseBody
  @RequestMapping(value = Array("/addFeatured"), method = Array(RequestMethod.POST))
  def addFeaturedApps(request: HttpServletRequest) = {

    val appIds: String = request.getParameter("appIds")

    val data = appIds.split(",").zipWithIndex.collect {
      case (v, i) if (v.length > 0) => (v -> (i + 1))
    }.toMap

    val resp = internalService.updateFeaturedApps(data)
    val result = Await.result(resp, discoveryApiTimeOut)

    if (result.isSuccess) onFeaturedAppsEdit(request, appIds, AuditLogStatus.sCode2AuditStatus(result.status))
    logger.debug(s"addingfeatured app | appId $appIds | status ${result.status} | response ${result.description}")
    result.status
  }

  @ResponseBody
  @RequestMapping(value = Array("/updateApp"), method = Array(RequestMethod.POST))
  def updateApp(request: HttpServletRequest) = {

    val appId = request.getParameter("appId")
    val pubName = Option(request.getParameter("appName"))
    val selectedCategories = Option(request.getParameter("selectedCategories"))
    val description = Option(request.getParameter("description"))
    val shortDesc = Option(request.getParameter("shortDesc"))
    val operators = request.getParameterValues("operators")

    val instructionMap = operators
      .map(operator => Map(operator -> request.getParameter(s"instructions.$operator")))
      .reduce(_ ++ _)

    val labels = List(1, 2, 3).map(count => Option(request.getParameter(s"label_$count")).getOrElse(""))

    val screenShots = List(1, 2, 3).map(count =>
      Map(s"screenshot_$count" -> Option(request.getParameter(s"screenshot_$count")),
        s"mobile_screenshot_$count" -> Option(request.getParameter(s"mobile_screenshot_$count")))
    ).reduce(_ ++ _).collect {
      case (k, v) if v.isDefined && !v.get.trim.isEmpty => (k, v.get.trim)
    }

    val icon = Option(request.getParameter("icon_1"))

    val appBanner = Option(request.getParameter("app_banner"))
    val opName = _messageProperties.getString("admin.appstore.operator.name")

    var chargingDataSet: Set[ChargingData] = Set.empty

    if (request.getParameterMap.containsKey("cmbSubsChargeType") && !request.getParameter("cmbSubsChargeType").isEmpty) {
      val chargeType: String = request.getParameter("cmbSubsChargeType")
      if (chargeType == "free") {
        val subscriptionChargingData = ChargingData(ncsType = "subscription", chargingType = chargeType, amount = Option(""), direction = Option(""), frequency = Option(""))
        chargingDataSet += subscriptionChargingData
      } else if (chargeType == "flat") {
        val subscriptionChargingData = ChargingData(ncsType = "subscription", chargingType = chargeType, amount = Option(request.getParameter("txtSubsAmount")), direction = Option(""), frequency = Option(request.getParameter("cmbSubsFreq")))
        chargingDataSet += subscriptionChargingData
      }
    }

    if (request.getParameterMap.containsKey("cmbDownChargeType") && !request.getParameter("cmbDownChargeType").isEmpty) {
      val chargeType: String = request.getParameter("cmbDownChargeType")
      if (chargeType == "free") {
        val downloadableChargingData = ChargingData(ncsType = "downloadable", chargingType = chargeType, amount = Option(""), direction = Option(""), frequency = Option(""))
        chargingDataSet += downloadableChargingData
      } else if (chargeType == "flat") {
        val downloadableChargingData = ChargingData(ncsType = "downloadable", chargingType = chargeType, amount = Option(request.getParameter("txtDownAmount")), direction = Option(""), frequency = Option(""))
        chargingDataSet += downloadableChargingData
      }
    }

    if (request.getParameterMap.containsKey("cmbSmsMtChargeType") && !request.getParameter("cmbSmsMtChargeType").isEmpty) {
      val mtChargeType: String = request.getParameter("cmbSmsMtChargeType")
      if (mtChargeType == "free") {
        val smsMtChargingData = ChargingData(ncsType = "vodafone-sms", chargingType = mtChargeType, amount = Option(""), direction = Option("mt"), frequency = Option(""))
        chargingDataSet += smsMtChargingData
      } else if (mtChargeType == "flat") {
        val smsMtChargingData = ChargingData(ncsType = "vodafone-sms", chargingType = mtChargeType, amount = Option(request.getParameter("txtSmsMtAmount")), direction = Option("mt"), frequency = Option(""))
        chargingDataSet += smsMtChargingData
      }
    }

    if (request.getParameterMap.containsKey("cmbSmsMoChargeType") && !request.getParameter("cmbSmsMoChargeType").isEmpty) {
      val moChargeType: String = request.getParameter("cmbSmsMoChargeType")
      if (moChargeType == "free") {
        val smsMoChargingData = ChargingData(ncsType = "vodafone-sms", chargingType = moChargeType, amount = Option(""), direction = Option("mo"), frequency = Option(""))
        chargingDataSet += smsMoChargingData
      } else if (moChargeType == "flat") {
        val smsMoChargingData = ChargingData(ncsType = "vodafone-sms", chargingType = moChargeType, amount = Option(request.getParameter("txtSmsMoAmount")), direction = Option("mo"), frequency = Option(""))
        chargingDataSet += smsMoChargingData
      }
    }

    if (request.getParameterMap.containsKey("cmbUssdMtChargeType") && !request.getParameter("cmbUssdMtChargeType").isEmpty) {
      val mtChargeType: String = request.getParameter("cmbUssdMtChargeType")
      if (mtChargeType == "free") {
        val ussdMtChargingData = ChargingData(ncsType = "vodafone-ussd", chargingType = mtChargeType, amount = Option(""), direction = Option("mt"), frequency = Option(""))
        chargingDataSet += ussdMtChargingData
      } else if (mtChargeType == "flat") {
        val ussdMtChargingData = ChargingData(ncsType = "vodafone-ussd", chargingType = mtChargeType, amount = Option(request.getParameter("txtUssdMtAmount")), direction = Option("mt"), frequency = Option(""))
        chargingDataSet += ussdMtChargingData
      }
    }

    if (request.getParameterMap.containsKey("cmbUssdMoChargeType") && !request.getParameter("cmbUssdMoChargeType").isEmpty) {
      val moChargeType: String = request.getParameter("cmbUssdMoChargeType")
      if (moChargeType == "free") {
        val ussdMoChargingData = ChargingData(ncsType = "vodafone-ussd", chargingType = moChargeType, amount = Option(""), direction = Option("mo"), frequency = Option(""))
        chargingDataSet += ussdMoChargingData
      } else if (moChargeType == "flat") {
        val ussdMoChargingData = ChargingData(ncsType = "vodafone-ussd", chargingType = moChargeType, amount = Option(request.getParameter("txtUssdMoAmount")), direction = Option("mo"), frequency = Option(""))
        chargingDataSet += ussdMoChargingData
      }
    }

    // Old app update implementation.
    /*    val resp = internalService.updateApp(UpdateReq(
          id = appId,
          publishName = pubName,
          category = category.filter(!_.trim.isEmpty),
          description = description.filter(!_.trim.isEmpty),
          appIcon = icon.filter(!_.trim.isEmpty),
          shortDesc = shortDesc.filter(!_.trim.isEmpty),
          screenShots = screenShots,
          instructions = instructionMap,
          labels = labels
        ))*/


    val chargingEditReq = ChargingEditReq(appId, chargingDataSet.toList)

    val resp = internalService.updateAppAndCharging(UpdateAppAndChargingReq(
      id = appId,
      publishName = pubName,
      category = selectedCategories.filter(!_.trim.isEmpty),
      description = description.filter(!_.trim.isEmpty),
      appIcon = icon.filter(!_.trim.isEmpty),
      shortDesc = shortDesc.filter(!_.trim.isEmpty),
      screenShots = screenShots,
      instructions = instructionMap,
      labels = labels,
      chargingDescription = chargingEditReq
    ))

    val result = Await.result(resp, discoveryApiTimeOut)

    if (result.isSuccess) onEdit(request, appId, AuditLogStatus.sCode2AuditStatus(result.status))
    logger.debug(s"updating app | appId $appId | displayName $pubName | status ${result.status} | response ${result.description}")
    result.status
  }

}
