package hms.appstore.admin.controller

import com.escalatesoft.subcut.inject.Injectable

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{RequestMethod, RequestMapping}
import org.springframework.ui.Model

import hms.appstore.api.json.{SearchQuery, AppStatus}
import hms.appstore.api.client.domain.ApplicationView
import hms.appstore.api.client.{DiscoveryService, DiscoveryInternalService}
import hms.appstore.admin.service._
import hms.appstore.admin.util.{AuditLogStatus, AuditLogging, WebUtil}

import scala.collection.JavaConverters._
import scala.concurrent.{Await, ExecutionContext}
import javax.servlet.http.{HttpServletResponse, HttpServletRequest}

@Controller
class NewAppsController extends Injectable with WebConfig with AuditLogging {


  val bindingModule = ServiceModule

  private val discoveryInternalService = inject[DiscoveryInternalService]
  private val discoveryService = inject[DiscoveryService]

  private implicit val executionContext = inject[ExecutionContext]

  private val searchStatues = List("New")


  @RequestMapping(value = Array("/new"), method = Array(RequestMethod.GET))
  def adminHome(model: Model, request: HttpServletRequest): String = {

    onPageAccess(request, "New application requests page", AuditLogStatus.SUCCESS)

    val start = Option(request.getParameter("skip")).getOrElse("0").toInt
    val limit = Option(request.getParameter("limit")).getOrElse(numberOfResultPerPage.toString).toInt

    val appsFuture = discoveryInternalService.findPendingApps(start = start, limit = limit)
    val categoriesFuture = discoveryService.categories()
    val pendingCountFuture = discoveryInternalService.countPendingApps()


    val result = for {
      apps <- appsFuture
      categories <- categoriesFuture
      pendingCountResult <- pendingCountFuture
    } yield (apps.getResults.map(ApplicationView(_)), categories.getResults.map(_.name), pendingCountResult.getResult.count)

    val (pendingApps, categories, pendingCount) = Await.result(result, discoveryApiTimeOut)

    val urlObjList = WebUtil.getPaginationUrls("new", pendingCount.toInt)
    val activePage = (start / numberOfResultPerPage)

    model.addAllAttributes(
      Map(
        "pendingCount" -> pendingCount,
        "urlList" -> urlObjList.asJava,
        "skip" -> start,
        "pageSize" -> numberOfResultPerPage,
        "appList" -> pendingApps.asJava,
        "activePage" -> activePage,
        "searchStatuses" -> searchStatues.asJava,
        "categories" -> categories.asJava,
        "userId" -> UserDetails.getUserId(request),
        "pageType" -> "New"
      ).asJava
    )

    "index"
  }

  @RequestMapping(value = Array("/new"), method = Array(RequestMethod.POST))
  def homeSearch(modelMap: Model, request: HttpServletRequest, response: HttpServletResponse): String = {

    val searchName = Option(request.getParameter("search-name"))
    val searchCategory = Option(request.getParameter("search-category"))

    val start = Option(request.getParameter("skip")).getOrElse("0").toInt
    val limit = Option(request.getParameter("limit")).getOrElse("10").toInt

    val categoriesFuture = discoveryService.categories()
    val pendingCountFuture = discoveryInternalService.countPendingApps()

    val query = SearchQuery(
      searchText = searchName,
      start = start,
      limit = limit,
      category = searchCategory,
      status = Some(AppStatus.New)
    )

    val appSearchFuture = discoveryInternalService.searchApps(query)
    val appSearchCountFuture = discoveryInternalService.count(query)


    val result = for {
      apps <- appSearchFuture
      categories <- categoriesFuture
      appCount <- appSearchCountFuture
      pendingCount <- pendingCountFuture
    } yield (apps.getResults.map(ApplicationView(_)), categories.getResults.map(_.name), appCount.getResult.count, pendingCount.getResult.count)

    val (allApps, categories, allAppCount, pendingCount) = Await.result(result, discoveryApiTimeOut)

    val urlObjList = WebUtil.getPaginationUrls("new", allAppCount.toInt)

    modelMap.addAllAttributes(
      Map(
        "pendingCount" -> pendingCount,
        "urlList" -> urlObjList.asJava,
        "skip" -> start,
        "appList" -> allApps.asJava,
        "searchStatuses" -> searchStatues.asJava,
        "searchKw" -> searchName.getOrElse(null),
        "searchFilterCat" -> searchCategory.getOrElse(null),
        "searchFilterStatus" -> "New",
        "categories" -> categories.asJava,
        "userId" -> UserDetails.getUserId(request)
      ).asJava
    )

    "index"
  }

}
