package hms.appstore.admin.service

import scala.concurrent.duration._
import scala.collection.JavaConverters._
import com.typesafe.config.ConfigFactory
import hms.scala.http.util.ConfigUtils

trait WebConfig {

  private val _config = ConfigUtils.prepareSubConfig(ConfigFactory.load(), "appstore.admin")

  private val _casConfig = ConfigFactory.load("cas.properties")

  /**
   * The max amount of time we can wait for a discovery-api call.
   * Note: This value should be > spray.can.client.request-timeout in application.conf
   * @return timeout duration
   */
  def discoveryApiTimeOut: Duration = Duration(_config.getString("discovery.api.timeout"))

  def numberOfResultPerPage: Int = _config.getInt("discovery.api.result.per.page")

  def snmpStartupTrap: String = _config.getString("snmp.startup.trap")

  def snmpShutdownTrap: String = _config.getString("snmp.shutdown.trap")

  def casLoginUrl: String = _casConfig.getString("cas.login.url")

  def casLogoutUrl: String = _casConfig.getString("cas.logout.url")

  def imageDirectoryPath: String = "/hms/data/discovery-api/images"

  def numOfFeaturedApps: Int = _config.getInt("number.of.featured.apps")

  def panelCategories: List[String] = _config.getStringList("panel.categories").asScala.toList

  def module: String = _config.getString("module")

  def categoryIconWidth: Int = _config.getInt("category.icon.width")

  def categoryIconHeight: Int = _config.getInt("category.icon.height")

  def categoryIconSize: Int = _config.getInt("category.icon.size")

  def appBannerFeatureEnable: Boolean = _config.getBoolean("app.banner.feature.enable")

  def imageBasePath: String = _config.getString("image.base.path")

  def bannerImageCharacterLimit: Int = _config.getInt("banner.image.name.character.limit")

  def notificationMessageTitleCharacterLimit: Int = _config.getInt("push.notification.message.title.character.limit")

  def notificationMessageBodyCharacterLimit: Int = _config.getInt("push.notification.message.body.character.limit")
}
