/*
 * (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.admin.util


import scala.collection.mutable.ListBuffer
import hms.appstore.admin.service.WebConfig

object WebUtil extends WebConfig {

  def getPaginationUrls(urlSuffix: String, count: Int): List[UrlValue] = {
    val retVal = new ListBuffer[UrlValue]
    var skip = 0

    val urlCount = count / numberOfResultPerPage

    for (i <- 0 to urlCount) {
      retVal.append(new UrlValue(s"/$urlSuffix?skip=$skip&limit=$numberOfResultPerPage", i))
      skip += (numberOfResultPerPage)
    }

    retVal.toList
  }

  def getPaginationUrlsForSearching(urlSuffix: String, count: Int,searchKw: String, searchCat: String, searchStatus:String): List[UrlValue] = {
    val retVal = new ListBuffer[UrlValue]
    var skip = 0
    val urlCount = count / numberOfResultPerPage
    for (i <- 0 to urlCount) {
      var urlParameters = s"/$urlSuffix?skip=$skip&limit=$numberOfResultPerPage"

      if (searchKw!=""){
        urlParameters += s"&search-name=$searchKw"
      }
      if (searchCat!=""){
        urlParameters += s"&search-category=$searchCat"
      }
      if (searchStatus!=""){
        urlParameters += s"&search-status=$searchStatus"
      }

      retVal.append(new UrlValue(urlParameters, i))
      skip += (numberOfResultPerPage)
    }
    retVal.toList
  }
}
