package hms.appstore.admin.controller

import javax.servlet.http.HttpServletRequest

object UserDetails {

  def getUserId(req:HttpServletRequest):String={
    if(req.getUserPrincipal != null) {
      req.getUserPrincipal.getName
    }else{
      "Undefined"
    }
  }
}
