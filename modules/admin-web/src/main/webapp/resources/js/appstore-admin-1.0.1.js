jQuery(function ($) {
    $('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
    function tooltip_placement(context, source) {
        var $source = $(source);
        var $parent = $source.closest('table')
        var off1 = $parent.offset();
        var w1 = $parent.width();

        var off2 = $source.offset();
        var w2 = $source.width();

        if (parseInt(off2.top) < parseInt(off1.top) + parseInt(w1 / 2)) return 'top';
        return 'bottom';
    }
});

$("#extra_search").click(function (e) {
    e.stopPropagation();
});


// $("form[class=editBanner]").on("reset", function (event) {
//     $("#editBanners").trigger('reset');
// });

// $("#editBannerClose").click(function () {
//     $("#editBanners").trigger('reset');
// });

function resetFunctionCat(data) {
    $("#" + data + "editCategories").trigger('reset');
}

function resetFunctionCat2(data) {
    $("#" + data + "editCategories").trigger('reset');
}

function resetFunctionCat3() {
    $("#addCategories").trigger('reset');
}

function resetFunction(data) {
    $("#" + data + "editBanners").trigger('reset');
    // location.reload();
}

function resetFunction2(data) {
    $("#" + data + "editBanners").trigger('reset');
    // location.reload();
}

function resetFunctionAddBanner() {
    $("#addBanners").trigger('reset');
    // location.reload();
}


//jQuery plugin to handle IE ajax issues
if (window.XDomainRequest) {
    jQuery.ajaxTransport(function (s) {
        if (s.crossDomain && s.async) {
            if (s.timeout) {
                s.xdrTimeout = s.timeout;
                delete s.timeout;
            }
            var xdr;
            return {
                send: function (_, complete) {
                    function callback(status, statusText, responses, responseHeaders) {
                        xdr.onload = xdr.onerror = xdr.ontimeout = jQuery.noop;
                        xdr = undefined;
                        complete(status, statusText, responses, responseHeaders);
                    }

                    xdr = new XDomainRequest();
                    xdr.onload = function () {
                        callback(200, "OK", { text: xdr.responseText }, "Content-Type: " + xdr.contentType);
                    };
                    xdr.onerror = function () {
                        callback(404, "Not Found");
                    };
                    xdr.onprogress = jQuery.noop;
                    xdr.ontimeout = function () {
                        callback(0, "timeout");
                    };
                    xdr.timeout = s.xdrTimeout || Number.MAX_VALUE;
                    xdr.open(s.type, s.url);
                    xdr.send(( s.hasContent && s.data ) || null);
                },
                abort: function () {
                    if (xdr) {
                        xdr.onerror = jQuery.noop;
                        xdr.abort();
                    }
                }
            };
        }
    });
}

function layoutFixed() {
    ace.settings.navbar_fixed(true);
    ace.settings.sidebar_fixed(true);
    ace.settings.breadcrumbs_fixed(true);

    //since app-edit, app-view pages are loaded using modal, here we force all modals to reload all the time.
    $('body').on('hidden', '.modal', function () {
        $(this).removeData('modal');
    });

}

function validateAppSearch(formNameStr) {
    var appName = document.forms[formNameStr]["search-name"].value;
    var appCategory = document.forms[formNameStr]["search-category"].value;
    var appStatus = document.forms[formNameStr]["search-status"].value;
    var spName = document.forms[formNameStr]["search-sp"].value;
    var appType = document.forms[formNameStr]["search-type"].value;
    var appId = document.forms[formNameStr]["search-id"].value;

    return !(
        appName.trim() == "" &&
        appCategory.trim() == "" &&
        appStatus.trim() == "" &&
        spName.trim() == "" &&
        appType.trim() == "" &&
        appId.trim() == "");
}

//===========================================================================================

var app = function () {
    var self = this;
    self.name = "";
    self.id = "";
    self.status = "";
    self.pubDate = "";
    self.developer = "";
    self.appTypeList = [];
    self.remarks = "";
    self.shorDescription = "";
    self.appCategory = "";
    self.rating = 0;
    self.iconPath = ""
};

/**
 * pull back json search result
 * @param sKey
 */
var appObjArray = [];
function findApps(sKey) {

    appObjArray = [];
    var baseURL = config["discovery-api-base-url"] + "search/" + sKey;
    var append = "/start/0/limit/" + config["live-search-limit"];
    var URL = baseURL + append;
    var jqXHR = $.ajax({
        type: "GET",
        url: URL,
        dataType: 'json',
        success: function (data, status, xhr) {
            toAppObject(data, appObjArray, sKey);
        },
        error: failAjax("live search request")
    }).done(function () {
        if (jqXHR.responseText != "S1000") {
        } else {
            if (sKey != "" || sKey == undefined) {
                clientSideAlerts('alert alert-block alert-error', 'red', '250px;', 'icon-remove', 'An error occurred while live application search.');
            }
        }

    });
    return false;
}

var appArray = [];
function findAppsCategoryAppAdd(sKey, category) {

    appArray = [];
    var baseURL = config["discovery-api-base-url"] + "search/" + sKey;
    var append = "/exclude/" + category + "/start/0/limit/" + config["live-search-limit"];
    var URL = baseURL + append;
    var jqXHR = $.ajax({
        type: "GET",
        url: URL,
        dataType: 'json',
        success: function (data, status, xhr) {
            wrapToAppObject(data, appArray, sKey);
        },
        error: failAjax("live search request")
    }).done(function () {
        if (jqXHR.responseText != "S1000") {
        } else {
            if (sKey != "" || sKey == undefined) {
                clientSideAlerts('alert alert-block alert-error', 'red', '250px;', 'icon-remove', 'An error occurred while live application search.');
            }
        }

    });
    return false;
}

function findFeaturedApps() {

    var baseURL = config["discovery-api-featured-app-url"];
    var featuredApps = [];
    $.ajax(({type: "GET",
        url: baseURL,
        dataType: 'json',
        crossDomain: true,
        cache: false,
        success: function (data, status, xhr) {
            console.log("loading featured applications " + status)
            toAppObject(data, featuredApps);
            loadFeatured(featuredApps);

            /*Adding Place Holder*/
            var i = 1;
            while (i < 9) {
                var checkFeaturedTile = $("#featured_app_" + i).html();
                if ($.trim(checkFeaturedTile) == "") {
                    checkFeaturedTile = "<div id='p" + i + "'class='dropPlaceHolder' align='center'><div class='dropPlaceHolderNum'>" + i + "</div><div class='dropPlaceHolderText'>Drag and Drop<br>an App here</div></div>";
                    $("#featured_app_" + i).html(checkFeaturedTile);
                }
                i++;
            }
            /*End Adding*/
        },
        error: function (xhr, textStatus, thrownError) {
            console.log("Ajax error" + thrownError)
        }
    })).done(function (data, status, jqXHR) {
        if (data.statusCode != "S1000") {
            clientSideAlerts('alert alert-block alert-error', 'red', '250px;', 'icon-remove', 'An error occurred while updating featured applications.');
        }
    })
    return false;
}


function failAjax(context) {
    console.log("loading ajax fail CONTEXT : " + context);
}


//==================================== Featured Apps =========================================


function updateFeaturedApps() {
    var baseURL = window.location.href
    var addFeaturedURL = baseURL.replace("featured", "addFeatured");
    var item = "";
    var list = $("div[id^=featured_app]:has(input)");
    console.log("featured app count now : " + list.length);
    for (var i = 0; i < list.length; i++) {
        item += list[i].children[0].children[0].children[2].value + ",";
    }
    var postURL = addFeaturedURL + "?appIds=" + item;

    var jqXHR = $.ajax({
        type: "POST",
        url: postURL,
        success: function (data, status, xhr) {
            /*console.log("added featured applications " + status);*/
            console.info(jqXHR.responseText);
            if (data.statusCode != "S1000") {
                clientSideAlerts('alert alert-block alert-success', 'green', '400px;', 'icon-ok', 'Successfully changed featured app list');

                /*Begin -- This is to re-arrange applications after remove or adding new applications*/
                var i = 1;
                while (i < 9) {
                    $("#featured_app_" + i).find(".featuredAppList").remove();
                    i++;
                }
                findFeaturedApps();
                /*End -- This is to re-arrange applications after remove or adding new applications*/


            } else {
                clientSideAlerts('alert alert-block alert-error', 'red', '400px;', 'icon-remove', 'Error, Please refresh the browser and try again');
            }
        },
        error: function (xhr, textStatus, thrownError) {
            console.log("Ajax error" + thrownError)
        }
    });

}

/**
 * add as featured application on drag drop
 * @param self
 * @param event
 * @param ui
 */
function onDropAddAsFeatured(event, ui, self, myid) {
    $(self).find(".featuredAppList").remove();
    $(self).find(".dropPlaceHolder").remove();
    addAppAsFeatured(ui.draggable, $("#" + myid));
}

function addAppAsFeatured($item, $featuredAppsDrop) {

    var featuredAppRemoveIcon = "<i class='icon-remove-sign icon-2x featuredAppRemoveButton' id='removeButtonID'></i>";

    $item.fadeOut(function () {
        var $list = $("ul", $featuredAppsDrop).length ?
            $("ul", $featuredAppsDrop) :
            $("<ul id='AddedfeatureApps' class='featuredAppList'/>").appendTo($featuredAppsDrop);

        $item.appendTo($list).fadeIn(function () {
            $item
                .animate({ width: "148px" })
                .css({ "background-color": "#FFECB2" })
                .find(".featuredAppSuggetionThumbnail")
                .animate({ width: "140px"})
                .animate({ height: "140px" })
                .find(".featuredAppSuggetionThumbnailImage")
                .animate({ width: "138px" })
                .animate({ height: "138px"});
        });
        $item.append(featuredAppRemoveIcon).appendTo($list).fadeIn(function () {
            updateFeaturedApps();
        });
    });
}

$(function () {

    $("#featured_app_1").droppable({
        accept: "#appSearchResList > li",
        revert: "invalid",
        activeClass: "testAccept",
        hoverClass: "testHover",

        drop: function (event, ui) {
            onDropAddAsFeatured(event, ui, this, "featured_app_1");
        }
    });

    $("#featured_app_2").droppable({
        accept: "#appSearchResList > li",
        revert: "invalid",
        activeClass: "testAccept",
        hoverClass: "testHover",

        drop: function (event, ui) {
            onDropAddAsFeatured(event, ui, this, "featured_app_2");
        }
    });

    $("#featured_app_3").droppable({
        accept: "#appSearchResList > li",
        revert: "invalid",
        activeClass: "testAccept",
        hoverClass: "testHover",

        drop: function (event, ui) {
            onDropAddAsFeatured(event, ui, this, "featured_app_3");
        }
    });

    $("#featured_app_4").droppable({
        accept: "#appSearchResList > li",
        revert: "invalid",
        activeClass: "testAccept",
        hoverClass: "testHover",

        drop: function (event, ui) {
            onDropAddAsFeatured(event, ui, this, "featured_app_4");
        }
    });

    $("#featured_app_5").droppable({
        accept: "#appSearchResList > li",
        revert: "invalid",
        activeClass: "testAccept",
        hoverClass: "testHover",

        drop: function (event, ui) {
            onDropAddAsFeatured(event, ui, this, "featured_app_5");
        }
    });

    $("#featured_app_6").droppable({
        accept: "#appSearchResList > li",
        revert: "invalid",
        activeClass: "testAccept",
        hoverClass: "testHover",

        drop: function (event, ui) {
            onDropAddAsFeatured(event, ui, this, "featured_app_6");

        }
    });

    $("#featured_app_7").droppable({
        accept: "#appSearchResList > li",
        revert: "invalid",
        activeClass: "testAccept",
        hoverClass: "testHover",

        drop: function (event, ui) {
            onDropAddAsFeatured(event, ui, this, "featured_app_7");
        }
    });

    $("#featured_app_8").droppable({
        accept: "#appSearchResList > li",
        revert: "invalid",
        activeClass: "testAccept",
        hoverClass: "testHover",

        drop: function (event, ui) {
            onDropAddAsFeatured(event, ui, this, "featured_app_8");
        }
    });
});

function shortenApplicationName(appName) {
    var maxLength = config["app.name.max.size"];
    if (appName.length > maxLength) {
        appName = appName.substring(0, maxLength)
        return appName + "...";
    } else {
        return appName;
    }

}

function shortenApplicationNameCategoryAppSearch(appName) {
    var maxLength = config["app.name.max.size.category.add.apps.search"];
    if (appName.length > maxLength) {
        appName = appName.substring(0, maxLength)
        return appName + "...";
    } else {
        return appName;
    }
}

function shortenApplicationNameCategoryApps(appName) {
    var maxLength = config["app.name.max.size.category.apps.add"];
    if (appName.length > maxLength) {
        appName = appName.substring(0, maxLength)
        return appName + "...";
    } else {
        return appName;
    }
}

function shortenApplicationCategoryName(categoryName) {
    var maxLength = config["category.name.max.size.category.add.apps.search"];
    if (categoryName.length > maxLength) {
        categoryName = categoryName.substring(0, maxLength)
        return categoryName + "...";
    } else {
        return categoryName;
    }
}

function shortenAppCategoryName(categoryName) {
    var maxLength = config["category.name.max.size.category.apps.add"];
    if (categoryName.length > maxLength) {
        categoryName = categoryName.substring(0, maxLength)
        return categoryName + "...";
    } else {
        return categoryName;
    }
}

function loadFeatured(featuredLIst) {

    for (var i = 0; i < featuredLIst.length; i++) {

        var ul = document.createElement("ul");
        var li = document.createElement("li");

        $(ul).empty();
        ul.setAttribute("class", "featuredAppList");
        ul.setAttribute("id", "AddedfeatureApps");

        li.setAttribute("class", "featureAppSuggestionAppBlock");
        li.setAttribute("style", "display: list-item; background-color: rgb(255, 236, 178); width: 148px;");

        var div1 = document.createElement("div");
        div1.setAttribute("class", "featuredAppSuggetionThumbnail");
        div1.setAttribute("style", "width: 140px; height: 120px;");

        var img = document.createElement("img");
        img.setAttribute("class", "featuredAppSuggetionThumbnailImage");
        img.setAttribute("style", "width: 138px; height: 120px;");
        img.setAttribute("src", featuredLIst[i].iconPath);

        li.appendChild(div1);
        div1.appendChild(img);

        var div2 = document.createElement("div");
        div2.setAttribute("class", "featureAppSuggestionDetails");
        li.appendChild(div2);

        var div3 = document.createElement("div");
        div3.setAttribute("class", "appsToBeFeaturedTitle");
        div3.innerHTML = shortenApplicationName(featuredLIst[i].name);
        console.log("Load Featured--->" + featuredLIst[i].name);
        div2.appendChild(div3);

        var div4 = document.createElement("div");
        div4.setAttribute("class", "appsToBeFeaturedCategory");
        div4.innerHTML = shortenApplicationName(featuredLIst[i].appCategory);
        div2.appendChild(div4);

        var divDeveloper = document.createElement("div");
        divDeveloper.setAttribute('class', 'truncate-text');
        divDeveloper.innerHTML = 'By ' + featuredLIst[i].developer;
        div2.appendChild(divDeveloper);

        var divRating = document.createElement("div");
        var appRatingHTML = "";

        divRating.setAttribute("class", "searchResRating")
        divRating.setAttribute("style", "display:inline-block");
        var appRatingHTML = "";
        appRatingHTML += "<label style='font-size: 12px; color: #6E6E6E;'>" + parseInt(featuredLIst[i].rating) + "<i class='icon-star' style='color: #FFA200'></i></label>"

        /*for (var x = 0; x != parseInt(featuredLIst[i].rating); x++) {
            appRatingHTML += "<i class='icon-star' style='color: #FFA200'></i>";
        }
        if (parseInt(featuredLIst[i].rating) < 5) {
            for (var z = 0; z < (5 - (parseInt(featuredLIst[i].rating))); z++) {
                appRatingHTML += "<i class='icon-star-empty' style='color: #FFA200'></i>";
            }
        }*/

        divRating.innerHTML = appRatingHTML;
        div2.appendChild(divRating);

        var input1 = document.createElement("input");
        $(input1).val(featuredLIst[i].id);
        input1.setAttribute("name", "appId");
        input1.setAttribute("type", "hidden");
        li.appendChild(input1);

        var input2 = document.createElement("input");
        $(input2).val(featuredLIst[i].name);
        input2.setAttribute("name", "appName");
        input2.setAttribute("type", "hidden");
        li.appendChild(input2);

        var i2 = document.createElement("i");

        $(i2).click(function removeSelectedFeaturedApp() {
            var id = $(this).attr("id");
            console.log(id);

            var getUl = $(this).closest("ul");
            var getParentId = $(getUl).parent("div").attr("id");
            var placeHolderParentId = "#" + getParentId;
            var getParentIdPlace = getParentId.slice(-1);
            console.log(placeHolderParentId);

            $.when($(this).closest("ul").remove()).then(function () {
                updateFeaturedApps();

                $(placeHolderParentId).html("<div id='p" + getParentIdPlace + "' class='dropPlaceHolder' align='center'><div class='dropPlaceHolderNum'>" + getParentIdPlace + "</div><div class='dropPlaceHolderText'>Drag and Drop<br>an App here</div></div>");

            });
        });

        i2.setAttribute("class", "icon-remove-sign icon-2x featuredAppRemoveButton");
        i2.setAttribute("id", featuredLIst[i].id);
        li.appendChild(i2);

        ul.appendChild(li);
        /*var getDummy  = document.getElementById("selectedFeaturedApps");
         getDummy.appendChild(ul);*/

        var countElementNum = parseInt(i + 1);
        var getViewElementID = "featured_app_" + countElementNum;
        var getRemovePlace = "p" + countElementNum;

//        console.log(getViewElementID);

        var getPlaceHolder = document.getElementById("" + getRemovePlace + "");
        $(getPlaceHolder).remove();

        var getViewElement = document.getElementById("" + getViewElementID + "");
        getViewElement.appendChild(ul);
    }
}

/**
 * wrap the search result in html elements
 */
function trigger(appsArray, sKey) {

    if (appsArray.length < 1 && sKey != "" && sKey != undefined) {
        clientSideAlerts('alert alert-warning', '', '350px;', '', 'no applications found with given keyword ' + sKey);
    }

    var ul = $("#appSearchResList");
    ul.empty();
    ul.hide();

    for (var i = 0; i < appsArray.length; i++) {

        var li = document.createElement("li");

        $(li).draggable({
            appendTo: "body",
            helper: "clone",
            revert: "invalid"
        });


        li.setAttribute("class", "featureAppSuggestionAppBlock");

        var div1 = document.createElement("div");
        div1.setAttribute("class", "featuredAppSuggetionThumbnail");

        var img = document.createElement("img");
        img.setAttribute("src", appsArray[i].iconPath);
        img.setAttribute("class", "featuredAppSuggetionThumbnailImage");

        div1.appendChild(img);
        li.appendChild(div1);

        var div2 = document.createElement("div");
        div2.setAttribute("class", "featureAppSuggestionDetails");

        var div3 = document.createElement("div");
        div3.setAttribute("class", "appsToBeFeaturedTitle");
        div3.innerHTML = appsArray[i].name;
        console.log("Trigger--->" + appsArray[i].name);
        div2.appendChild(div3);
        li.appendChild(div2);


        var div4 = document.createElement("div");
        div4.setAttribute("class", "appsToBeFeaturedCategory");
        div4.innerHTML = appsArray[i].appCategory;
        div2.appendChild(div4);

        var input_id = document.createElement("input");
        $(input_id).val(appsArray[i].id);
        input_id.setAttribute("name", "appId");
        input_id.setAttribute("type", "hidden");
        li.appendChild(input_id);

        var input_name = document.createElement("input");
        $(input_name).val(appsArray[i].name);
        input_name.setAttribute("name", "appName");
        input_name.setAttribute("type", "hidden");
        li.appendChild(input_name);

        var divRating = document.createElement("div");
        var appRatingHTML = "";
        var divRating = document.createElement("div");
        divRating.setAttribute("class", "searchResRating")
        divRating.setAttribute("style", "display:inline-block");
        var appRatingHTML = "";
        appRatingHTML += "<label style='font-size: 12px; color: #6E6E6E;'>" + parseInt(appsArray[i].rating) + "<i class='icon-star' style='color: #FFA200'></i></label>"
        /*for (var x = 0; x != parseInt(appsArray[i].rating); x++) {
         appRatingHTML += "<i class='icon-star' style='color: #FFA200'></i>";
         }
         if (parseInt(appsArray[i].rating) < 5) {
         for (var z = 0; z < (5 - (parseInt(appsArray[i].rating))); z++) {
         appRatingHTML += "<i class='icon-star-empty' style='color: #FFA200'></i>";
         }
         }*/

        divRating.innerHTML = appRatingHTML;
        div2.appendChild(divRating);
//        console.log("RATING : " + appsArray[i].rating)

        ul.append(li);
//        console.log("result application names : " + appsArray[i].name);
    }
    ul.fadeIn();
}

function triggerCategoryAppAdd(appsArray, sKey) {

    if (appsArray.length < 1 && sKey != "" && sKey != undefined) {
        clientSideAlerts('alert alert-warning', '', '350px;', '', 'no applications found with given keyword ' + sKey);
    }

    var ul = $("#appSearchResList");
    ul.empty();
    ul.hide();

    for (var i = 0; i < appsArray.length; i++) {

        var li = document.createElement("li");

        li.setAttribute("class", "featureAppSuggestionAppBlock");

        var div1 = document.createElement("div");
        div1.setAttribute("class", "featuredAppSuggetionThumbnail");

        var img = document.createElement("img");
        img.setAttribute("src", appsArray[i].iconPath);
        img.setAttribute("class", "featuredAppSuggetionThumbnailImage");

        div1.appendChild(img);

        var addBtn = document.createElement("input");
        $(addBtn).val("+");
        addBtn.setAttribute("type", "button");
        addBtn.setAttribute("class", "appAdd");
        addBtn.setAttribute("onclick", "addAppToList($(this));$(this).closest('li').remove();");

        li.appendChild(addBtn);
        li.appendChild(div1);

        var div2 = document.createElement("div");
        div2.setAttribute("class", "featureAppSuggestionDetailsNew");

        var div3 = document.createElement("div");
        div3.setAttribute("class", "appsToBeFeaturedTitle");
        div3.innerHTML = shortenApplicationNameCategoryAppSearch(appsArray[i].name);
        console.log("Trigger--->" + appsArray[i].name);
        div2.appendChild(div3);
        li.appendChild(div2);


        var div4 = document.createElement("div");
        div4.setAttribute("class", "appsToBeFeaturedCategory");
        div4.innerHTML = shortenApplicationCategoryName(appsArray[i].appCategory);
        div2.appendChild(div4);

        var input_id = document.createElement("input");
        $(input_id).val(appsArray[i].id);
        input_id.setAttribute("name", "appId");
        input_id.setAttribute("type", "hidden");
        li.appendChild(input_id);

        var input_name = document.createElement("input");
        $(input_name).val(appsArray[i].name);
        input_name.setAttribute("name", "appName");
        input_name.setAttribute("type", "hidden");
        li.appendChild(input_name);

        var input_category = document.createElement("input");
        $(input_category).val(appsArray[i].appCategory);
        input_category.setAttribute("name", "appCategory");
        input_category.setAttribute("type", "hidden");
        li.appendChild(input_category);

        var divRating = document.createElement("div");
        divRating.setAttribute("class", "searchResRating")
        divRating.setAttribute("style", "display:inline-block");
        var appRatingHTML = "";
        appRatingHTML += "<label style='font-size: 12px; color: #6E6E6E;'>" + parseInt(appsArray[i].rating) + "<i class='icon-star' style='color: #FFA200'></i></label>"

        /*        for (var x = 0; x != parseInt(appsArray[i].rating); x++) {
         appRatingHTML += "<i class='icon-star' style='color: #FFA200'></i>";
         }
         if (parseInt(appsArray[i].rating) < 5) {
         for (var z = 0; z < (5 - (parseInt(appsArray[i].rating))); z++) {
         appRatingHTML += "<i class='icon-star-empty' style='color: #FFA200'></i>";
         }
         }*/

        divRating.innerHTML = appRatingHTML;
        div2.appendChild(divRating);

//        console.log("RATING : " + appsArray[i].rating)

        ul.append(li);
//        console.log("result application names : " + appsArray[i].name);
    }
    ul.fadeIn();
}

/**
 * wrap scala objects to javascript objects
 * @param json
 */
function toAppObject(json, searchResult, sKey) {

//    console.log("converting xhr result to js app objects");

    var iconBase = config["app-image-base-url"];
    var tempApp;
    json = json.results;
    for (var i = 0; i < json.length; i++) {

        tempApp = new app();

        tempApp.name = json[i]["name"];
        tempApp.id = json[i]["id"];
        tempApp.pubDate = json[i]["requested-date"];
        tempApp.remarks = json[i].remarks;
        tempApp.status = json[i]["status"];
        tempApp.shorDescription = json[i]["short-description"];
        var developerName = json[i]["developer"];
        tempApp.developer = (developerName.length > 12) ? (developerName.substr(0, 10) + '..') : developerName;
        tempApp.appCategory = json[i]["category"];
        tempApp.appTypeList = json[i]["app-types"];
        tempApp.rating = json[i]["rating"];
        tempApp.iconPath = (iconBase + json[i]["app-icon"]);
        searchResult.push(tempApp)
    }

    trigger(appObjArray, sKey);
}

function wrapToAppObject(json, searchResult, sKey) {

//    console.log("converting xhr result to js app objects");

    var iconBase = config["app-image-base-url"];
    var tempApp;
    json = json.results;
    for (var i = 0; i < json.length; i++) {

        tempApp = new app();

        tempApp.name = json[i]["name"];
        tempApp.id = json[i]["id"];
        tempApp.pubDate = json[i]["requested-date"];
        tempApp.remarks = json[i].remarks;
        tempApp.status = json[i]["status"];
        tempApp.shorDescription = json[i]["short-description"];
        var developerName = json[i]["developer"];
        tempApp.developer = (developerName.length > 12) ? (developerName.substr(0, 10) + '..') : developerName;
        tempApp.appCategory = json[i]["category"];
        tempApp.appTypeList = json[i]["app-types"];
        tempApp.rating = json[i]["rating"];
        tempApp.iconPath = (iconBase + json[i]["app-icon"]);
        searchResult.push(tempApp)
    }

    triggerCategoryAppAdd(appArray, sKey);
}

$(function () {

    $("#searchKey").val("");
    $("#searchKey").keyup(function (e) {

        if (this.value.length == 0) {
            $("#appSearchResList").empty();
        }
        if (e.keyCode == 13) {
            console.log("Search for " + this.value);
            findApps(this.value);
        }
    });
    var baseURL = window.location.href;

    if (baseURL.indexOf("featured") != -1) {
        console.log("Loading Featured Apps");
        findFeaturedApps();
    }
});

function getQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

$(function () {

    $("#appSearchKey").val("");

    $("#appSearchKey").unbind('keyup').bind('keyup', function (e) {
        if (this.value.length == 0) {
            $("#appSearchResList").empty();
        }
        if (e.keyCode == 13) {
            console.log("Search for " + this.value);
            findAppsCategoryAppAdd(this.value, $("#categoryNameField").val());
        }
    });
    /*$("#appSearchKey").keyup(function (e) {

     if (this.value.length == 0) {
     $("#appSearchResList").empty();
     }
     if (e.keyCode == 13) {
     console.log("Search for " + this.value);
     findAppsCategoryAppAdd(this.value);
     }
     });*/
});

//===========================================================================================


function clientSideAlerts(alertClass, alertColor, alertwidth, alertIcon, alertMsgNormal) {

    var getAlertArea = document.getElementById("alertArea");
    var alertIDNum = Math.floor((Math.random() * 1000000000000000000) + 1);
    var alertID = "#" + alertIDNum;

    var div1 = document.createElement("div");
    div1.setAttribute("class", "alert alert-block pull-right " + alertClass + "");
    div1.setAttribute("id", "" + alertIDNum + "");
    div1.setAttribute("style", "width: " + alertwidth + "; margin-top: -15px; margin-right: 10px;");

    var iconOk = document.createElement("i");
    iconOk.setAttribute("class", "" + alertIcon + " " + alertColor + "");
    div1.appendChild(iconOk);

    var alertCloseButton = document.createElement("button");
    alertCloseButton.setAttribute("type", "button");
    alertCloseButton.setAttribute("class", "close");
    alertCloseButton.setAttribute("data-dismiss", "alert");

    var closeButtonIcon = document.createElement("i");
    closeButtonIcon.setAttribute("class", "icon-remove");
    alertCloseButton.appendChild(closeButtonIcon);

    var normalTag = document.createElement("normal");

    normalTag.innerHTML = " " + alertMsgNormal + "";
    div1.appendChild(normalTag);

    var strongTag = document.createElement("strong");
    strongTag.setAttribute("class", "" + alertColor + "");

    div1.appendChild(strongTag);
    div1.appendChild(alertCloseButton);
    getAlertArea.appendChild(div1);

    function hideAlert(alertID) {
        setTimeout(function () {
            $(alertID).fadeOut(1000);
        }, 3000);
    };

    hideAlert("" + alertID + "");
}


function hideModal() {
    $('.modal').hide();
    $("#editBanners").trigger('reset');
    $('.modal-backdrop').remove();

    $(".modal").on("hidden.bs.modal", function () {
        $(this).find('form')[0].reset();
    });
}
/**
 * change state label on app publish/unpublish
 * @param appId
 */
function changeStateLabel(appId) {

//    console.info(appId);
    var searchId = appId + "AppStateLabel";

    var linkId = appId + "pubUnpubLink";
    var iconId = appId + "pubUnpubIcon";
    var rejectIconId = appId + "rejectIcon";


    console.log(linkId + "--------------------------------------");
    console.log(iconId + "--------------------------------------");


    var element = document.getElementById(searchId);

    var link_element = document.getElementById(linkId);
    var icon_element = document.getElementById(iconId);
    var reject_element = document.getElementById(rejectIconId);

    var currentLbl = element.getAttribute("class");

    element.removeAttribute("class");

    link_element.removeAttribute("class");
    link_element.removeAttribute("data-original-title");
    link_element.removeAttribute("href");
    icon_element.removeAttribute("class");


    if (currentLbl.indexOf("warning") != -1 ||
        currentLbl.indexOf("success") == -1) {
        element.innerHTML = "Published";

        element.setAttribute("class", "label label-success");

        link_element.setAttribute("class", "red tooltip-error");
        link_element.setAttribute("data-original-title", "Unpublish");
        link_element.setAttribute("href", "#" + appId + "upub");
        icon_element.setAttribute("class", "icon-unlink bigger-130");
        reject_element.remove();

    } else {
        element.innerHTML = "Unpublished";

        element.setAttribute("class", "label label-warning");
        link_element.setAttribute("class", "green tooltip-success");
        link_element.setAttribute("data-original-title", "Publish");
        link_element.setAttribute("href", "#" + appId + "pub");
        icon_element.setAttribute("class", " icon-link bigger-130");

    }
}

function removeNewlyPublished(trId) {
    var removeId = "#" + trId + "Row";
    $(removeId).remove();
}

function progressBar(pa_id, pb_id, img_plh, i, j) {

    var progressBarArea = document.getElementById(pa_id);
    var defaultImage = document.getElementById(img_plh);

    progressBarArea.removeAttribute("style");
    defaultImage.setAttribute("style", "display:none;");

    var progressBar = document.getElementById(pb_id);

    function callback() {
        setTimeout(function () {

            progressBar.setAttribute("style", "width:" + i + "%");
//            console.log(i);

        }, j);
    }

    callback();
//    console.log("finish");
}


//=========================================== Featured Panel Configurations ==============================================


$("#frmPannel").on("submit", function (event) {

    event.preventDefault();
    var formData = $(this).serialize();
    var targetURL = window.location.href;

    $.ajax({
        type: "POST",
        url: targetURL,
        data: formData,
        success: function (data, status, jqXHR) {
            if (data == "S1000") {
                clientSideAlerts('alert alert-block alert-success', 'green', '400px;', 'icon-ok', 'Panel configurations successfully saved!');
            } else {
                clientSideAlerts('alert alert-block alert-error', 'red', '400px;', 'icon-remove', 'Failed to save panel configurations!');
            }
        }
    });
});


//============================================ Advertisement Configuration =================================================

$("#frmAds").on("submit", function (event) {
    event.preventDefault();
    $(this).validate();
    var targetURL = event.currentTarget.action;
    var formData = $(this).serialize();
    $.ajax({
        type: "POST",
        url: targetURL,
        data: formData,
        success: function (data, status, jqXHR) {
            if (data == "S1000") {
                clientSideAlerts('alert alert-block alert-success', 'green', '400px;', 'icon-ok', 'advertisement configurations saved successfully!');
            } else {
                clientSideAlerts('alert alert-block alert-error', 'red', '400px;', 'icon-remove', 'failed to save the advertisement configurations!');
            }
        }
    });
});

// ========================================== Edit Application ======================================

/**
 * Generate id for the base64Image string
 * @param base64
 */
function base64ImgToId(base64, hashTextBoxID, uploadType, fileNamePrefix, pa_id, pb_id, img_plh, getImgPreviewArea, previewSrc, errorNotificationId) {

    var baseURL = config["discovery-api-base-url"];
    var actionURL = baseURL + "image-upload";
    var fType = base64.split(';')[0].split('/')[1]
    if (fType == "jpeg") {
        fType = "jpg";
    }

    var getErrorText = $('#' + errorNotificationId);
    base64 = base64.split(';')[1].split(',')[1]

    var jsonParsed = JSON.stringify({
        fileType: fType,
        uploadType: uploadType,
        fileNamePrefix: fileNamePrefix,
        data: base64
    });

    $.ajax({
        type: "POST",
        url: actionURL,
        contentType: 'application/json',
        data: jsonParsed,
        dataType: 'json',
        error: failAjax("sending image to server for validation failed"),

        success: function (data, status, jqXHR) {
            var genID = data.result.uid;
            console.info("UID " + genID);
            document.getElementById(hashTextBoxID).setAttribute("value", genID);
            progressBar(pa_id, pb_id, img_plh, 100, 100);
            var progressBarArea = document.getElementById(pa_id);
            progressBarArea.setAttribute("style", "display:none");

            if (genID == -1) {
                var errorNote = "<i class='icon-remove'></i> " + data.statusDescription;
                console.log(errorNote + '' + errorNotificationId);
                getErrorText.html(errorNote);
                $('#' + hashTextBoxID).attr("value", "");
                $(getImgPreviewArea).attr("src", "");
                $(getImgPreviewArea).attr("style", "display:none;");
//                $(imagePlaceHolder).attr("style", "");
            } else {
                getErrorText.html("");
//                $(imagePlaceHolder  ).attr("style", "display:none");
                $('#' + hashTextBoxID).attr("value", genID);
                $(getImgPreviewArea).attr("src", previewSrc);
                $(getImgPreviewArea).attr("style", "");
            }
        }
    });

}

/**
 * update application on app Edit
 * @param actionURL
 */
function editApp(actionURL, formData) {

    var jqXHR = $.ajax({
        type: "POST",
        url: actionURL,
        data: formData
    }).done(function () {
        location.reload();
        hideModal();
        if (jqXHR.responseText == "S1000") {
            clientSideAlerts('alert alert-block alert-success', 'green', '400px;', 'icon-ok', 'Changes successfully saved!');
        } else {
            clientSideAlerts('alert alert-block alert-error', 'red', '400px;', 'icon-remove', 'Failed to save application changes!');
        }
    });
}

/**
 * update appStatus form serialization
 */
$("form[class=appstatus]").on("submit", function (event) {
    event.preventDefault();
    $(this).validate();
    var baseURL = event.currentTarget.action;
    var data = $(this).serialize();

    console.log($(this));
    updateAppStatus(baseURL, data, $(this).context.appId.value, $(this).context.appName.value);

});

/**
 * Make xhr to update appStatus
 * @param url
 */
function updateAppStatus(url, data, idToUpdate, appName) {
    var jqXHR = $.ajax({
        type: "POST",
        url: url,
        data: data,
        scriptCharset: "utf-8",
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        crossDomain: true,
        error: failAjax("updating appStatus failed")
    }).done(function () {
        hideModal();
        if (jqXHR.responseText == "S1000") {
            changeStateLabel(idToUpdate, appName);
            clientSideAlerts('alert alert-block alert-success',
                'green',
                '400px;',
                'icon-ok',
                ("Application  " + appName + " state successfully updated"));
        } else {
            clientSideAlerts('alert alert-block alert-error',
                'red',
                '400px;',
                'icon-remove',
                ("Application  " + appName + " state update failed"));
        }
    })
}

//Delete Category /

$("form[class=deleteCategory]").on("submit", function (event) {
    event.preventDefault();
    $(this).validate();
    var baseURL = event.currentTarget.action;
    var data = $(this).serialize();

    console.log($(this));
    deleteCategory(baseURL, data);

});

/**
 * Make xhr to delete Category
 * @param url
 */
function deleteCategory(url, data) {
    var jqXHR = $.ajax({
        type: "POST",
        url: url,
        data: data,
        scriptCharset: "utf-8",
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        crossDomain: true,
        error: failAjax("deleting Category failed")
    }).done(function () {
        location.reload();
        hideModal();
        if (jqXHR.responseText == "S1000") {
            clientSideAlerts('alert alert-block alert-success',
                'green',
                '400px;',
                'icon-ok',
                ("Category  deleted successfully"));
        } else {
            clientSideAlerts('alert alert-block alert-error',
                'red',
                '400px;',
                'icon-remove',
                ("Failed to delete the Category"));
        }
    })
}


//Edit Category /

$("form[class=editCategory]").on("submit", function (event) {
    event.preventDefault();
    var formData = new FormData($(this)[0]);
    var baseURL = event.currentTarget.action;
    var data = $(this).serialize();
    console.log($(this));
    editCategory(baseURL, formData);
});

/**
 * Make xhr to delete Category
 * @param url
 */
function editCategory(url, data) {
    var jqXHR = $.ajax({
        type: "POST",
        url: url,
        data: data,
        enctype: 'multipart/form-data',
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        error: failAjax("Editing Category is failed")
    }).done(function () {
        window.location.reload(true);
        // location.reload();
        hideModal();
        if (jqXHR.responseText == "S1000") {
            clientSideAlerts('alert alert-block alert-success',
                'green',
                '400px;',
                'icon-ok',
                ("Category  " + " state successfully updated"));
        } else {
            clientSideAlerts('alert alert-block alert-error',
                'red',
                '400px;',
                'icon-remove',
                ("Category  " + " state update failed"));
        }
    })
}


//Delete Category /

$("form[class=deleteBanner]").on("submit", function (event) {
    event.preventDefault();
    $(this).validate();
    var baseURL = event.currentTarget.action;
    var data = $(this).serialize();

    console.log($(this));
    deleteBanner(baseURL, data);

});

/**
 * Make xhr to delete Category
 * @param url
 */
function deleteBanner(url, data) {
    var jqXHR = $.ajax({
        type: "POST",
        url: url,
        data: data,
        scriptCharset: "utf-8",
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        crossDomain: true,
        error: failAjax("deleting Banner failed")
    }).done(function () {
        window.location.reload(true);
        hideModal();
        if (jqXHR.responseText == "S1000") {
            clientSideAlerts('alert alert-block alert-success',
                'green',
                '400px;',
                'icon-ok',
                ("Banner  deleted successfully"));
        } else {
            clientSideAlerts('alert alert-block alert-error',
                'red',
                '400px;',
                'icon-remove',
                ("Failed to delete the Banner"));
        }
    })
}


//Edit Banner /


$("form[class=editBanner]").on("submit", function (event) {
    event.preventDefault();
    $(this).validate();
    var formData = new FormData($(this)[0]);
    var baseURL = event.currentTarget.action;
    var data = $(this).serialize();
    console.log($(this));
    editBanner(baseURL, formData);

});

/**
 * Make xhr to delete Category
 * @param url
 */


function editBanner(url, data) {
    var jqXHR = $.ajax({
        type: "POST",
        url: url,
        data: data,
        enctype: 'multipart/form-data',
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        error: failAjax("Editing Banner is failed")
    }).done(function (res) {
        if (res == "S1000") {
            clientSideAlerts('alert alert-block alert-success',
                'green',
                '400px;',
                'icon-ok',
                ("Banner  " + " state successfully updated"));
            hideModal();
            location.reload();
        } else {
            hideModal();
            clientSideAlerts('alert alert-block alert-error',
                'red',
                '400px;',
                'icon-remove',
                (res));
        }
    })
}

function getFile(elementID) {
    document.getElementById(elementID).click();
    return false;
}

function imgUploadError(imgUploadErrorText, imgUploadErrorElement) {
    var errorNotifyArea = document.getElementById(imgUploadErrorElement);
    errorNotifyArea.innerHTML = "<i class='icon-warning-sign'></i> " + imgUploadErrorText;
}

function imgUploadErrorClear(imgUploadErrorElement) {
    var errorNotifyArea = document.getElementById(imgUploadErrorElement);
    errorNotifyArea.innerHTML = "";
}


function preview(input, previewTag, hashTextBoxId, hiddenImgOriginal, uploadType, appId, fileNamePrefix, pa_id, pb_id, img_plh) {
    if (input.files && input.files[0]) {

        var errorNotificationId = appId + 'imgUploadingError';
        switch (uploadType) {
            case "Icon":
                errorNotificationId += 'Icon';
                break;
            case "ScreenShot":
                errorNotificationId += 'ScreenShot';
                break;
            case "MobileScreenShot":
                errorNotificationId += 'Mobile';
                break;
            case "AppBanner":
                errorNotificationId += 'Banner';
                break;
        }

        var getFile = input.files[0];
        var reader = new FileReader();
        reader.onload = function (e) {

            var imageData = e.target.result;
            var getHiddenImg = document.getElementById(hiddenImgOriginal);
            var getPreviewImg = document.getElementById(previewTag);

            getHiddenImg.setAttribute("src", imageData);

            var checkImageInHidden = getHiddenImg.getAttribute("src");
            if (checkImageInHidden != null) {
                var getImageHeight = $("#" + hiddenImgOriginal).height();
                var getImageWidth = $("#" + hiddenImgOriginal).width();
            }

            console.log("--------------------------" + getImageHeight);
            console.log("--------------------------" + getImageWidth);

            var getImageAscRat = getImageWidth / getImageHeight;

            console.log("Width -> " + getImageWidth + " \n" + " Height -> " + getImageHeight + " \n Aspect Ratio -> " + getImageAscRat);
            base64ImgToId(imageData, hashTextBoxId, uploadType, fileNamePrefix, pa_id, pb_id, img_plh, getPreviewImg, imageData, errorNotificationId);
        }

    }
    reader.readAsDataURL(input.files[0]);
}

$("form[class=approveReject]").on("submit", function (event) {

    event.preventDefault();
    $(this).validate();
    var actionURL = event.currentTarget.action + "?";
    var data = $(this).serialize();
    approveOrReject(actionURL, data, $(this).context.appName.value, $(this).context.appId.value, this);

});

function approveOrReject(url, data, appName, appId, self) {
    var successMsg, failMsg;

    if (self.action.indexOf("approve") > 0) {
        successMsg = "published to";
        failMsg = "publish";
    } else {
        successMsg = "rejected from";
        failMsg = "reject"
    }
    var jqXHR = $.ajax({
        type: "POST",
        url: url,
        data: data,
        scriptCharset: "utf-8",
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        success: function (data, status, xhr) {
            console.log(status);
        },
        error: failAjax("approving/rejecting app failed")
    }).done(function () {
        hideModal();
        if (jqXHR.responseText == "S1000") {
            if (failMsg == "reject" || window.location.href.indexOf("new") != -1) {
                removeNewlyPublished(appId);
            } else {
                changeStateLabel(appId, appName);
            }
            clientSideAlerts('alert alert-block alert-success',
                'green',
                '400px;',
                'icon-ok',
                ('application ' + appName + ' was ' + successMsg + ' the appstore '));
        } else {
            clientSideAlerts('alert alert-block alert-error',
                'red',
                '400px;',
                'icon-remove',
                ('failed to ' + failMsg + ' the application' + appName));
        }
    });
}

/*-------------------DROPME Plugin-------------------*/
(function ($) {
    var elmDrag, replacerSet = $();
    var eventStack = ['dragstart', 'dragend', 'selectstart', 'dragover', 'dragenter', 'drop'];

    $.fn.dropme = function (options) {

        var userOpt = options.toString();

        options = $.extend({
            linkTo: false
        }, options);

        if (options.contId) {
            //var elmes = '<ul class="sortable">';
            var elmes = '';
            var lnth = options.elem.length;
            var i;
            for (i = 0; i < lnth; i++) {
                elmes += '<li id=' + options.elem[i].id + '>' + options.elem[i].title + '</li>';
            }
            // elmes += '</ul>';
            $('#' + options.contId + ' .dropme').html(elmes);
        }

        return this.each(function () {
            var regEx = new RegExp("/^enable|disable|destroy$/");
            if (userOpt.match(regEx)) {

                var itemInOpt = $(this).data('items');

                var items = $(this).children(itemInOpt);

                if (userOpt == 'enable') {
                    items.attr('draggable', true);
                } else {
                    items.attr('draggable', false);
                }

                if (userOpt == 'destroy') {
                    items.add(this).removeData('linkTo items').off(JSON.stringify(eventStack));
                }
                return;
            }
            var index, items = $(this).children(options.items);

            var replacer = $('<' + (this.tagName.match(/^ul|ol|div$/i) ? 'li' : 'div') + ' class="drop-replacer">');

            $(this).data('items', options.items);

            replacerSet = replacerSet.add(replacer);

            if (options.linkTo) {
                $(options.linkTo).add(this).data('linkTo', options.linkTo);
            }

            items.attr('draggable', 'true').on(eventStack[0], function (e) {
                var dataTrnsfr = e.originalEvent.dataTransfer;
                dataTrnsfr.effectAllowed = 'move';
                dataTrnsfr.setData('Text', 'dummy');
                elmDrag = $(this);
                index = (elmDrag).addClass('drop-elmDrag').index();
            }).on(eventStack[1], function () {
                (elmDrag = $(this)).removeClass('drop-elmDrag').show();
                replacerSet.detach();
                if (index != elmDrag.index()) {
                    items.parent().trigger('sortupdate', {
                        item: elmDrag
                    });
                }
                elmDrag = null;
            }).not('a[href], img').on(eventStack[2], function () {
                this.dragDrop && this.dragDrop();
                return false;
            }).end().add([this, replacer]).on('dragover dragenter drop', function (event) {
                if (!items.is(elmDrag) && options.linkTo !== $(elmDrag).parent().data('linkTo')) {
                    return true;
                }
                if (event.type == 'drop') {
                    event.stopPropagation();
                    replacerSet.filter(':visible').after(elmDrag);
                    return false;
                }
                event.preventDefault();
                event.originalEvent.dataTransfer.dropEffect = 'move';
                if (items.is(this)) {
                    if (options.replacerSize) {
                        replacer.height(elmDrag.outerHeight());
                    }
                    elmDrag.hide();
                    $(this)[replacer.index() < $(this).index() ? 'after' : 'before'](replacer);
                    replacerSet.not(replacer).detach();
                } else if (!replacerSet.is(this) && !$(this).children(options.items).length) {
                    replacerSet.detach();
                    $(this).append(replacer);
                }
                return false;
            });
        });
    };
})(jQuery);
/*-------------------DROPME Plugin-------------------*/


function addAppToList(field) {

    $("#paraNoApps").hide();

    var appId = $(field).closest("li").find("input[name='appId']").val();

    var appIdNodes = document.querySelectorAll('.appIdHidden'),
        appIds = [];

    $.each(appIdNodes, function (index, appIdNode) {
        appIds.push(appIdNode.value)
    });

    if ($.inArray(appId, appIds) == -1) {
        $('.dropme').dropme('disable');
        var imageUrl = $(field).closest("li").find("div.featuredAppSuggetionThumbnail").find("img").attr("src");
        var appName = $(field).closest("li").find("div.featureAppSuggestionDetailsNew").find("div.appsToBeFeaturedTitle").text();
        var appCategory = $(field).closest("li").find("div.featureAppSuggestionDetailsNew").find("div.appsToBeFeaturedCategory").text();
        var appRating = $(field).closest("li").find("div.featureAppSuggestionDetailsNew").find("div.searchResRating").text();

        var listItem = '<li class="featureAppSuggestionAppBlock"><div><button id="btnClose" onclick="$(this).closest(\'li\').remove();updateCategoryApps();" class="appLiClose">&#10006;</button></div>' +
            '<div><input class="appIdHidden" type="hidden" value=\"' + appId + '\"><div>' +
            '<img src=\"' + imageUrl + '\" style="float: left; width: 60px; height: 60px; margin-right: 5px;"></div>' +
            '<div class="appsToBeFeaturedTitle"><label style="font-size: 12px; margin-bottom: 0px;">' + shortenApplicationNameCategoryApps(appName) + '</label></div>' +
            '<div class="appsToBeFeaturedCategory appsToBeFeaturedTitle"><label style="font-size: 12px;">' + shortenAppCategoryName(appCategory) + '</label><label style="font-size: 12px; margin-top: -5px;">' + appRating + '<i class="icon-star" style="color: #FFA200"></i></label></div></div></li>';

        $(".dropme").append(listItem);
        updateCategoryApps();
        $('.dropme').dropme('enable').bind('sortupdate', function (e, elm) {
            updateCategoryApps();
        });
    } else {
        $(".alert").hide();
        clientSideAlerts('alert alert-warning', '', '350px;', '', 'Selected app is already included.');
    }
}

function updateCategoryApps() {

    var appIdNodes = document.querySelectorAll('.appIdHidden'),
        appIds = [];

    $.each(appIdNodes, function (index, appIdNode) {
        appIds.push(appIdNode.value)
    });

    if (appIdNodes.length == 0) {
        appIds.push("");
    }
    $.ajax({
        type: "POST",
        url: "addAppsToCategory",
        data: {
            "category": getQueryStringParams("category"),
            "appIds": appIds
        },
        success: function (result) {
            if (result) {
                if (result == "S1000") {
                    $(".alert").hide();
                    clientSideAlerts('alert alert-block alert-success', 'green', '400px;', 'icon-ok', 'Successfully updated the apps of the category.');
                    /*                    setTimeout(function () {
                     window.location.reload(true);
                     }, 1000);*/
                } else {
                    $(".alert").hide();
                    clientSideAlerts('alert alert-block alert-error', 'red', '250px;', 'icon-remove', 'An error occurred while updating apps of the category.');
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".alert").hide();
            clientSideAlerts('alert alert-block alert-error', 'red', '250px;', 'icon-remove', 'An error occurred while updating apps of the category.' + errorThrown);
        }
    });
}