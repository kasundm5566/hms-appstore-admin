<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmtj" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%
    response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
    response.addHeader("Pragma", "no-cache");
    response.setDateHeader("Expires", 0);
%>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<style>
    .btn {
        background-color: rgb(7, 55, 99);
        color: white;
        border: none;
        border-radius: 10%;
        cursor: pointer;
        padding: 0px 10px 0px 10px;
        text-decoration: none;

    }

    .swal-button {
        padding: 7px 19px;
        border-radius: 2px;
        background-color: #bf0000;
        font-size: 12px;
        border: 1px solid #bf0000;
        color: #eaedf1;
        text-shadow: 0px -1px 0px rgba(0, 0, 0, 0.3);
    }
</style>
<script>
    function editImageURL(id) {
        var imgName = document.getElementById(id + "textCat").value.replace(/^.*[\\\/]/, '');
        document.getElementById(id + "textCat").value = imgName;
    }
</script>
<div class="nav-search" id="nav-search">
    <div class="hidden-phone visible-desktop">
        <a data-toggle="modal" data-target="#addCategory">
            <span class="btn"><i class="icon-plus icon-large"></i> Add </span>
        </a>
    </div>
</div>

</div>   <%--***don't remove this !**important**! end of bread crumb tag--%>

<div class="page-content">
    <div class="row-fluid">
        <div class="span12">

            <%--TABLE--%>
            <div class="row-fluid">
                <div class="table-header">
                    <fmt:message code="admin.appstore.category.table.header"/>
                </div>
                <div class="table-responsive">
                    <table id="AllAppsTable" class="table table-striped table-bordered table-hover"
                           style="table-layout: fixed;">
                        <thead>
                        <tr>
                            <th><fmt:message code="admin.appstore.category.table.name.column.header"/></th>
                            <th><fmt:message code="admin.appstore.category.table.description.column.header"/></th>
                            <th><fmt:message code="admin.appstore.category.table.icon.column.header"/></th>
                            <th style="width: 10%;"><fmt:message
                                    code="admin.appstore.category.table.action.column.header"/></th>
                            <%--app table action column header--%>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${categories}" var="category">
                            <tr>
                                <td>
                                    <c:out value="${category.name}"/>
                                </td>
                                <td style="word-wrap: break-word;">
                                    <c:out value="${category.description}"/>
                                </td>
                                <td>
                                        <%--<c:out value="${category.image_url}"/>--%>
                                        <%--<img src="resources/img/<c:out value="${category.image_url}"/>" width="50" height="50">--%>
                                    <img src="<fmt:message code="admin.appstore.category.base.url"/>/<c:out value="${category.image_url}"/>"
                                         width="50" height="50">
                                </td>
                                <td>
                                    <div class="hidden-phone visible-desktop action-buttons">
                                        <c:url value="categoryApps" var="url">
                                            <c:param name="category" value="${category.name}"/>
                                        </c:url>
                                        <a class="blue tooltip-info"
                                           href="${url}"
                                           data-rel="tooltip"
                                           title="<fmt:message code='admin.categories.table.app.action.view.icon.tool.tip'/>">
                                            <i class="icon-zoom-in bigger-130"></i></a>

                                        <a class="green tooltip-success"
                                           href="#<c:out value='${category.position}'/>edit"
                                           data-toggle="modal"
                                           data-rel="tooltip"
                                           title="edit" onclick="editImageURL(<c:out value='\"${category.id}\"'/>)">
                                            <i class="icon-pencil bigger-130"></i></a>

                                        <a class="red tooltip-error" href="#<c:out value='${category.position}'/>delete"
                                           data-toggle="modal"
                                           data-rel="tooltip"
                                           title="delete">
                                            <i class="icon-ban-circle bigger-120"></i></a>
                                    </div>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>


            </div>
            <%--row fluid end--%>
        </div>
        <%--span12 end--%>
    </div>
    <%--row fluid end--%>
</div>

</div>

<%--MODALS BEGINS--%>
<c:forEach items="${categories}" var="category">
    <%--Edit Category--%>
    <form action="editCategory" id="<c:out value='${category.position}'/>editCategories" name="editCategories"
          class="editCategory" method="POST"
          accept-charset="UTF-8" enctype="multipart/form-data">>
        <div id="<c:out value='${category.position}'/>edit" style="width:800px; margin-left:-400px;"
             class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="row-fluid vdf_modal_header_bgc">
                <div class="span10">
                    <div class="modal-header">
                        <h5 id="myModalLabel">
                            <fmt:message code="admin.appstore.category.edit.form.edit.word"/>
                            <fmt:message code="admin.appstore.category.edit.form.title.part"/>
                        </h5>
                    </div>
                </div>
                <div class="span2" style="padding-top: 15px; padding-right: 15px;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
                            onclick="resetFunctionCat2(<c:out value='\"${category.position}\"'/>)">x
                    </button>
                </div>
            </div>
            <div class="modal-body">
                <div class="span4">
                    <div class="row-fluid">
                        <div class="span12">
                            <label class="control-label mandatoryFieldLabels" style="font-size: 12px;">
                                <b><fmt:message code="admin.appstore.category.add.form.category.name"/></b>
                            </label>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span12" style="display: none">
                            <input type="text" value="<c:out value='${category.id}'/>"
                                   name="category_id"/>
                        </div>
                        <div class="span12" style="display: none">
                            <input type="text" value="<c:out value='${category.description}'/>" name="description"/>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <input class="editCatName" type="text" value="<c:out value='${category.name}'/>" name="name"
                                   style="margin-top: -5px; margin-bottom: 20px;" required/>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <label class="control-label mandatoryFieldLabels" style="font-size: 12px;">
                                <b><fmt:message code="admin.appstore.category.add.form.category.description"/></b>
                            </label>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <input type="text" value="<c:out value='${category.description}'/>"
                                   style="margin-top: -5px; margin-bottom: 20px;" name="cat-description"
                                   required/>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <label class="control-label mandatoryFieldLabels" style="font-size: 12px;">
                                <b><fmt:message code="admin.appstore.category.add.form.category.image"/></b>
                            </label>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span12" style="display: none;">
                            <input type="text" class="textClass" name="image_url"
                                   id="<c:out value='${category.id}'/>textCat"
                                   value="<c:out value="${category.image_url}"/>" required readonly/>
                        </div>
                        <div class="span12" style="display: none">
                            <input type="text" value="<c:out value='${category.image_url}'/>" name="image_url_del"/>
                        </div>
                        <input type='file' class="imageClass" name="multipartFile"
                               id="<c:out value='${category.id}'/>imgCat"
                               onchange="editCategoryDetails(this ,<c:out value='\"${category.id}\"'/>);"
                               style="margin-top: -5px; margin-bottom: 20px; line-height: 0px;"/>
                    </div>
                </div>
                <div class="span3">
                    <img id="<c:out value='${category.id}'/>catIconEdit"
                         src="<fmt:message code="admin.appstore.category.base.url"/>/<c:out value="${category.image_url}"/>"
                         width="200"
                         height="200" alt="your image" style="margin-top: 20px;"/>
                </div>
            </div>
            <div class="modal-footer">
                <button class="hmsSearchButton " data-dismiss="modal" aria-hidden="true" type="reset"
                        onclick="resetFunctionCat(<c:out value='${category.position}'/>)">
                    <fmt:message code="admin.appstore.app.edit.form.close.button"/>
                </button>
                <button class="hmsSearchButton"
                        style="margin-right:5px;" type="submit"
                        onclick="return validateCategoryName($(this).closest('form').find('.editCatName').val())">
                    <fmt:message code="admin.appstore.app.edit.form.save.button"/>
                </button>
            </div>
        </div>
    </form>


    <%--Delete Category--%>
    <form action="deleteCategory" id="deleteCategories" name="deleteCategories" class="deleteCategory" method="POST"
          accept-charset="UTF-8">
        <div id="<c:out value='${category.position}'/>delete" style="width:600px;"
             class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="row-fluid vdf_modal_header_bgc">
                <div class="span10">
                    <div class="modal-header">
                        <h5 id="myModalLabel">
                            <fmt:message code="admin.appstore.category.delete.form.edit.word"/>
                            <fmt:message code="admin.appstore.category.edit.form.title.part"/>
                        </h5>
                    </div>
                </div>
                <div class="span2" style="padding-top: 15px; padding-right: 15px;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                    <div class="span12" style="display: none">
                        <input type="text" value="<c:out value='${category.id}'/>" name="category_id"/>
                    </div>

                    <div class="span12" style="display: none">
                        <input type="text" value="<c:out value='${category.image_url}'/>" name="image_url"/>
                    </div>
                    <div class="span12">
                        <h5 id="myModalLabel">
                            Do you want to remove the Category : <c:out value='${category.name}'/>
                        </h5>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="hmsSearchButton " data-dismiss="modal" aria-hidden="true" type="reset">
                    No
                </button>
                <button class="hmsSearchButton"
                        style="margin-right:5px;" type="submit">
                    Yes
                </button>
            </div>
        </div>
    </form>

    <%--TABLE END--%>
    </div>

</c:forEach>


<%--ADD Category--%>
<form action="addCategory" id="addCategories" name="addCategories" method="POST" accept-charset="UTF-8"
      enctype="multipart/form-data">
    <div id="addCategory" style="width:800px; margin-left:-400px;"
         class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="row-fluid vdf_modal_header_bgc">
            <div class="span10">
                <div class="modal-header">
                    <h5>
                        <fmt:message code="admin.appstore.category.add.form.title.part"/>
                    </h5>
                </div>
            </div>
            <div class="span2" style="padding-top: 15px; padding-right: 15px;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
                        onclick="resetFunctionCat3()">x
                </button>
            </div>
        </div>

        <div class="modal-body">
            <div class="span4">
                <div class="row-fluid">
                    <div class="span12">
                        <label class="control-label mandatoryFieldLabels" style="font-size: 12px;">
                            <b><fmt:message code="admin.appstore.category.add.form.category.name"/></b>
                        </label>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <input class="addCatName" type="text" name="name" style="margin-top: -5px; margin-bottom: 20px;"
                               required/>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <label class="control-label mandatoryFieldLabels" style="font-size: 12px;">
                            <b><fmt:message code="admin.appstore.category.add.form.category.description"/></b>
                        </label>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <input type="text" name="description" style="margin-top: -5px; margin-bottom: 20px;" required/>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <label class="control-label mandatoryFieldLabels" style="font-size: 12px;">
                            <b><fmt:message code="admin.appstore.category.add.form.category.image"/></b>
                        </label>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12" style="display: none;">
                        <input type="text" name="image_url" id="imgTxt" required readonly/>
                    </div>

                    <input type='file' name="multipartFile" id="imgNme" onchange="categoryIcon(this);"
                           style="margin-top: -5px; margin-bottom: 20px; line-height: 0px;" required/>

                </div>
            </div>
            <div class="span3">
                <img id="imgPl" src="resources/img/app_store_sample_app_icon.png" alt="your icon" width="200"
                     height="200" style="margin-top: 20px;"/>
            </div>
        </div>

        <div class="modal-footer">
            <button class="hmsSearchButton " data-dismiss="modal" aria-hidden="true" type="reset"
                    onclick="resetFunctionCat3()">
                <fmt:message code="admin.appstore.app.edit.form.close.button"/>
            </button>
            <button id="categorySubmit" class="hmsSearchButton"
                    style="margin-right:5px;" type="submit"
                    onclick="return validateCategoryName($(this).closest('form').find('.addCatName').val())">
                <fmt:message code="admin.appstore.app.edit.form.save.button"/>
            </button>
        </div>
    </div>
</form>


<script type="text/javascript">
    var formId = "#addCategories";
    $(formId).on("submit", function (event) {
        var x = document.forms["addCategories"]["name"].value;
        var y = document.forms["addCategories"]["description"].value;
        var z = document.forms["addCategories"]["image_url"].value;
        if (x == "" || y == "") {
            swal("Warning", "All the fields must be filled", "warning");
            return false;
        }
        event.preventDefault();
        var formData = new FormData($(this)[0]);
        var baseURL1 = event.currentTarget.action;
        var parameters = $(this).serialize();
        $.ajax({
            type: "POST",
            url: baseURL1,
            data: formData,
            enctype: 'multipart/form-data',
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {
                location.reload();
                hideModal();
                if (result == "S1000") {
                    clientSideAlerts('alert alert-block alert-success', 'green', '400px;', 'icon-ok', 'Category configurations saved successfully!');
                } else {
                    clientSideAlerts('alert alert-block alert-error', 'red', '400px;', 'icon-remove', 'failed to add the Category configurations!');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
    });

    function validateCategoryName(value) {
        var maxLength = config["category.name.max.size"];
        if ($.trim(value).length > maxLength) {
            swal("Error", "Category name can have maximum of " + maxLength + " characters.", "error");
            return false;
        }
    }
</script>

<script>
    function categoryIcon(input) {
        var iconWidth = '<c:out value="${iconWidth}"/>';
        var iconHeight = '<c:out value="${iconWidth}"/>';
        var iconSize = '<c:out value="${iconSize}"/>';
        var file = input.files && input.files[0];
        var imageType = /image.*/;
        if (file.type.match(imageType)) {
            if (file) {
                var reader = new FileReader();
                var img = new Image();
                img.src = window.URL.createObjectURL(file);
                img.onload = function () {
                    var width = img.naturalWidth,
                            height = img.naturalHeight,
                            size = input.files[0].size / 1024;
                    window.URL.revokeObjectURL(img.src);
                    if (width <= iconWidth && height <= iconHeight) {
                        if (size <= iconSize) {
                            reader.onload = function (e) {
                                $('#imgPl')
                                        .attr('src', e.target.result);
                            };

                            reader.readAsDataURL(file);

                            var x = document.getElementById("imgNme").value.replace(/^.*[\\\/]/, '');
//                            document.getElementById("imgTxt").value = x;
                        }
                        else {
                            swal("Error", "<fmt:message code="admin.appstore.category.icon.size.error"/>", "error");
                            $('#imgNme').replaceWith($('#imgNme').val('').clone(true));
                        }
                    }
                    else {
                        swal("Error", "<fmt:message code="admin.appstore.category.icon.resolution.error"/>", "error");
                        $('#imgNme').replaceWith($('#imgNme').val('').clone(true));
                    }

                };

            }
        }
        else {
            swal("Error", "<fmt:message code="admin.appstore.category.icon.type.error"/>", "error");
            $('#imgNme').replaceWith($('#imgNme').val('').clone(true));
        }
    }
</script>

<style>
    img {
        max-width: 200px;
        max-height: 200px;
    }

    input[type=file] {
        padding: 10px;
        background: #f7f7f7;
    }
</style>

<script>
    function editCategoryDetails(input, id) {
        var iconWidth = '<c:out value="${iconWidth}"/>';
        var iconHeight = '<c:out value="${iconWidth}"/>';
        var iconSize = '<c:out value="${iconSize}"/>';
        var file = input.files && input.files[0];
        var imageType = /image.*/;
        if (file.type.match(imageType)) {
            if (file) {
                var reader = new FileReader();
                var img = new Image();
                img.src = window.URL.createObjectURL(file);
                img.onload = function () {
                    var width = img.naturalWidth,
                            height = img.naturalHeight,
                            size = input.files[0].size / 1024;
                    window.URL.revokeObjectURL(img.src);
                    if (width <= iconWidth && height <= iconHeight) {
                        if (size <= iconSize) {
                            reader.onload = function (e) {
                                $('#' + id + 'catIconEdit')
                                        .attr('src', e.target.result);
                            };

                            reader.readAsDataURL(file);

                            var x = document.getElementById(id + "imgCat").value.replace(/^.*[\\\/]/, '');
                            document.getElementById(id + "textCat").value = x;
                        }
                        else {
                            swal("Error", "<fmt:message code="admin.appstore.category.icon.size.error"/>", "error")
                            $('#' + id + 'imgCat').replaceWith($('#' + id + 'imgCat').val('').clone(true));
                        }
                    }
                    else {
                        swal("Error", "<fmt:message code="admin.appstore.category.icon.resolution.error"/>", "error");
                        $('#' + id + 'imgCat').replaceWith($('#' + id + 'imgCat').val('').clone(true));
                    }
                };

            }
        }
        else {
            swal("Error", "<fmt:message code="admin.appstore.category.icon.type.error"/>", "error");
            $('#' + id + 'imgCat').replaceWith($('#' + id + 'imgCat').val('').clone(true));
        }
    }
</script>
