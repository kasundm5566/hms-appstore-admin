<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>


<div class="row-fluid">
    <div class="span12">
        <button type="button" class="close right" data-dismiss="modal" aria-hidden="true"> x</button>
    </div>
</div>

<div class="row-fluid">
    <div class="span2">
        <div style="width:100px; height:100px;">
            <img src="<fmt:message code='admin.appstore.image.base.url'/><c:out value='${app.appIcon}'/>" style="width:100px;"/>
        </div>
    </div>
    <div class="span9">
        <div class="row-fluid">
            <div class="span12">
                <span style="font-size:16pt; margin-right:10px;"><c:out value="${app.name}"/></span>
                <a class="blue tooltip-info" href="#" data-rel="tooltip1"
                   title="This is the name of the application" style="text-decoration: none;">
                    <i class="icon-question-sign light-grey"></i>
                </a>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12">
                            <span style="margin-right:10px;">
                                <c:out value="${app.category}"/>
                            </span>
                <a class="blue tooltip-info" href="#" data-rel="tooltip1"
                   title="This is the category to which the application belongs to" style="text-decoration: none;">
                    <i class="icon-question-sign light-grey"></i>
                </a>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12">
                            <span id='<c:out value="${app.id}"/>AppStatusLabel_inView' class="label
                              <c:if test="${app.status=='Unpublish'}">label-warning </c:if>
                              <c:if test="${app.status=='Publish'}">label-success </c:if>"
                                  style="margin-right:10px;">
                <c:if test="${app.status != 'New'}">
                    <c:out value="${app.status}"/>ed
                </c:if>
                <c:if test="${app.status == 'New'}">
                    <c:out value="${app.status}"/>
                </c:if>
                </span>
                <a class="blue tooltip-info" href="#" data-rel="tooltip1"
                   title="This is the current state of the application on the app store" style="text-decoration: none;">
                    <i class="icon-question-sign light-grey"></i>
                </a>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12">
                <span style="margin-right:10px; color: #FFA200;">
                    <c:forEach begin="1" end="${app.rating}">
                        <i class='icon-star' style='color: #FFA200'></i>
                    </c:forEach>
                    <c:forEach begin="1" end="${5 - app.rating}">
                        <i class='icon-star-empty' style='color: #FFA200'></i>
                    </c:forEach>
                </span>
            </div>
        </div>

    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <blockquote>
            <p style="font-size:15px;"><c:out value="${app.shortDesc}"/></p>
        </blockquote>
    </div>
</div>

<div class="row-fluid">
    <div class="span3">
        <label class="control-label grey" style="text-align:right;">
            <fmt:message code="admin.appstore.app.view.form.charges.label"/>
        </label>
    </div>
    <div class="span9">

        <p><c:out value="${app.chargingLabel}"/>
            <a class="blue tooltip-info" href="#" data-rel="tooltip1"
               title="Charging amount for application usage or subscription" style="text-decoration: none;">
                <i class="icon-question-sign light-grey"></i>
            </a>
        </p>
    </div>
</div>

<div class="row-fluid">
    <div class="span3">
        <label class="control-label grey" style="text-align:right;">
            <fmt:message code="admin.appstore.app.view.form.charging.description"/>
        </label>
    </div>
    <div class="span9">

        <p><c:out value="${app.chargingDetails}"/>
            <a class="blue tooltip-info" href="#" data-rel="tooltip1"
               title="This is the detailed description of application charges" style="text-decoration: none;">
                <i class="icon-question-sign light-grey"></i>
            </a>
        </p>
    </div>
</div>

<div class="row-fluid">
    <div class="span3">
        <label class="control-label grey" style="text-align:right;">
            <fmt:message code="admin.appstore.app.view.form.detailed.description"/>
        </label>
    </div>
    <div class="span9">
        <p>
            <c:out value="${app.description}"/>
            <a class="blue tooltip-info" href="#" data-rel="tooltip1"
               title="A long description of the application" style="text-decoration: none;">
                <i class="icon-question-sign light-grey"></i>
            </a>
        </p>
    </div>
</div>


<div class="row-fluid">
    <div class="span3">
        <label class="control-label grey" style="text-align:right;">
            <fmt:message code="admin.appstore.app.view.form.instructions"/>
        </label>
    </div>
    <div class="span8">

        <c:forEach items="${app.instructions}" var="entry">
            <div class="well well-small">
                <div class="row-fluid">

                    <div class="span10">
                        <label class="control-label">
                            <c:set var="operatorDisplayName" value="operator.${entry.key}.display.name"/>
                            <b>
                                <fmt:message code="${fn:toLowerCase(operatorDisplayName)}"/>
                            </b>
                        </label>
                    </div>

                    <div class="span10" style="margin-left: 0">
                        <p><c:out value="${entry.value}"/>
                            <a class="blue tooltip-info" href="#" data-rel="tooltip1"
                               title="Instructions for subscribing, downloading or using the application can be added here for each operator"
                               style="text-decoration: none;">
                                <i class="icon-question-sign light-grey"></i>
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </c:forEach>

    </div>
</div>

<div class="row-fluid">
    <div class="span3">
        <label class="control-label grey" style="text-align:right;">
            <fmt:message code="admin.appstore.app.view.form.remarks.title"/>
        </label>
    </div>
    <div class="span9">
        <p>
            <c:out value="${app.remarks}"/>
            <a class="blue tooltip-info" href="#" data-rel="tooltip1"
               title="Latest remark for this app" style="text-decoration: none;">
                <i class="icon-question-sign light-grey"></i>
            </a>
        </p>
    </div>
</div>


    <div class="row-fluid" id='<c:out value="${app.id}"/>AppViewLabel'>
        <div class="span3">
            <label class="control-label grey">
                <fmt:message code="admin.appstore.app.view.form.images.title"/>
            </label>
        </div>
        <div class="span9">
        </div>
    </div>

    <div class="tabbable tabs-left" id='<c:out value="${app.id}"/>AppViewImgPanel'>
        <ul class="nav nav-tabs" id="myTab3">

            <c:if test="${appBannerFeatureEnable == true}">
                <li class="active">
                    <a data-toggle="tab" href="#<c:out value="${app.id}"/>bannerImgs">
                        <i class="icon-screenshot bigger-110"></i>
                        <fmt:message code="admin.appstore.app.view.form.banner.images.title"/>
                    </a>
                </li>
            </c:if>
            <c:if test="${app.downloadable == true}">

                <li class="">
                    <a data-toggle="tab" href="#<c:out value="${app.id}"/>screenImgs">
                        <i class="icon-screenshot bigger-110"></i>
                        <fmt:message code="admin.appstore.app.view.form.screens.images.title"/>
                    </a>
                </li>

                <li class="">
                    <a data-toggle="tab" href="#<c:out value="${app.id}"/>mobileImgs">
                        <i class="icon-mobile-phone bigger-110"></i>
                        <fmt:message code="admin.appstore.app.view.form.mobile.images.title"/>
                    </a>
                </li>

            </c:if>

        </ul>

        <div class="tab-content">

            <div id='<c:out value="${app.id}"/>bannerImgs' class="tab-pane active">
                <c:forEach items="${app.appBanner}" var="map">
                    <c:set var="bCaption" value="${map['caption']}"/>
                    <c:set var="bUrl" value="${map['url']}"/>
                    <img src='<fmt:message code='admin.appstore.image.base.url'/><c:out value="${bUrl}"/>' class="img-polaroid appViewImage"/>
                </c:forEach>
            </div>

            <c:if test="${app.downloadable==true}">
                <div id='<c:out value="${app.id}"/>screenImgs' class="tab-pane">
                    <c:set var="hasScreenShots" value="false"/>

                    <c:forEach items="${app.screenShots}" var="map">
                        <c:set var="caption" value="${map['caption']}"/>
                        <c:set var="url" value="${map['url']}"/>
                        <c:set var="mobile" value="${fn:containsIgnoreCase(caption, 'mobile')}"/>
                        <c:if test="${mobile == false}">
                            <c:set var="hasScreenShots" value="true"/>
                            <img src='<fmt:message code='admin.appstore.image.base.url'/><c:out value="${url}"/>'
                                 class="img-polaroid appViewImage"/>
                        </c:if>
                    </c:forEach>

                    <c:if test="${hasScreenShots == false}">
                        <div class="well well-small">No images available</div>
                    </c:if>
                </div>

                <div id='<c:out value="${app.id}"/>mobileImgs' class="tab-pane">
                    <c:set var="hasMobileScreenShots" value="false"/>

                    <c:forEach items="${app.screenShots}" var="map">
                        <c:set var="caption" value="${map['caption']}"/>
                        <c:set var="url" value="${map['url']}"/>
                        <c:set var="mobile" value="${fn:containsIgnoreCase(caption, 'mobile')}"/>
                        <c:if test="${mobile == true}">
                            <c:set var="hasMobileScreenShots" value="true"/>
                            <img src='<fmt:message code='admin.appstore.image.base.url'/><c:out value="${url}"/>'
                                 class="img-polaroid appViewImage"/>
                        </c:if>
                    </c:forEach>

                    <c:if test="${hasMobileScreenShots == false}">
                        <div class="well well-small">No images available</div>
                    </c:if>
                </div>
            </c:if>
        </div>
    </div>
