<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<link rel="stylesheet" href="resources/css/bootstrap-multiselect.css" media="screen"/>

<form id="<c:out value="${app.id}"/>editAppModal" name='<c:out value="${app.id}"/>editAppModal'
      class="appEdit" action="updateApp" method="POST" style="width:900px;">
<div class="row-fluid">
    <div class="span3">
        <label class="control-label" style="text-align:right; font-size:12px;">
            <b><fmt:message code="admin.appstore.app.edit.form.app.id.label"/></b>
        </label>
    </div>
    <div class="span7">
        <c:out value="${app.id}"/>
        <input name="appId" type="hidden" style="display: none;width: 0;" value="${app.id}"/>
    </div>

</div>

<div class="row-fluid">
    <div class="span3">
        <label class="control-label" style="text-align:right; font-size:12px;">
            <b><fmt:message code="admin.appstore.app.edit.form.app.name.label"/></b>
        </label>
    </div>
    <div class="span7">
        <input type="text" value='<c:out value="${app.name}"/>' name="appName" maxlength="25" required/>
        <a class="blue tooltip-info" href="#" data-rel="tooltip1"
           title="This is the publish name of the application" style="text-decoration: none;">
            <i class="icon-question-sign light-grey"></i>
        </a>
    </div>
</div>

<div class="row-fluid">
    <div class="span3">
        <label class="control-label" style="text-align:right; font-size:12px;">
            <b><fmt:message code="admin.appstore.app.edit.form.app.status.label"/></b>
        </label>
    </div>
    <div class="span7">
        <c:if test="${app.status != 'New'}">
            <c:out value="${app.status}"/>ed
        </c:if>
        <c:if test="${app.status == 'New'}">
            <c:out value="${app.status}"/>
        </c:if>
        <a class="blue tooltip-info" href="#" data-rel="tooltip1"
           title="This is the current state of the application on the app store" style="text-decoration: none;">
            <i class="icon-question-sign light-grey"></i>
        </a>
    </div>
</div>

<div class="row-fluid">
    <div class="span3">
        <label class="control-label" style="text-align:right; font-size:12px;">
            <b><fmt:message code="admin.appstore.app.edit.form.app.category.label"/></b>
        </label>
    </div>
    <div class="span7">
        <input type="hidden" name="selectedCategories" id="selectedCategoriesHid">
        <select class="span4" id="multiCatSelect" multiple="multiple" name="categories">
            <c:set var="appCategories" value="${fn:split(app.category, ',')}"/>
            <c:forEach items="${categories}" var="category">
                <c:set var="isFound" value="false"/>
                <c:forEach items="${appCategories}" var="appCategory">
                    <c:if test="${fn:trim(appCategory) eq category}">
                        <option selected value="${category}">${category}</option>
                        <c:set var="isFound" value="true"/>
                    </c:if>
                </c:forEach>
                <c:if test="${isFound eq false}">
                    <option value="${category}">${category}</option>
                </c:if>
            </c:forEach>
        </select>
        <a class="blue tooltip-info" href="#" data-rel="tooltip1"
           title="This is the category to which the application belongs to" style="text-decoration: none;">
            <i class="icon-question-sign light-grey"></i>
        </a>
    </div>
    <div class="span3 categoryErrorLabel"></div>
    <div class="span7 categoryErrorLabel" style="margin-left: 0px;">
        <label class="control-label"
               style="color: #B91D47; font-family: sans-serif; font-size:14px;">
            <fmt:message code="admin.appstore.app.edit.category.error"/>
        </label>
    </div>
</div>

<div class="row-fluid">
    <div class="span3">
        <label class="control-label" style="text-align:right; font-size:12px;">
            <b>Labels </b>
        </label>
    </div>
    <div class="span7">
        <c:forEach items="${app.labels}" var="label" varStatus="status">
            <select name="label_${status.count}" class="span4" required>
                <c:forEach items="${categories}" var="category">
                    <option value="${category}" ${category==label?"selected":""}>${category}</option>
                </c:forEach>
            </select>
            <%--<input type="text" value="${label}" class="span4" name="label_${status.count}">--%>
        </c:forEach>
    </div>
</div>

<div class="row-fluid">
    <div class="span2">
    </div>
    <div class="span8">
        <hr style="height:1px; border:none; background-color:#b5b5b5;width: 770px; margin-left: -108px;"/>
    </div>
</div>

<div class="row-fluid">
    <div class="span3">
        <label class="control-label" style="text-align:right; font-size:12px;">
            <b><fmt:message code="admin.appstore.app.edit.form.app.charges.label"/></b>
        </label>
    </div>
    <div class="span7">

    </div>
</div>

<div class="row-fluid">
    <div class="span3">
        <label class="control-label" style="text-align:right; font-size:12px;">
            <fmt:message code="admin.appstore.app.edit.form.display.ncs.label"/>
        </label>
    </div>
    <div class="span7">
        <c:out value="${app.chargingLabel}"/>
        <a class="blue tooltip-info" href="#" data-rel="tooltip1"
           title="Charging amount for application usage or subscription" style="text-decoration: none;">
            <i class="icon-question-sign light-grey"></i>
        </a>
    </div>
</div>

<c:forEach items="${chargingData}" var="data">
    <c:choose>
        <c:when test="${data.ncsType eq \"subscription\"}">
            <c:set var="subscriptionChargingType" value="${data.chargingType}"/>
            <c:choose>
                <c:when test="${subscriptionChargingType eq \"flat\"}">
                    <c:set var="subscriptionAmount" value="${data.amount}"/>
                    <c:set var="subscriptionFrequency" value="${data.frequency}"/>
                </c:when>
            </c:choose>
        </c:when>
        <c:when test="${data.ncsType eq \"downloadable\"}">
            <c:set var="downloadableChargingType" value="${data.chargingType}"/>
            <c:choose>
                <c:when test="${downloadableChargingType eq \"flat\"}">
                    <c:set var="downloadableAmount" value="${data.amount}"/>
                </c:when>
            </c:choose>
        </c:when>
        <c:when test="${data.ncsType eq \"vodafone-sms\"}">
            <c:choose>
                <c:when test="${(data.chargingType eq \"flat\") and (data.direction eq \"mo\")}">
                    <c:set var="smsMoAmount" value="${data.amount}"/>
                    <c:set var="smsMoChargingType" value="${data.chargingType}"/>
                </c:when>
                <c:when test="${(data.chargingType eq \"flat\") and (data.direction eq \"mt\")}">
                    <c:set var="smsMtAmount" value="${data.amount}"/>
                    <c:set var="smsMtChargingType" value="${data.chargingType}"/>
                </c:when>
            </c:choose>
        </c:when>
        <c:when test="${data.ncsType eq \"vodafone-ussd\"}">
            <c:choose>
                <c:when test="${(data.chargingType eq \"flat\") and (data.direction eq \"mo\")}">
                    <c:set var="ussdMoAmount" value="${data.amount}"/>
                    <c:set var="ussdMoChargingType" value="${data.chargingType}"/>
                </c:when>
                <c:when test="${(data.chargingType eq \"flat\") and (data.direction eq \"mt\")}">
                    <c:set var="ussdMtAmount" value="${data.amount}"/>
                    <c:set var="ussdMtChargingType" value="${data.chargingType}"/>
                </c:when>
            </c:choose>
        </c:when>
    </c:choose>
</c:forEach>

<c:forEach items="${chargingMessageFormats}" var="format">
    <c:choose>
        <c:when test="${format.ncsType eq \"subscription\"}">
            <c:set var="subscriptionMsgFmt" value="${format.messageFormat}"/>
            <input type="hidden" id="subMsgFmt" value="<c:out value="${subscriptionMsgFmt}"/>">
        </c:when>
        <c:when test="${format.ncsType eq \"downloadable\"}">
            <c:set var="downloadableMsgFmt" value="${format.messageFormat}"/>
            <input type="hidden" id="downMsgFmt" value="<c:out value="${downloadableMsgFmt}"/>">
        </c:when>
        <c:when test="${format.ncsType eq \"sms-mo\"}">
            <c:set var="smsMoMsgFmt" value="${format.messageFormat}"/>
            <input type="hidden" id="smsMoMsgFmt" value="<c:out value="${smsMoMsgFmt}"/>">
        </c:when>
        <c:when test="${format.ncsType eq \"sms-mt\"}">
            <c:set var="smsMtMsgFmt" value="${format.messageFormat}"/>
            <input type="hidden" id="smsMtMsgFmt" value="<c:out value="${smsMtMsgFmt}"/>">
        </c:when>
        <c:when test="${format.ncsType eq \"ussd\"}">
            <c:set var="ussdMsgFmt" value="${format.messageFormat}"/>
            <input type="hidden" id="ussdMsgFmt" value="<c:out value="${ussdMsgFmt}"/>">
        </c:when>
        <c:when test="${format.ncsType eq \"free\"}">
            <c:set var="freeMsgFmt" value="${format.messageFormat}"/>
            <input type="hidden" id="freeMsgFmt" value="<c:out value="${freeMsgFmt}"/>">
        </c:when>
    </c:choose>
</c:forEach>

<div class="row-fluid">
<label hidden></label>
<c:forEach items="${app.ncs}" var="ncs">
<c:if test="${ncs eq \"subscription\"}">
    <div class="span3">
        <label style="display: inline-block; margin-right: 24px; float: right;font-size:12px;" class="control-label">
            <fmt:message code="admin.appstore.app.edit.form.ncs.subscription.label"/>
        </label>
    </div>
    <div class="span7" style="margin-left: 0px;">
        <div class="span10">
            <div class="span3">
                <label class="control-label" style="display: inline-block; font-size: 12px;">
                    <fmt:message code="admin.appstore.app.edit.form.charging.type.label"/>
                </label>
                <select id="cmbSubsType" name="cmbSubsChargeType" style="width:80px; margin-right: 10px;">
                    <c:choose>
                        <c:when test="${subscriptionChargingType eq \"free\"}">
                            <option selected value="free"><fmt:message
                                    code="admin.appstore.app.edit.form.charging.type.free.option"/></option>
                            <option value="flat"><fmt:message
                                    code="admin.appstore.app.edit.form.charging.type.flat.option"/></option>
                        </c:when>
                        <c:when test="${subscriptionChargingType eq \"flat\"}">
                            <option value="free"><fmt:message
                                    code="admin.appstore.app.edit.form.charging.type.free.option"/></option>
                            <option selected value="flat"><fmt:message
                                    code="admin.appstore.app.edit.form.charging.type.flat.option"/></option>
                        </c:when>
                        <c:otherwise>
                            <option selected value="free"><fmt:message
                                    code="admin.appstore.app.edit.form.charging.type.free.option"/></option>
                            <option value="flat"><fmt:message
                                    code="admin.appstore.app.edit.form.charging.type.flat.option"/></option>
                        </c:otherwise>
                    </c:choose>
                </select>
            </div>
            <div class="span3">
                <label class="control-label" style="display: inline-block; font-size: 12px;">
                    <fmt:message code="admin.appstore.app.edit.form.charging.amount.label"/>
                </label>

                <input id="txtSubsAmnt" name="txtSubsAmount" style="width:50px; margin-right: 2px;" type="text"
                       value="<c:out value="${subscriptionAmount}"/>">
            </div>
            <div class="span3">
                <label class="control-label" style="display: inline-block; font-size: 12px;">
                    <fmt:message code="admin.appstore.app.edit.form.charging.frequency.label"/>
                </label>
                <select id="cmbSubsfr" name="cmbSubsFreq" style="width:100px;">
                    <c:choose>
                        <c:when test="${subscriptionFrequency eq \"weekly\"}">
                            <option selected value="weekly"><fmt:message
                                    code="admin.appstore.app.edit.form.charging.frequency.weekly.option"/></option>
                            <option value="monthly"><fmt:message
                                    code="admin.appstore.app.edit.form.charging.frequency.monthly.option"/></option>
                            <option value="daily"><fmt:message
                                    code="admin.appstore.app.edit.form.charging.frequency.daily.option"/></option>
                        </c:when>
                        <c:when test="${subscriptionFrequency eq \"monthly\"}">
                            <option value="weekly"><fmt:message
                                    code="admin.appstore.app.edit.form.charging.frequency.weekly.option"/></option>
                            <option selected value="monthly"><fmt:message
                                    code="admin.appstore.app.edit.form.charging.frequency.monthly.option"/></option>
                            <option value="daily"><fmt:message
                                    code="admin.appstore.app.edit.form.charging.frequency.daily.option"/></option>
                        </c:when>
                        <c:when test="${subscriptionFrequency eq \"daily\"}">
                            <option value="weekly"><fmt:message
                                    code="admin.appstore.app.edit.form.charging.frequency.weekly.option"/></option>
                            <option value="monthly"><fmt:message
                                    code="admin.appstore.app.edit.form.charging.frequency.monthly.option"/></option>
                            <option selected value="daily"><fmt:message
                                    code="admin.appstore.app.edit.form.charging.frequency.daily.option"/></option>
                        </c:when>
                        <c:otherwise>
                            <option selected value="weekly"><fmt:message
                                    code="admin.appstore.app.edit.form.charging.frequency.weekly.option"/></option>
                            <option value="monthly"><fmt:message
                                    code="admin.appstore.app.edit.form.charging.frequency.monthly.option"/></option>
                            <option value="daily"><fmt:message
                                    code="admin.appstore.app.edit.form.charging.frequency.daily.option"/></option>
                        </c:otherwise>
                    </c:choose>
                </select>
            </div>
        </div>
    </div>
    <div class="span3 subscriptionChargingErrorLabel"></div>
    <div class="span7 subscriptionChargingErrorLabel" style="margin-left: 0px;">
        <label class="control-label"
               style="color: #B91D47; font-family: sans-serif; font-size:14px;">
            <fmt:message code="admin.appstore.app.edit.form.subscription.charging.details.error"/>
        </label>
    </div>
</c:if>
<c:if test="${ncs eq \"downloadable\"}">
    <div class="span3">
        <label style="display: inline-block; margin-right: 24px; float: right;font-size:12px;" class="control-label">
            <fmt:message code="admin.appstore.app.edit.form.ncs.downloadable.label"/>
        </label>
    </div>
    <div class="span7" style="margin-left: 0px;">
        <div class="span10">
            <div class="span3">
                <label class="control-label" style="display: inline-block; font-size: 12px;">
                    <fmt:message code="admin.appstore.app.edit.form.charging.type.label"/>
                </label>
                <select id="cmbDownType" name="cmbDownChargeType" style="width:80px; margin-right: 10px;">
                    <c:choose>
                        <c:when test="${downloadableChargingType eq \"free\"}">
                            <option selected value="free"><fmt:message
                                    code="admin.appstore.app.edit.form.charging.type.free.option"/></option>
                            <option value="flat"><fmt:message
                                    code="admin.appstore.app.edit.form.charging.type.flat.option"/></option>
                        </c:when>
                        <c:when test="${downloadableChargingType eq \"flat\"}">
                            <option value="free"><fmt:message
                                    code="admin.appstore.app.edit.form.charging.type.free.option"/></option>
                            <option selected value="flat"><fmt:message
                                    code="admin.appstore.app.edit.form.charging.type.flat.option"/></option>
                        </c:when>
                        <c:otherwise>
                            <option selected value="free"><fmt:message
                                    code="admin.appstore.app.edit.form.charging.type.free.option"/></option>
                            <option value="flat"><fmt:message
                                    code="admin.appstore.app.edit.form.charging.type.flat.option"/></option>
                        </c:otherwise>
                    </c:choose>
                </select>
            </div>
            <div class="span3">
                <label class="control-label" style="display: inline-block; font-size: 12px;">
                    <fmt:message code="admin.appstore.app.edit.form.charging.amount.label"/>
                </label>
                <input id="txtDownAmnt" name="txtDownAmount" style="width:50px; margin-right: 10px;" type="text"
                       value="<c:out value="${downloadableAmount}"/>">
            </div>
        </div>
    </div>
    <div class="span3 downloadableChargingErrorLabel"></div>
    <div class="span7 downloadableChargingErrorLabel" style="margin-left: 0px;">
        <label class="control-label"
               style="color: #B91D47; font-family: sans-serif; font-size:14px;">
            <fmt:message code="admin.appstore.app.edit.form.downloadable.charging.details.error"/>
        </label>
    </div>
</c:if>
<c:if test="${ncs eq \"vodafone-sms\"}">
    <div class="span3">
        <label style="display: inline-block; margin-right: 24px; float: right;font-size:12px;" class="control-label">
            <fmt:message code="admin.appstore.app.edit.form.ncs.sms.mo.label"/>
        </label>
    </div>
    <div class="span7" style="margin-left: 0px;">
        <div>
            <div class="span10">

                <div class="span3">
                    <label class="control-label" style="display: inline-block; font-size: 12px;">
                        <fmt:message code="admin.appstore.app.edit.form.charging.type.label"/>
                    </label>
                    <select id="cmbSmsMoType" name="cmbSmsMoChargeType" style="width:80px; margin-right: 10px;">
                        <c:choose>
                            <c:when test="${smsMoChargingType eq \"free\"}">
                                <option selected value="free"><fmt:message
                                        code="admin.appstore.app.edit.form.charging.type.free.option"/></option>
                                <option value="flat"><fmt:message
                                        code="admin.appstore.app.edit.form.charging.type.flat.option"/></option>
                            </c:when>
                            <c:when test="${smsMoChargingType eq \"flat\"}">
                                <option value="free"><fmt:message
                                        code="admin.appstore.app.edit.form.charging.type.free.option"/></option>
                                <option selected value="flat"><fmt:message
                                        code="admin.appstore.app.edit.form.charging.type.flat.option"/></option>
                            </c:when>
                            <c:otherwise>
                                <option selected value="free"><fmt:message
                                        code="admin.appstore.app.edit.form.charging.type.free.option"/></option>
                                <option value="flat"><fmt:message
                                        code="admin.appstore.app.edit.form.charging.type.flat.option"/></option>
                            </c:otherwise>
                        </c:choose>
                    </select>
                </div>
                <div class="span3">
                    <label class="control-label" style="display: inline-block; font-size: 12px;">
                        <fmt:message code="admin.appstore.app.edit.form.charging.amount.label"/>
                    </label>
                    <input id="txtSmsMoAmnt" name="txtSmsMoAmount" style="width:50px; margin-right: 10px;" type="text"
                           value="<c:out value="${smsMoAmount}"/>">
                </div>
            </div>
        </div>
        <div class="span7 smsMoChargingErrorLabel" style="margin-left: 0px;">
            <label class="control-label"
                   style="color: #B91D47; font-family: sans-serif; font-size:14px;">
                <fmt:message code="admin.appstore.app.edit.form.sms.mo.charging.details.error"/>
            </label>
        </div>
    </div>

    <div class="span3">
        <label style="display: inline-block; margin-right: 24px; float: right;font-size:12px;" class="control-label">
            <fmt:message code="admin.appstore.app.edit.form.ncs.sms.mt.label"/>
        </label>
    </div>
    <div class="span7" style="margin-left: 0px;">
        <div>
            <div class="span10">

                <div class="span3">
                    <label class="control-label" style="display: inline-block; font-size: 12px;">
                        <fmt:message code="admin.appstore.app.edit.form.charging.type.label"/>
                    </label>
                    <select id="cmbSmsMtType" name="cmbSmsMtChargeType" style="width:80px; margin-right: 10px;">
                        <c:choose>
                            <c:when test="${smsMtChargingType eq \"free\"}">
                                <option selected value="free"><fmt:message
                                        code="admin.appstore.app.edit.form.charging.type.free.option"/></option>
                                <option value="flat"><fmt:message
                                        code="admin.appstore.app.edit.form.charging.type.flat.option"/></option>
                            </c:when>
                            <c:when test="${smsMtChargingType eq \"flat\"}">
                                <option value="free"><fmt:message
                                        code="admin.appstore.app.edit.form.charging.type.free.option"/></option>
                                <option selected value="flat"><fmt:message
                                        code="admin.appstore.app.edit.form.charging.type.flat.option"/></option>
                            </c:when>
                            <c:otherwise>
                                <option selected value="free"><fmt:message
                                        code="admin.appstore.app.edit.form.charging.type.free.option"/></option>
                                <option value="flat"><fmt:message
                                        code="admin.appstore.app.edit.form.charging.type.flat.option"/></option>
                            </c:otherwise>
                        </c:choose>
                    </select>
                </div>

                <div class="span3">
                    <label class="control-label" style="display: inline-block; font-size: 12px;">
                        <fmt:message code="admin.appstore.app.edit.form.charging.amount.label"/>
                    </label>
                    <input id="txtSmsMtAmnt" name="txtSmsMtAmount" style="width:50px; margin-right: 10px;" type="text"
                           value="<c:out value="${smsMtAmount}"/>">
                </div>
            </div>
        </div>
        <div class="span7 smsMtChargingErrorLabel" style="margin-left: 0px;">
            <label class="control-label"
                   style="color: #B91D47; font-family: sans-serif; font-size:14px;">
                <fmt:message code="admin.appstore.app.edit.form.sms.mt.charging.details.error"/>
            </label>
        </div>
    </div>
</c:if>
<c:if test="${ncs eq \"vodafone-ussd\"}">

    <div class="span3">
        <label style="display: inline-block; margin-right: 24px; float: right;font-size:12px;" class="control-label">
            <fmt:message code="admin.appstore.app.edit.form.ncs.ussd.mo.label"/>
        </label>
    </div>

    <div class="span7" style="margin-left: 0px;">
        <div>
            <div class="span10">
                <div class="span3">
                    <label class="control-label" style="display: inline-block; font-size: 12px;">
                        <fmt:message code="admin.appstore.app.edit.form.charging.type.label"/>
                    </label>
                    <select id="cmbUssdMoType" name="cmbUssdMoChargeType" style="width:80px; margin-right: 10px;">
                        <c:choose>
                            <c:when test="${ussdMoChargingType eq \"free\"}">
                                <option selected value="free"><fmt:message
                                        code="admin.appstore.app.edit.form.charging.type.free.option"/></option>
                                <option value="flat"><fmt:message
                                        code="admin.appstore.app.edit.form.charging.type.flat.option"/></option>
                            </c:when>
                            <c:when test="${ussdMoChargingType eq \"flat\"}">
                                <option value="free"><fmt:message
                                        code="admin.appstore.app.edit.form.charging.type.free.option"/></option>
                                <option selected value="flat"><fmt:message
                                        code="admin.appstore.app.edit.form.charging.type.flat.option"/></option>
                            </c:when>
                            <c:otherwise>
                                <option selected value="free"><fmt:message
                                        code="admin.appstore.app.edit.form.charging.type.free.option"/></option>
                                <option value="flat"><fmt:message
                                        code="admin.appstore.app.edit.form.charging.type.flat.option"/></option>
                            </c:otherwise>
                        </c:choose>
                    </select>
                </div>

                <div class="span3">
                    <label class="control-label" style="display: inline-block; font-size: 12px;">
                        <fmt:message code="admin.appstore.app.edit.form.charging.amount.label"/>
                    </label>
                    <input id="txtUssdMoAmnt" name="txtUssdMoAmount" style="width:50px; margin-right: 10px;" type="text"
                           value="<c:out value="${ussdMoAmount}"/>">
                </div>
            </div>
        </div>
        <div class="span7 ussdMoChargingErrorLabel" style="margin-left: 0px;">
            <label class="control-label"
                   style="color: #B91D47; font-family: sans-serif; font-size:14px;">
                <fmt:message code="admin.appstore.app.edit.form.ussd.mo.charging.details.error"/>
            </label>
        </div>
    </div>

    <div class="span3">
        <label style="display: inline-block; margin-right: 24px; float: right;font-size:12px;" class="control-label">
            <fmt:message code="admin.appstore.app.edit.form.ncs.ussd.mt.label"/>
        </label>
    </div>

    <div class="span7" style="margin-left: 0px;">
        <div>
            <div class="span10">
                <div class="span3">
                    <label class="control-label" style="display: inline-block; font-size: 12px;">
                        <fmt:message code="admin.appstore.app.edit.form.charging.type.label"/>
                    </label>
                    <select id="cmbUssdMtType" name="cmbUssdMtChargeType" style="width:80px; margin-right: 10px;">
                        <c:choose>
                            <c:when test="${ussdMtChargingType eq \"free\"}">
                                <option selected value="free"><fmt:message
                                        code="admin.appstore.app.edit.form.charging.type.free.option"/></option>
                                <option value="flat"><fmt:message
                                        code="admin.appstore.app.edit.form.charging.type.flat.option"/></option>
                            </c:when>
                            <c:when test="${ussdMtChargingType eq \"flat\"}">
                                <option value="free"><fmt:message
                                        code="admin.appstore.app.edit.form.charging.type.free.option"/></option>
                                <option selected value="flat"><fmt:message
                                        code="admin.appstore.app.edit.form.charging.type.flat.option"/></option>
                            </c:when>
                            <c:otherwise>
                                <option selected value="free"><fmt:message
                                        code="admin.appstore.app.edit.form.charging.type.free.option"/></option>
                                <option value="flat"><fmt:message
                                        code="admin.appstore.app.edit.form.charging.type.flat.option"/></option>
                            </c:otherwise>
                        </c:choose>
                    </select>
                </div>

                <div class="span3">
                    <label class="control-label" style="display: inline-block; font-size: 12px;">
                        <fmt:message code="admin.appstore.app.edit.form.charging.amount.label"/>
                    </label>
                    <input id="txtUssdMtAmnt" name="txtUssdMtAmount" style="width:50px; margin-right: 10px;" type="text"
                           value="<c:out value="${ussdMtAmount}"/>">
                </div>
            </div>
        </div>
        <div class="span7 ussdMtChargingErrorLabel" style="margin-left: 0px;">
            <label class="control-label"
                   style="color: #B91D47; font-family: sans-serif; font-size:14px;">
                <fmt:message code="admin.appstore.app.edit.form.ussd.mt.charging.details.error"/>
            </label>
        </div>
    </div>
</c:if>
</c:forEach>
</div>


<div class="row-fluid">
    <div class="span3">
        <label class="control-label" style="text-align:right; font-size:12px;">
            <fmt:message code="admin.appstore.app.edit.form.app.charging.description.label"/>
        </label>
    </div>
    <div id="chargingDetailsDiv" class="span7">
        <c:out value="${app.chargingDetails}"/>
        <a class="blue tooltip-info" href="#" data-rel="tooltip1"
           title="This is the detailed description of application charges" style="text-decoration: none;">
            <i class="icon-question-sign light-grey"></i>
        </a>
    </div>
</div>

<div class="row-fluid">
    <div class="span2">
    </div>
    <div class="span8">
        <hr style="height:1px; border:none; background-color:#b5b5b5;width: 770px; margin-left: -108px;"/>
    </div>
</div>

<div class="row-fluid">
    <div class="span3">
        <label class="control-label" style="text-align:right; font-size:12px;">
            <b><fmt:message code="admin.appstore.app.edit.form.app.summarized.description.label"/></b>
        </label>
    </div>
    <div class="span7">
        <input type="text" value='<c:out value="${app.shortDesc}"/>' style="width: 196px;" name="shortDesc" required/>
        <a class="blue tooltip-info" href="#" data-rel="tooltip1"
           title="A short description of the application" style="text-decoration: none;">
            <i class="icon-question-sign light-grey"></i>
        </a>
    </div>
</div>

<div class="row-fluid">
    <div class="span3">
        <label class="control-label" style="text-align:right; font-size:12px;">
            <b><fmt:message code="admin.appstore.app.edit.form.app.detailed.description.label"/></b>
        </label>
    </div>
    <div class="span7">
        <textarea name="description" style="max-width: 345px;width:345px;" required>${app.description}</textarea>
        <a class="blue tooltip-info" href="#" data-rel="tooltip1"
           title="A long description of the application" style="text-decoration: none;">
            <i class="icon-question-sign light-grey"></i>
        </a>
    </div>
</div>

<div class="row-fluid">
    <div class="span3">
        <label class="control-label" style="text-align:right; font-size:12px;">
            <b><fmt:message code="admin.appstore.app.edit.form.app.instructions.label"/></b>
        </label>
    </div>

    <div class="span9">
        <c:forEach items="${app.instructions}" var="entry">
            <input type="hidden" class="hidden" value="${entry.key}" name="operators"/>

            <div class="row-fluid">
                <div class="span10">
                    <c:set var="operatorDisplayName" value="operator.${entry.key}.display.name"/>
                    <strong><fmt:message code="${fn:toLowerCase(operatorDisplayName)}"/> &nbsp;:</strong>
                </div>

                <div class="span10" style="margin-left: 0;">
                    <div class="row-fluid">
                        <textarea name="instructions.${entry.key}" class="span11"
                                  style="word-wrap: break-word; resize: vertical; height: 80px; margin-right: 3px;"
                                  required><c:out value="${entry.value}"/></textarea>

                        <a class="blue tooltip-info" href="#" data-rel="tooltip1"
                           title="Instructions for subscribing, downloading or using the application can be added here for each operator."
                           style="text-decoration: none; vertical-align: text-top; float: right;">
                            <i class="icon-question-sign light-grey"></i>
                        </a>

                    </div>
                </div>

            </div>
        </c:forEach>
    </div>
</div>

<div class="row-fluid">
    <div class="span3">
        <label class="control-label" style="text-align:right; font-size:12px;">
            <b><fmt:message code="admin.appstore.app.edit.form.app.remarks.label"/></b>
        </label>
    </div>
    <div class="span7">
        <p>
            <c:out value="${app.remarks}"/>
            <a class="blue tooltip-info" href="#" data-rel="tooltip1"
               title="Latest remark for this app" style="text-decoration: none;">
                <i class="icon-question-sign light-grey"></i>
            </a>
        </p>
    </div>
</div>

<%--<c:if test="${app.downloadable == true}">--%>
<div class="row-fluid">
<div class="span3">
    <label class="control-label" style="text-align:right; font-size:12px;">
        <b><fmt:message code="admin.appstore.app.edit.form.app.images.label"/></b>
    </label>
</div>
<div class="span7">

<%--image uploading tabs start--%>
<div class="tabbable" style="width:550px;">

<ul class="nav nav-tabs" id="myTab">
    <li class="active" id="<c:out value="${app.id}"/>webTabClick">
        <a data-toggle="tab" href="#<c:out value="${app.id}"/>webImg">
            <fmt:message code="admin.appstore.app.edit.form.app.web.images.label"/>
        </a>
    </li>

    <c:if test="${appBannerFeatureEnable == true}">
        <li class="" id="<c:out value="${app.id}"/>bannerTabClick">
            <a data-toggle="tab" href="#<c:out value="${app.id}"/>bannerImg">
                <fmt:message code="admin.appstore.app.edit.form.app.web.banner.images.label"/>
            </a>
        </li>
    </c:if>

    <c:if test="${app.downloadable == true}">
        <li class="" id="<c:out value="${app.id}"/>scrnTabClick">
            <a data-toggle="tab" href="#<c:out value="${app.id}"/>ScrnShots">
                <fmt:message code="admin.appstore.app.edit.form.app.screens.label"/>
            </a>
        </li>

        <li class="" id="<c:out value="${app.id}"/>mobTabClick">
            <a data-toggle="tab" href="#<c:out value="${app.id}"/>mobImg">
                <fmt:message code="admin.appstore.app.edit.form.app.mobile.images.label"/>
            </a>
        </li>
    </c:if>
</ul>

<div class="tab-content">

<div id="<c:out value="${app.id}"/>webImg" class="tab-pane active">
    <div class="row-fluid">
        <div class="span2">
            <label class="control-label"
                   style="text-align:right; font-size:12px;">
                <b><fmt:message code="admin.appstore.app.edit.form.app.instructions.label"/></b>
            </label>
        </div>
        <div class="span9">
            <fmt:message code="admin.appstore.app.edit.form.app.icon.instruction"/>
        </div>
    </div>

    <div class="row-fluid" style="margin-top: 10px;">
        <div class="span2">
            <label class="control-label" style="text-align:right; font-size:12px;">
                <b><fmt:message code="admin.appstore.app.edit.form.app.images.upload.image.file.label"/></b>
            </label>
        </div>
        <div class="span9">

            <div class='image-select-class' id=''
                 onclick="return getFile('<c:out value="${app.id}"/>IconAppImage')">
                <img src="<fmt:message code='admin.appstore.image.base.url'/><c:out value='${app.appIcon}'/>"
                     id='<c:out value="${app.id}"/>IconSelectedImgPreview'
                     class="selectedImage">

                <div id='icon_defaultImage_1<c:out value="${app.id}"/>'>
                    <i class="icon-picture bigger-250 image-select-icon"></i>
                    <br/>Select image
                </div>

                <%--TODO need to uncomment these thing for future progress bar--%>
                <div id='icon_progressArea_1<c:out value="${app.id}"/>' style="display: none;">
                    <img src='./resources/img/loading.gif' class='loading_spinner'>

                    <div class="progress progress-mini">
                        <div id='icon_progressBar_1<c:out value="${app.id}"/>'
                             class="progressBar progress-bar progress-danger" style="width:0%;"></div>
                    </div>
                </div>
            </div>

            <%--Begining of old image select and upload--%>

            <%--<button class="hmsSearchButton" style="color:#464646;"
                    onclick="return getFile('<c:out value="${app.name}"/>IconAppImage')">
                <i class="icon-picture"></i> Select Image
            </button>--%>

            <%--Error notification in file uploading--%>
            <br/>

            <div style="float: left;" class="red appEditErrorNotification span12"
                 id='<c:out value="${app.id}"/>imgUploadingErrorIcon'></div>

            <input style="display: none;" type='text' value=''
                   id='<c:out value="${app.id}"/>IconImgStr' name='icon_1'/>

            <%--image preview div--%>
            <input type="file" id='<c:out value="${app.id}"/>IconAppImage' style="display: none;"
                   onchange="preview(this, '<c:out value="${app.id}"/>IconSelectedImgPreview', '<c:out
                           value="${app.id}"/>IconImgStr', '<c:out
                           value="${app.id}"/>IconSelectedImgOriginal',
                           'Icon',
                           '<c:out value="${app.id}"/>',
                           'icon_1',
                           'icon_progressArea_1<c:out value="${app.id}"/>',
                           'icon_progressBar_1<c:out value="${app.id}"/>',
                           'icon_defaultImage_1<c:out value="${app.id}"/>')">

            <img style="display: none;" src='' id='<c:out value="${app.id}"/>IconSelectedImgOriginal'
                 style="width: auto; height:auto;">

        </div>
    </div>
</div>

<%--insert app banner--%>
<div id="<c:out value="${app.id}"/>bannerImg" class="tab-pane">
    <div class="row-fluid">
        <div class="span2">
            <label class="control-label" style="text-align:right; font-size:12px;">
                <b><fmt:message code="admin.appstore.app.edit.form.app.instructions.label"/></b>
            </label>
        </div>
        <div class="span9">
            <fmt:message code="admin.appstore.app.edit.form.app.banner.instruction"/>
        </div>
    </div>

    <div class="row-fluid" style="margin-top: 10px;">
        <div class="span2">
            <label class="control-label" style="text-align:right; font-size:12px;">
                <b><fmt:message code="admin.appstore.app.edit.form.app.images.upload.image.file.label"/></b>
            </label>
        </div>
        <div class="span9">
            <%--begin iteration--%>
            <c:forEach var="map" items="${app.appBanner}">
                <c:set var="bCaption" value="${map['caption']}"/>
                <c:set var="bUrl" value="${map['url']}"/>
                <div class='image-select-class' id=''
                     onclick="return getFile('<c:out value="${app.id}"/>appBannerImage')">

                    <img src="<fmt:message code='admin.appstore.image.base.url'/><c:out value='${bUrl}'/>"
                         id='<c:out value="${app.id}"/>bannerSelectedImgPreview'
                         class="selectedImage">

                    <div id='banner_defaultImage_1<c:out value="${app.id}"/>'>
                        <i class="icon-picture bigger-250 image-select-icon"></i>
                        <br/>Select image
                    </div>

                        <%--TODO need to uncomment these thing for future progress bar--%>
                    <div id='banner_progressArea_1<c:out value="${app.id}"/>' style="display: none;">
                        <img src='./resources/img/loading.gif' class='loading_spinner'>

                        <div class="progress progress-mini">
                            <div id='banner_progressBar_1<c:out value="${app.id}"/>'
                                 class="progressBar progress-bar progress-danger" style="width:0%;"></div>
                        </div>
                    </div>
                </div>
            </c:forEach>
            <%--end iteration--%>
            <%--if banners are empty show the place holder--%>
            <c:if test="${(fn:length(app.appBanner))==0}">
                <div class='image-select-class' id=''
                     onclick="return getFile('<c:out value="${app.id}"/>appBannerImage')">

                    <img src="<fmt:message code='admin.appstore.image.base.url'/><c:out value='${bUrl}'/>"
                         id='<c:out value="${app.id}"/>bannerSelectedImgPreview'
                         class="selectedImage">

                    <div id='banner_defaultImage_1<c:out value="${app.id}"/>'>
                        <i class="icon-picture bigger-250 image-select-icon"></i>
                        <br/>Select image
                    </div>

                        <%--TODO need to uncomment these thing for future progress bar--%>
                    <div id='banner_progressArea_1<c:out value="${app.id}"/>' style="display: none;">
                        <img src='./resources/img/loading.gif' class='loading_spinner'>

                        <div class="progress progress-mini">
                            <div id='banner_progressBar_1<c:out value="${app.id}"/>'
                                 class="progressBar progress-bar progress-danger" style="width:0%;"></div>
                        </div>
                    </div>
                </div>
            </c:if>
            <br/>

            <div style="float: left;" class="red appEditErrorNotification span12"
                 id='<c:out value="${app.id}"/>imgUploadingErrorBanner'></div>
            <%--Error notification in file uploading--%>

            <%--<div class="selectedImgInAppAera">
                <img src='' id='<c:out value="${app.name}"/>IconSelectedImgPreview'
                     class="selectedImage">
            </div>--%>

            <input style="display: none;" type='text' value=''
                   id='<c:out value="${app.id}"/>BannerImgStr' name='app_banner'/>

            <%--image preview div--%>
            <input type="file" id='<c:out value="${app.id}"/>appBannerImage' style="display: none;"
                   onchange="preview(this
                           , '<c:out value="${app.id}"/>bannerSelectedImgPreview'
                           , '<c:out value="${app.id}"/>BannerImgStr'
                           ,'<c:out value="${app.id}"/>BannerSelectedImgOriginal',
                           'AppBanner',
                           '<c:out value="${app.id}"/>',
                           'banner_1',
                           'banner_progressArea_1<c:out value="${app.id}"/>',
                           'banner_progressBar_1<c:out value="${app.id}"/>',
                           'banner_defaultImage_1<c:out value="${app.id}"/>');">

            <img style="display: none;" src='' id='<c:out value="${app.id}"/>BannerSelectedImgOriginal'
                 style="width: auto; height:auto;">

        </div>
    </div>
</div>
<%--end app banner--%>

<c:if test="${app.downloadable == true}">

<div id="<c:out value="${app.id}"/>ScrnShots" class="tab-pane">
    <div class="row-fluid">
        <div class="span2">
            <label class="control-label"
                   style="text-align:right; font-size:12px;">
                <b><fmt:message code="admin.appstore.app.edit.form.app.instructions.label"/></b>
            </label>
        </div>
        <div class="span9">
            <fmt:message code="admin.appstore.app.edit.form.app.screenshot.instruction"/>
        </div>
    </div>

    <div class="row-fluid" style="margin-top: 10px;">
        <div class="span2">
            <label class="control-label" style="text-align:right; font-size:12px;">
                <b><fmt:message code="admin.appstore.app.edit.form.app.images.upload.image.file.label"/></b>
            </label>
        </div>
        <div class="span9">
            <c:set var="screenshot_index" value="1"/>
            <c:forEach items="${app.screenShots}" var="map">
                <c:set var="caption" value="${map['caption']}"/>
                <c:set var="url" value="${map['url']}"/>
                <c:set var="mobile" value="${fn:containsIgnoreCase(caption, 'mobile')}"/>

                <c:if test="${mobile == false}">
                    <div class='image-select-class' id=''
                         onclick="return getFile('<c:out value="${app.id}"/>ScrnAppImage_<c:out
                                 value="${screenshot_index}"/>')">
                        <img src='<fmt:message code='admin.appstore.image.base.url'/><c:out value="${url}"/>'
                             id='<c:out value="${app.id}"/>ScrnSelectedImgPreview_<c:out value="${screenshot_index}"/>'
                             class="selectedImage"/>

                        <div id='screen_defaultImage_<c:out value="${screenshot_index}"/><c:out value="${app.id}"/>'>
                            <i class="icon-picture bigger-250 image-select-icon"></i>
                            <br/>Select image
                        </div>

                            <%--TODO need to uncomment these thing for future progress bar--%>
                        <div id='screen_progressArea_<c:out value="${screenshot_index}"/><c:out value="${app.id}"/>'
                             style="display: none;">
                            <img src='./resources/img/loading.gif' class='loading_spinner'>

                            <div class="progress progress-mini">
                                <div id='screen_progressBar_<c:out value="${screenshot_index}"/><c:out value="${app.id}"/>'
                                     class="progressBar progress-bar progress-danger" style="width:0%;"></div>
                            </div>
                        </div>

                    </div>

                    <c:set var="screenshot_index" value="${screenshot_index + 1}"/>
                </c:if>
            </c:forEach>


            <c:forEach var="index" begin="${screenshot_index}" end="3">
                <div class='image-select-class' id=''
                     onclick="return getFile('<c:out value="${app.id}"/>ScrnAppImage_<c:out value="${index}"/>')">
                    <img src='' id='<c:out value="${app.id}"/>ScrnSelectedImgPreview_<c:out value="${index}"/>'
                         class="selectedImage">

                    <div id='screen_defaultImage_<c:out value="${index}"/><c:out value="${app.id}"/>'>
                        <i class="icon-picture bigger-250 image-select-icon"></i>
                        <br/>Select image
                    </div>

                        <%--TODO need to uncomment these thing for future progress bar--%>
                    <div id='screen_progressArea_<c:out value="${index}"/><c:out value="${app.id}"/>'
                         style="display: none;">
                        <img src='./resources/img/loading.gif' class='loading_spinner'>

                        <div class="progress progress-mini">
                            <div id='screen_progressBar_<c:out value="${index}"/><c:out value="${app.id}"/>'
                                 class="progressBar progress-bar progress-danger" style="width:0%;"></div>
                        </div>
                    </div>

                </div>
                <c:set var="index" value="${index + 1}"/>
            </c:forEach>

            <br/>

                <%--<button class="hmsSearchButton" style="color:#464646;"
                        onclick="return getFile('<c:out value="${app.name}"/>ScrnAppImage')">
                    <i class="icon-picture"></i> Select Image
                </button>--%>

                <%--Error notification in file uploading--%>
            <div class="red appEditErrorNotification span12"
                 id='<c:out value="${app.id}"/>imgUploadingErrorScreenShot'></div>
                <%--Error notification in file uploading--%>

                <%--<div class="selectedImgInAppAera">
                    <img src='' id='<c:out value="${app.name}"/>ScrnSelectedImgPreview'
                         class="selectedImage">
                </div>--%>

            <input style="display: none;" type='text' value=''
                   id='<c:out value="${app.id}"/>ScrnImgStr_1' name='screenshot_1'/>

            <input style="display: none;" type='text' value=''
                   id='<c:out value="${app.id}"/>ScrnImgStr_2' name='screenshot_2'/>

            <input style="display: none;" type='text' value=''
                   id='<c:out value="${app.id}"/>ScrnImgStr_3' name='screenshot_3'/>

                <%--image preview div--%>
            <input type="file" id='<c:out value="${app.id}"/>ScrnAppImage_1' style="display: none;"
                   onchange="preview(this, '<c:out value="${app.id}"/>ScrnSelectedImgPreview_1', '<c:out
                           value="${app.id}"/>ScrnImgStr_1', '<c:out
                           value="${app.id}"/>ScrnSelectedImgOriginal', 'ScreenShot', '<c:out
                           value="${app.id}"/>',
                           'screenshot_1',
                           'screen_progressArea_1<c:out value="${app.id}"/>',
                           'screen_progressBar_1<c:out value="${app.id}"/>',
                           'screen_defaultImage_1<c:out value="${app.id}"/>')">

            <input type="file" id='<c:out value="${app.id}"/>ScrnAppImage_2' style="display: none;"
                   onchange="preview(this, '<c:out value="${app.id}"/>ScrnSelectedImgPreview_2',
                           '<c:out value="${app.id}"/>ScrnImgStr_2'
                           , '<c:out value="${app.id}"/>ScrnSelectedImgOriginal'
                           , 'ScreenShot'
                           , '<c:out value="${app.id}"/>',
                           'screenshot_2',
                           'screen_progressArea_2<c:out value="${app.id}"/>',
                           'screen_progressBar_2<c:out value="${app.id}"/>',
                           'screen_defaultImage_2<c:out value="${app.id}"/>')">

            <input type="file" id='<c:out value="${app.id}"/>ScrnAppImage_3' style="display: none;"
                   onchange="preview(this, '<c:out value="${app.id}"/>ScrnSelectedImgPreview_3', '<c:out
                           value="${app.id}"/>ScrnImgStr_3', '<c:out
                           value="${app.id}"/>ScrnSelectedImgOriginal', 'ScreenShot', '<c:out
                           value="${app.id}"/>',
                           'screenshot_3',
                           'screen_progressArea_3<c:out value="${app.id}"/>',
                           'screen_progressBar_3<c:out value="${app.id}"/>',
                           'screen_defaultImage_3<c:out value="${app.id}"/>')">

            <img style="display: none;" src='' id='<c:out value="${app.id}"/>ScrnSelectedImgOriginal'
                 style="width: auto; height:auto;">

        </div>

    </div>
</div>

<div id="<c:out value="${app.id}"/>mobImg" class="tab-pane">
    <div class="row-fluid">
        <div class="span2">
            <label class="control-label"
                   style="text-align:right; font-size:12px;">
                <b><fmt:message code="admin.appstore.app.edit.form.app.instructions.label"/></b>
            </label>
        </div>
        <div class="span9">
            <fmt:message code="admin.appstore.app.edit.form.app.mobilescreenshot.instruction"/>
                <%--description will goes here! :) --%>
        </div>
    </div>

    <div class="row-fluid" style="margin-top: 10px;">
        <div class="span2">
            <label class="control-label" style="text-align:right; font-size:12px;">
                <b><fmt:message code="admin.appstore.app.edit.form.app.images.upload.image.file.label"/></b>
            </label>
        </div>
        <div class="span9">
            <c:set var="mobile_screenshot_index" value="1"/>
            <c:forEach items="${app.screenShots}" var="map">
                <c:set var="caption" value="${map['caption']}"/>
                <c:set var="url" value="${map['url']}"/>
                <c:set var="mobile" value="${fn:containsIgnoreCase(caption, 'mobile')}"/>

                <c:if test="${mobile == true}">
                    <div class='image-select-class' id=''
                         onclick="return getFile('<c:out value="${app.id}"/>MobAppImage_<c:out
                                 value="${mobile_screenshot_index}"/>')">
                        <img src='<fmt:message code='admin.appstore.image.base.url'/><c:out value="${url}"/>'
                             id='<c:out value="${app.id}"/>MobSelectedImgPreview_<c:out value="${mobile_screenshot_index}"/>'
                             class="selectedImage">

                        <div id='mobile_defaultImage_<c:out value="${mobile_screenshot_index}"/><c:out value="${app.id}"/>'>
                            <i class="icon-picture bigger-250 image-select-icon"></i>
                            <br/>Select image
                        </div>

                            <%--TODO need to uncomment these thing for future progress bar--%>
                        <div id='mobile_progressArea_<c:out value="${mobile_screenshot_index}"/><c:out value="${app.id}"/>'
                             style="display: none;">
                            <img src='./resources/img/loading.gif' class='loading_spinner'>

                            <div class="progress progress-mini">
                                <div id='mobile_progressBar_<c:out value="${mobile_screenshot_index}"/><c:out value="${app.id}"/>'
                                     class="progressBar progress-bar progress-danger" style="width:0%;"></div>
                            </div>
                        </div>

                    </div>

                    <c:set var="mobile_screenshot_index" value="${mobile_screenshot_index + 1}"/>
                </c:if>
            </c:forEach>

            <c:forEach var="index" begin="${mobile_screenshot_index}" end="3">
                <div class='image-select-class' id=''
                     onclick="return getFile('<c:out value="${app.id}"/>MobAppImage_<c:out value="${index}"/>')">
                    <img src='' id='<c:out value="${app.id}"/>MobSelectedImgPreview_<c:out value="${index}"/>'
                         class="selectedImage">

                    <div id='mobile_defaultImage_<c:out value="${index}"/><c:out value="${app.id}"/>'>
                        <i class="icon-picture bigger-250 image-select-icon"></i>
                        <br/>Select image
                    </div>

                        <%--TODO need to uncomment these thing for future progress bar--%>
                    <div id='mobile_progressArea_<c:out value="${index}"/><c:out value="${app.id}"/>'
                         style="display: none;">
                        <img src='./resources/img/loading.gif' class='loading_spinner'>

                        <div class="progress progress-mini">
                            <div id='mobile_progressBar_<c:out value="${index}"/><c:out value="${app.id}"/>'
                                 class="progressBar progress-bar progress-danger" style="width:0%;"></div>
                        </div>
                    </div>

                </div>
                <c:set var="index" value="${index + 1}"/>
            </c:forEach>

            <br/>

                <%--<button class="hmsSearchButton" style="color:#464646;"--%>
                <%--onclick="return getFile('<c:out value="${app.name}"/>MobAppImage_1')">--%>
                <%--<i class="icon-picture"></i> Select Image--%>
                <%--</button>--%>

                <%--<button class="hmsSearchButton" style="color:#464646;"--%>
                <%--onclick="return getFile('<c:out value="${app.name}"/>MobAppImage_2')">--%>
                <%--<i class="icon-picture"></i> Select Image--%>
                <%--</button>--%>

                <%--<button class="hmsSearchButton" style="color:#464646;"--%>
                <%--onclick="return getFile('<c:out value="${app.name}"/>MobAppImage_3')">--%>
                <%--<i class="icon-picture"></i> Select Image--%>
                <%--</button>--%>

                <%--Error notification in file uploading--%>
            <div class="red appEditErrorNotification span12"
                 id='<c:out value="${app.id}"/>imgUploadingErrorMobile'></div>
                <%--Error notification in file uploading--%>

                <%--<div class="selectedImgInAppAera">
                    <img src='' id='<c:out value="${app.name}"/>MobSelectedImgPreview_1'
                         class="selectedImage">
                </div>

                <div class="selectedImgInAppAera">
                    <img src='' id='<c:out value="${app.name}"/>MobSelectedImgPreview_2'
                         class="selectedImage">
                </div>

                <div class="selectedImgInAppAera">
                    <img src='' id='<c:out value="${app.name}"/>MobSelectedImgPreview_3'
                         class="selectedImage">
                </div>--%>

            <input style="display: none;" type='text' value=''
                   id='<c:out value="${app.id}"/>MobImgStr_1' name='mobile_screenshot_1'/>
            <input style="display: none;" type='text' value=''
                   id='<c:out value="${app.id}"/>MobImgStr_2' name='mobile_screenshot_2'/>
            <input style="display: none;" type='text' value=''
                   id='<c:out value="${app.id}"/>MobImgStr_3' name='mobile_screenshot_3'/>
                <%--image preview div--%>

            <input type="file" id='<c:out value="${app.id}"/>MobAppImage_1' style="display: none;"
                   onchange="preview(this,
                           '<c:out value="${app.id}"/>MobSelectedImgPreview_1',
                           '<c:out value="${app.id}"/>MobImgStr_1',
                           '<c:out value="${app.id}"/>MobSelectedImgOriginal',
                           'MobileScreenShot',
                           '<c:out value="${app.id}"/>',
                           'mobile_screenshot_1',
                           'mobile_progressArea_1<c:out value="${app.id}"/>',
                           'mobile_progressBar_1<c:out value="${app.id}"/>',
                           'mobile_defaultImage_1<c:out value="${app.id}"/>')">

            <input type="file" id='<c:out value="${app.id}"/>MobAppImage_2' style="display: none;"
                   onchange="preview(this,
                           '<c:out value="${app.id}"/>MobSelectedImgPreview_2',
                           '<c:out value="${app.id}"/>MobImgStr_2',
                           '<c:out value="${app.id}"/>MobSelectedImgOriginal',
                           'MobileScreenShot',
                           '<c:out value="${app.id}"/>',
                           'mobile_screenshot_2',
                           'mobile_progressArea_2<c:out value="${app.id}"/>',
                           'mobile_progressBar_2<c:out value="${app.id}"/>',
                           'mobile_defaultImage_2<c:out value="${app.id}"/>')">

            <input type="file" id='<c:out value="${app.id}"/>MobAppImage_3' style="display: none;"
                   onchange="preview(this,
                           '<c:out value="${app.id}"/>MobSelectedImgPreview_3',
                           '<c:out value="${app.id}"/>MobImgStr_3',
                           '<c:out value="${app.id}"/>MobSelectedImgOriginal',
                           'MobileScreenShot',
                           '<c:out value="${app.id}"/>',
                           'mobile_screenshot_3',
                           'mobile_progressArea_3<c:out value="${app.id}"/>',
                           'mobile_progressBar_3<c:out value="${app.id}"/>',
                           'mobile_defaultImage_3<c:out value="${app.id}"/>')">


            <img style="display: none;" src='' id='<c:out value="${app.id}"/>MobSelectedImgOriginal'
                 style="width: auto; height:auto;">

        </div>
    </div>
</div>

</c:if>
</div>
</div>

</div>
</div>

<%--Java String.format equivalent javascript plugin--%>
<script src="resources/js/sprintf.min.js"></script>
<%--Java String.format equivalent javascript plugin--%>

<script src="resources/js/bootstrap-multiselect-1.0.1.js"></script>

<script type="text/javascript">
var formId = "#<c:out value="${app.id}"/>editAppModal";
var amountRegex = /^[0-9]\d{0,9}(\.\d{1,2})?%?$/;
$(".subscriptionChargingErrorLabel").hide();
$(".downloadableChargingErrorLabel").hide();
$(".smsMoChargingErrorLabel").hide();
$(".smsMtChargingErrorLabel").hide();
$(".ussdMoChargingErrorLabel").hide();
$(".ussdMtChargingErrorLabel").hide();
$(".categoryErrorLabel").hide();

var subMsgFmt = $("#subMsgFmt").val();
var downMsgFmt = $("#downMsgFmt").val();
var smsMoMsgFmt = $("#smsMoMsgFmt").val();
var smsMtMsgFmt = $("#smsMtMsgFmt").val();
var ussdMsgFmt = $("#ussdMsgFmt").val();
var freeMsgFmt = $("#freeMsgFmt").val();
var singleNcsTypeFreeMsgFmt = "";
var chargingDescriptionSeparator = " ";

var completeChargingDescription = "";
var downloadableChargingDescription = "";
var subscriptionChargingDescription = "";
var smsMtChargingDescription = "";
var smsMoChargingDescription = "";
var ussdChargingDescription = "";

function validateAmountFields(chargeTypeField, field, errorField) {
    console.log("chargeType: " + chargeTypeField.val())
    if ($(chargeTypeField).val() === "flat") {
        if ($(field).val().length == 0 || !parseFloat($(field).val()) > 0) {
            $(errorField).show();
            return false;
        } else if (!amountRegex.test($(field).val())) {
            $(errorField).show();
            return false;
        } else {
            $(errorField).hide();
            return true;
        }
    } else {
        $(errorField).hide();
        return true;
    }
}

function validateCategory() {
    if (!$("#multiCatSelect").val()) {
        $(".categoryErrorLabel").show();
        return false;
    } else {
        return true;
    }
}

$(formId).on("submit", function (event) {
    if (validateCategory() == false || validateAmountFields($("#cmbSubsType"), $("#txtSubsAmnt"), $(".subscriptionChargingErrorLabel")) == false || validateAmountFields($("#cmbDownType"), $("#txtDownAmnt"), $(".downloadableChargingErrorLabel")) == false ||
            validateAmountFields($("#cmbSmsMoType"), $("#txtSmsMoAmnt"), $(".smsMoChargingErrorLabel")) == false || validateAmountFields($("#cmbSmsMtType"), $("#txtSmsMtAmnt"), $(".smsMtChargingErrorLabel")) == false ||
            validateAmountFields($("#cmbUssdMoType"), $("#txtUssdMoAmnt"), $(".ussdMoChargingErrorLabel")) == false || validateAmountFields($("#cmbUssdMtType"), $("#txtUssdMtAmnt"), $(".ussdMtChargingErrorLabel")) == false) {
        return false;
    } else {
        event.preventDefault();
        var baseURL = event.currentTarget.action;
        $("#selectedCategoriesHid").val($("#multiCatSelect").val())
        var parameters = $(this).serialize();
        console.log(parameters);
        editApp(baseURL, parameters);
    }
});

// When the modal is appearing, if the charging type is free, other charging fields of the particular ncs type will be disabled.
enableDisableFields($("#cmbSubsType"), $("#txtSubsAmnt"), $("#cmbSubsfr"));
enableDisableFields($("#cmbDownType"), $("#txtDownAmnt"));
enableDisableFields($("#cmbSmsMtType"), $("#txtSmsMtAmnt"));
enableDisableFields($("#cmbSmsMoType"), $("#txtSmsMoAmnt"));
enableDisableFields($("#cmbUssdMtType"), $("#txtUssdMtAmnt"));
enableDisableFields($("#cmbUssdMoType"), $("#txtUssdMoAmnt"));

function generateChargingDescription(chargingTypeField, freeMessageFormat, fieldMessageFormat, currency, amountField, frequencyField) {
    var message = "";
    if ($(chargingTypeField).val() === "free") {
        message = freeMessageFormat;
    } else {
        if ($(frequencyField).length) {
            if (amountRegex.test($(amountField).val())) {
                message = sprintf(fieldMessageFormat, $(frequencyField).val().charAt(0).toUpperCase() + $(frequencyField).val().slice(1), currency, $(amountField).val());
            }
        } else {
            if (amountRegex.test($(amountField).val())) {
                message = sprintf(fieldMessageFormat, currency, $(amountField).val());
            }
        }
    }
    return message;
}

subscriptionChargingDescription = generateChargingDescription($("#cmbSubsType"), singleNcsTypeFreeMsgFmt, subMsgFmt, 'FJD', $("#txtSubsAmnt"), $("#cmbSubsfr"));
downloadableChargingDescription = generateChargingDescription($("#cmbDownType"), singleNcsTypeFreeMsgFmt, downMsgFmt, 'FJD', $("#txtDownAmnt"));
smsMtChargingDescription = generateChargingDescription($("#cmbSmsMtType"), singleNcsTypeFreeMsgFmt, smsMtMsgFmt, 'FJD', $("#txtSmsMtAmnt"));
smsMoChargingDescription = generateChargingDescription($("#cmbSmsMoType"), singleNcsTypeFreeMsgFmt, smsMoMsgFmt, 'FJD', $("#txtSmsMoAmnt"));
ussdChargingDescription = generateChargingDescription($("#cmbUssdMoType"), singleNcsTypeFreeMsgFmt, ussdMsgFmt, 'FJD', $("#txtUssdMoAmnt"));

// When the value of charging type dropdown changes, other charging fields of the particular ncs type will be disabled.
$("#cmbSubsType").change(function () {
    enableDisableFields($("#cmbSubsType"), $("#txtSubsAmnt"), $("#cmbSubsfr"));
    subscriptionChargingDescription = generateChargingDescription($("#cmbSubsType"), singleNcsTypeFreeMsgFmt, subMsgFmt, 'FJD', $("#txtSubsAmnt"), $("#cmbSubsfr"));
    completeChargingDescription = subscriptionChargingDescription + chargingDescriptionSeparator +
            downloadableChargingDescription + chargingDescriptionSeparator + smsMtChargingDescription +
            chargingDescriptionSeparator + smsMoChargingDescription + chargingDescriptionSeparator + ussdChargingDescription;
    if ($.trim(completeChargingDescription).length == 0) {
        $("#chargingDetailsDiv").text(freeMsgFmt);
    } else {
        $("#chargingDetailsDiv").text(completeChargingDescription);
    }
});

$("#txtSubsAmnt").keyup(function () {
    subscriptionChargingDescription = generateChargingDescription($("#cmbSubsType"), singleNcsTypeFreeMsgFmt, subMsgFmt, 'FJD', $("#txtSubsAmnt"), $("#cmbSubsfr"));
    completeChargingDescription = subscriptionChargingDescription + chargingDescriptionSeparator +
            downloadableChargingDescription + chargingDescriptionSeparator + smsMtChargingDescription +
            chargingDescriptionSeparator + smsMoChargingDescription + chargingDescriptionSeparator + ussdChargingDescription;
    if ($.trim(completeChargingDescription).length == 0) {
        $("#chargingDetailsDiv").text(freeMsgFmt);
    } else {
        $("#chargingDetailsDiv").text(completeChargingDescription);
    }
});

$("#cmbSubsfr").change(function () {
    subscriptionChargingDescription = generateChargingDescription($("#cmbSubsType"), freeMsgFmt, subMsgFmt, 'FJD', $("#txtSubsAmnt"), $("#cmbSubsfr"));
    completeChargingDescription = subscriptionChargingDescription + downloadableChargingDescription + smsMtChargingDescription + smsMoChargingDescription + ussdChargingDescription;
    if ($.trim(completeChargingDescription).length == 0) {
        $("#chargingDetailsDiv").text(freeMsgFmt);
    } else {
        $("#chargingDetailsDiv").text(completeChargingDescription);
    }
});

$("#cmbDownType").change(function () {
    enableDisableFields($("#cmbDownType"), $("#txtDownAmnt"));
    downloadableChargingDescription = generateChargingDescription($("#cmbDownType"), singleNcsTypeFreeMsgFmt, downMsgFmt, 'FJD', $("#txtDownAmnt"));
    completeChargingDescription = subscriptionChargingDescription + chargingDescriptionSeparator +
            downloadableChargingDescription + chargingDescriptionSeparator + smsMtChargingDescription +
            chargingDescriptionSeparator + smsMoChargingDescription + chargingDescriptionSeparator + ussdChargingDescription;
    if ($.trim(completeChargingDescription).length == 0) {
        $("#chargingDetailsDiv").text(freeMsgFmt);
    } else {
        $("#chargingDetailsDiv").text(completeChargingDescription);
    }
});

$("#txtDownAmnt").keyup(function () {
    downloadableChargingDescription = generateChargingDescription($("#cmbDownType"), singleNcsTypeFreeMsgFmt, downMsgFmt, 'FJD', $("#txtDownAmnt"));
    completeChargingDescription = subscriptionChargingDescription + chargingDescriptionSeparator +
            downloadableChargingDescription + chargingDescriptionSeparator + smsMtChargingDescription +
            chargingDescriptionSeparator + smsMoChargingDescription + chargingDescriptionSeparator + ussdChargingDescription;
    if ($.trim(completeChargingDescription).length == 0) {
        $("#chargingDetailsDiv").text(freeMsgFmt);
    } else {
        $("#chargingDetailsDiv").text(completeChargingDescription);
    }
});

$("#cmbSmsMtType").change(function () {
    enableDisableFields($("#cmbSmsMtType"), $("#txtSmsMtAmnt"));
    smsMtChargingDescription = generateChargingDescription($("#cmbSmsMtType"), singleNcsTypeFreeMsgFmt, smsMtMsgFmt, 'FJD', $("#txtSmsMtAmnt"));
    completeChargingDescription = subscriptionChargingDescription + chargingDescriptionSeparator +
            downloadableChargingDescription + chargingDescriptionSeparator + smsMtChargingDescription +
            chargingDescriptionSeparator + smsMoChargingDescription + chargingDescriptionSeparator + ussdChargingDescription;
    if ($.trim(completeChargingDescription).length == 0) {
        $("#chargingDetailsDiv").text(freeMsgFmt);
    } else {
        $("#chargingDetailsDiv").text(completeChargingDescription);
    }
});

$("#txtSmsMtAmnt").keyup(function () {
    smsMtChargingDescription = generateChargingDescription($("#cmbSmsMtType"), singleNcsTypeFreeMsgFmt, smsMtMsgFmt, 'FJD', $("#txtSmsMtAmnt"));
    completeChargingDescription = subscriptionChargingDescription + chargingDescriptionSeparator +
            downloadableChargingDescription + chargingDescriptionSeparator + smsMtChargingDescription +
            chargingDescriptionSeparator + smsMoChargingDescription + chargingDescriptionSeparator + ussdChargingDescription;
    if ($.trim(completeChargingDescription).length == 0) {
        $("#chargingDetailsDiv").text(freeMsgFmt);
    } else {
        $("#chargingDetailsDiv").text(completeChargingDescription);
    }
});

$("#cmbSmsMoType").change(function () {
    enableDisableFields($("#cmbSmsMoType"), $("#txtSmsMoAmnt"));
    smsMoChargingDescription = generateChargingDescription($("#cmbSmsMoType"), singleNcsTypeFreeMsgFmt, smsMoMsgFmt, 'FJD', $("#txtSmsMoAmnt"));
    completeChargingDescription = subscriptionChargingDescription + chargingDescriptionSeparator +
            downloadableChargingDescription + chargingDescriptionSeparator + smsMtChargingDescription +
            chargingDescriptionSeparator + smsMoChargingDescription + chargingDescriptionSeparator + ussdChargingDescription;
    if ($.trim(completeChargingDescription).length == 0) {
        $("#chargingDetailsDiv").text(freeMsgFmt);
    } else {
        $("#chargingDetailsDiv").text(completeChargingDescription);
    }
});

$("#txtSmsMoAmnt").keyup(function () {
    smsMoChargingDescription = generateChargingDescription($("#cmbSmsMoType"), singleNcsTypeFreeMsgFmt, smsMoMsgFmt, 'FJD', $("#txtSmsMoAmnt"));
    completeChargingDescription = subscriptionChargingDescription + chargingDescriptionSeparator +
            downloadableChargingDescription + chargingDescriptionSeparator + smsMtChargingDescription +
            chargingDescriptionSeparator + smsMoChargingDescription + chargingDescriptionSeparator + ussdChargingDescription;
    if ($.trim(completeChargingDescription).length == 0) {
        $("#chargingDetailsDiv").text(freeMsgFmt);
    } else {
        $("#chargingDetailsDiv").text(completeChargingDescription);
    }
});

$("#cmbUssdMtType").change(function () {
    enableDisableFields($("#cmbUssdMtType"), $("#txtUssdMtAmnt"));
});

$("#cmbUssdMoType").change(function () {
    enableDisableFields($("#cmbUssdMoType"), $("#txtUssdMoAmnt"));
    ussdChargingDescription = generateChargingDescription($("#cmbUssdMoType"), singleNcsTypeFreeMsgFmt, ussdMsgFmt, 'FJD', $("#txtUssdMoAmnt"));
    completeChargingDescription = subscriptionChargingDescription + chargingDescriptionSeparator +
            downloadableChargingDescription + chargingDescriptionSeparator + smsMtChargingDescription +
            chargingDescriptionSeparator + smsMoChargingDescription + chargingDescriptionSeparator + ussdChargingDescription;
    if ($.trim(completeChargingDescription).length == 0) {
        $("#chargingDetailsDiv").text(freeMsgFmt);
    } else {
        $("#chargingDetailsDiv").text(completeChargingDescription);
    }
});

$("#txtUssdMoAmnt").keyup(function () {
    ussdChargingDescription = generateChargingDescription($("#cmbUssdMoType"), singleNcsTypeFreeMsgFmt, ussdMsgFmt, 'FJD', $("#txtUssdMoAmnt"));
    completeChargingDescription = subscriptionChargingDescription + chargingDescriptionSeparator +
            downloadableChargingDescription + chargingDescriptionSeparator + smsMtChargingDescription +
            chargingDescriptionSeparator + smsMoChargingDescription + chargingDescriptionSeparator + ussdChargingDescription;
    if ($.trim(completeChargingDescription).length == 0) {
        $("#chargingDetailsDiv").text(freeMsgFmt);
    } else {
        $("#chargingDetailsDiv").text(completeChargingDescription);
    }
});

function enableDisableFields(field, disablingField1, disablingField2) {
    var chargeType = $(field).val();
    if ($(disablingField2).length) {
        if ($.trim(chargeType) === "free") {
            $(disablingField1).prop('disabled', true);
            $(disablingField1).val("")
            $(disablingField2).prop('disabled', true);
        } else {
            $(disablingField1).prop('disabled', false);
            $(disablingField2).prop('disabled', false);
        }
    } else {
        if ($.trim(chargeType) === "free") {
            $(disablingField1).prop('disabled', true);
            $(disablingField1).val("")
        } else {
            $(disablingField1).prop('disabled', false);
        }
    }
}

completeChargingDescription = subscriptionChargingDescription + chargingDescriptionSeparator +
        downloadableChargingDescription + chargingDescriptionSeparator + smsMtChargingDescription +
        chargingDescriptionSeparator + smsMoChargingDescription + chargingDescriptionSeparator + ussdChargingDescription;
if ($.trim(completeChargingDescription).length == 0) {
    $("#chargingDetailsDiv").text(freeMsgFmt);
} else {
    $("#chargingDetailsDiv").text(completeChargingDescription);
}

$("#btnModalClose").click(function () {
    window.location.reload(true);
});

$(".appEditModal").on("hidden.bs.modal", function () {
    window.location.reload(true);
});

$("#multiCatSelect").multiselect({
    maxHeight: 200,
    buttonWidth: '170px',
    nonSelectedText: 'No categories selected',
    dropUp: false
});

</script>
</form>


