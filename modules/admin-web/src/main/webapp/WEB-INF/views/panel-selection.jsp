<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmtj" uri="http://java.sun.com/jsp/jstl/fmt" %>

</div>  <%--don't remove this, !***important***! end of bread crumbs tag--%>
<fmtj:bundle basename="messages">

<style type="text/css">
    .grey {
        /*background-color: #6c6c6c;*/
    }

    .border {
        /*-webkit-box-shadow: inset 0px 0px 2px 2px #696969;*/
        /*box-shadow: inset 0px 0px 2px 2px #696969;*/
    }

    .padding_10 {
        padding: 10px;
    }

    .featured_panel {
        background-color: #efefef;
        background-image: url("resources/img/top-banner.png");
        background-repeat: no-repeat;
        background-size: 100%;
        height: 150px;
    }

    .col-md-offset-1 {
        /*margin-left: 2.333333%;*/
    }

    .panel_one {
        background-color: #efefef;
        height: 100px;
    }

    .panel_two {
        background-color: #efefef;
        height: 100px;
    }

    .left_panel {
        background-color: #efefef;
        /*height: 365px;*/
        width: 157px;
    }

    hr {
        margin-bottom: 8px;
        margin-top: 8px;
    }

    .pull-right {
        float: right;
    }

    .span1 {
        margin-left: -25px;
    }

    h3.no-margin {
        margin: 0px;
        margin-bottom: 10px;
        background-color: #d3d3d3;
    }

    select {
        margin-top: 10px;
    }
</style>

<c:forEach var="cpConfig" items="${currentConfig}">
    <c:if test="${cpConfig.id=='panelTop'}">
        <c:set var="panelTopCategory" value="${cpConfig.category}"/>
    </c:if>
    <c:if test="${cpConfig.id=='panelBottom'}">
        <c:set var="panelBottomCategory" value="${cpConfig.category}"/>
    </c:if>
    <c:if test="${cpConfig.id=='panelMiddle'}">
        <c:set var="panelMiddleCategory" value="${cpConfig.category}"/>
    </c:if>
</c:forEach>

<div class="page-content" style="margin-top: 20px">

    <form action="panels" method="post" id='frmPanelConfig'>
        <div class="row grey span10 text-center container-fluid">

            <div class="span2 col-md-offset-1">
                <div class="left_panel row border">
                    <h3 class="no-margin" style="font-size: 20px;"><fmt:message code="admin.appstore.panel.selection.page.side.menu"/></h3>
                    <label style="text-align: left; margin-left: 5px;"><fmt:message code="admin.appstore.panel.selection.page.side.menu.link1"/></label>
                    <label style="text-align: left; margin-left: 5px;"><fmt:message code="admin.appstore.panel.selection.page.side.menu.link2"/></label>
                    <label style="text-align: left; margin-left: 5px;"><fmt:message code="admin.appstore.panel.selection.page.side.menu.link3"/></label>
                    <label style="text-align: left; margin-left: 5px;"><fmt:message code="admin.appstore.panel.selection.page.side.menu.link4"/></label>
                </div>
                <div class="left_panel row border" style="height: 5px; background: none;">
                </div>
                <div class="left_panel row border">
                    <img src="resources/img/left-banner.png">
                </div>
            </div>

            <div class="span7">
                <div class="featured_panel row border padding_10 ">
                </div>

                <hr/>
                <div class="panel_one row border   panel_sel_border">
                    <h3 class="no-margin" style="font-size: 20px;">App Panel Top</h3>

                        <span>
                            <select class="form-control" name='panelTop'>
                                <option selected value="${panelTopCategory}"><fmtj:message
                                        key="${panelTopCategory}"/></option>
                                <c:forEach var="category" items="${panelCategories}">
                                    <c:if test="${category!=panelTopCategory}">
                                        <option value="${category}"><fmtj:message key="${category}"/></option>
                                    </c:if>
                                </c:forEach>
                            </select>
                        </span>
                </div>

                <hr/>
                <div class="panel_one row border   panel_sel_border">
                    <h3 class="no-margin" style="font-size: 20px;">App Panel Middle</h3>

                        <span>
                            <select class="form-control" name='panelMiddle'>
                                <option selected value="${panelMiddleCategory}"><fmtj:message
                                        key="${panelMiddleCategory}"/></option>
                                <c:forEach var="category" items="${panelCategories}">
                                    <c:if test="${category!=panelMiddleCategory}">
                                        <option value="${category}"><fmtj:message key="${category}"/></option>
                                    </c:if>
                                </c:forEach>
                            </select>
                        </span>
                </div>

                <hr/>
                <div class="panel_two row border  panel_sel_border">
                    <h3 class="no-margin" style="font-size: 20px;">App Panel Bottom</h3>

                    <span>
                        <select class="form-control" name="panelBottom">
                            <option selected value="${panelBottomCategory}"><fmtj:message
                                    key="${panelBottomCategory}"/></option>
                            <c:forEach var="category" items="${panelCategories}">
                                <c:if test="${category!=panelBottomCategory}">
                                    <option value="${category}"><fmtj:message key="${category}"/></option>
                                </c:if>
                            </c:forEach>
                        </select>
                    </span>
                </div>
            </div>
            <div class="">
                <button class="btn btn-lg btn-danger form-control" id='savePanels'>Save</button>
            </div>
        </div>
    </form>

</div>

<script type="text/javascript">

    $('#savePanels').click(function (e) {
        e.preventDefault();
        var formSerialized = jQuery('#frmPanelConfig').serialize();
        var panelConfig = toJsonValues(formSerialized);
        var isValidConfig = validatePanelConfig(panelConfig);
        if (isValidConfig) {
            $('#frmPanelConfig').submit();
        } else {
            clientSideAlerts('alert alert-block alert-error', 'red', '250px;', 'icon-remove', 'two or more panels contain the same app category or you may have left a panel empty');
        }
    });

    function toJsonValues(formFields) {
        var valueArray = [];
        formFields = formFields.split('&');
        var len = formFields.length;
        try {
            for (var i = 0; i < len; i++) {
                var value = new Object();
                var kv = formFields[i].toString().split('=');
                value['id'] = kv[0];
                if (kv[1] == '' || kv[1] == undefined)
                    throw Error('Panels cannot leave empty');
                value['category'] = kv[1].toString().replace('+', ' ');
                valueArray.push(value);
            }
        } catch (err) {
            alert('Panels cannot leave empty!');
        }

        return valueArray;
    }


    function validatePanelConfig(config) {

        if (config.length != 3) {
            return false;
        }
        var panelTopCat = config[0]['category'];
        var panelBottomCat = config[1]['category'];
        var panelMiddleCat = config[2]['category'];

        return ((panelTopCat != panelBottomCat)
                && (panelMiddleCat != panelTopCat)
                && (panelMiddleCat != panelBottomCat));
    }

</script>

</fmtj:bundle>
