<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

</div>  <%--don't remove this, !***important***! end of bread crumbs tag--%>
    <div class="page-content" style="padding-left:40px;">

        <div class="row-fluid">
            <h4>
                <fmt:message code='admin.appstore.advertisements.page.main.title'/>
            </h4>
            <h6 style="margin-bottom: 25px;">
                <fmt:message code='admin.appstore.advertisements.page.short.description'/>
            </h6>
        </div>

        <form action="" method="post" id="frmAds" style="margin-bottom: 50px">

            <c:forEach items="${addURLS}" var="Ad">
                <div class="row-fluid">
                    <div class="span12">
                        <label>
                            <span class="lbl">
                                <i class="icon-double-angle-right bigger-110"></i>
                                <b>
                                    <c:if test="${Ad.id=='first'}">
                                        <fmt:message code='admin.appstore.advertisements.page.homepage.label'/>
                                    </c:if>
                                    <c:if test="${Ad.id=='second'}">
                                        <fmt:message code='admin.appstore.advertisements.page.application.details.page.label'/>
                                    </c:if>
                                    <c:if test="${Ad.id=='third'}">
                                        <fmt:message code='admin.appstore.advertisements.page.search.results.page.label'/>
                                    </c:if>
                                </b>
                            </span>
                        </label>
                    </div>
                    <input type="text" class="hidden" value="<c:out value="${Ad.id}"/>"
                           name="<c:out value="${Ad.id}"/>_id"/>

                    <div class="span9">
                        <div class="span2">
                            <label class="pull-right hidden-phone visible-desktop">
                                Name
                            </label>
                            <label class="hidden-desktop visible-phone">
                                Name
                            </label>
                        </div>
                        <div class="span10">
                            <input type="text" class="span9" name="<c:out value="${Ad.id}"/>_name"
                                   value='<c:out value="${Ad.name}"/>' required>
                        </div>
                    </div>
                    <div class="span9">
                        <div class="span2">
                            <label class="pull-right hidden-phone visible-desktop">
                                Image URL
                            </label>
                            <label class="hidden-desktop visible-phone">
                                Image URL
                            </label>
                        </div>
                        <div class="span10">
                            <input type="url" class="span9" name="<c:out value="${Ad.id}"/>_image_url"
                                   value='<c:out value="${Ad.imageUrl}"/>' required>
                        </div>
                    </div>
                    <div class="span9">
                        <div class="span2">
                            <label class="pull-right hidden-phone visible-desktop">
                                Target URL
                            </label>
                            <label class="hidden-desktop visible-phone">
                                Target URL
                            </label>
                        </div>
                        <div class="span10">
                            <input type="url" class="span9" name="<c:out value="${Ad.id}"/>_target_url"
                                   value='<c:out value="${Ad.targetUrl}"/>' required>
                        </div>
                    </div>
                </div>

            </c:forEach>
            <br/>
            <div class="row-fluid">
                <div class="span9">
                    <div class="span1" style="margin-left: -5px;"></div>
                    <div class="span9">

                        <button class="hmsSearchButton pull-right" style="color:#262626; margin-right:5px;"
                                type="submit">
                            <fmt:message code='admin.appstore.advertisements.page.save.button'/>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>