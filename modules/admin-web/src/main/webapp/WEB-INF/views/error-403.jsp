<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<html lang="en-US">
    <head profile="http://www.w3.org/2005/10/profile">
        <link rel="icon"
              type="image/png"
              href="resources/img/favicon.ico">

        <meta charset="utf-8"/>
        <title>
            <fmt:message code="admin.appstore.title"/>
        </title>
        </title>
        <meta name="description" content="overview &amp; stats"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <!--basic styles-->

        <link href="resources/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
        <link href="resources/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen"/>
        <link rel="stylesheet" href="resources/css/font-awesome.min.css" media="screen"/>


        <!--[if IE 7]>
        <link rel="stylesheet" href="resources/css/font-awesome-ie7.min.css"/>
        <![endif]-->

        <!--page specific plugin styles-->
        <!--fonts-->
        <link rel="stylesheet" href="resources/css/google-font-api.css"/>

        <!--ace styles-->
        <link rel="stylesheet" href="resources/css/ace.min.css" media="screen"/>
        <link rel="stylesheet" href="resources/css/ace-responsive.min.css" media="screen"/>
        <link rel="stylesheet" href="resources/css/ace-skins.min.css" media="screen"/>

        <!--[if lte IE 8]>
        <link rel="stylesheet" href="resources/css/ace-ie.min.css"/>
        <![endif]-->

            <%--hms customizations--%>
        <link rel="stylesheet" href="resources/css/hms-appstore-custom.css" media="screen"/>
            <%--hml customizations--%>
    </head>

    <body onload="layoutFixed()">

        <%--HEADER BAR ?????????? BEGINS--%>
    <div class="navbar" id="navbar">
        <div class="navbar-inner">
            <span><img src="resources/img/logo.png" alt="vdf_logo" class="vdf_logo_tl"/></span>
            <div class="container-fluid">
                <a href='<fmt:message code="admin.appstore.base.url"/>' class="brand">
                    <small>
                        <fmt:message code="admin.appstore.header.bar.page.heading"/>
                    </small>
                </a><!--/.brand-->
                <!--/.ace-nav-->
            </div>
            <!--/.container-fluid-->
        </div>
        <!--/.navbar-inner-->
    </div>
        <%--HEADER BAR ?????????? ENDS--%>

    <div align="center" style="width: 100%">
        <div class="error-container" style="max-width: 600px; margin-top: 5%;">
            <div class="well">
                <h1 class="grey lighter smaller">
            <span class="blue bigger-125">
                <i class="icon-sitemap"></i>
                403
            </span>
                    Access Denied
                </h1>

                <hr>
                <h3 class="lighter smaller">Access to the specified resource has been forbidden.</h3>

                <div>

                    <div class="space"></div>
                        <%--<h4 class="smaller">Meanwhile,</h4>--%>

                    <ul class="list-unstyled spaced inline bigger-110 margin-15">
                        <li>
                            <i class="icon-hand-right blue"></i>
                            Give us more info on how this specific error occurred
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

        <%--FOOTER BEGINS--%>
    <div id="footer" align="center" style="margin-top:30px;">
        <p class="muted credit copyright"><fmt:message code="admin.appstore.copy.right"/> <a
                href="http://www.hsenidmobile.com"> <fmt:message code="admin.appstore.company.name"/> </a>
            <fmt:message code="admin.appstore.all.rights.reserved"/></p>
    </div>
        <%--FOOTER ENDS--%>


    </body>
</html>