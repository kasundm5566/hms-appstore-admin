<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>

</div>  <%--don't remove this, !***important***! end of bread crumbs tag--%>
<div class="page-content" style="padding-left:40px;">

    <div class="row-fluid">
        <h4><fmt:message code="admin.appstore.featured.app.page.main.title"/></h4>
        <h6 style="margin-bottom: 25px;"><fmt:message code="admin.appstore.featured.app.page.sub.title"/></h6>
    </div>

    <div class="row-fluid">
        <div class="span4">
            <div class="row-fluid">
                <div class="span12">
                    <input id="searchKey" type="text" class="span12" placeholder='<fmt:message code="featured.app.search.box.placeholder"/>'>
                </div>
            </div>

            <div class="row-fluid">
                <div class="span12">
                    <div id="appSuggestionList">
                        <ul id="appSearchResList">
                        </ul>
                    </div>
                </div>
            </div>
        </div>


        <div class="span8">
            <div class="row-fluid">
                <div class="span12">

                    <div id="selectedFeaturedApps">

                        <c:forEach begin="1" end="${numOffApps}" var="i">
                            <div class="test">
                                <div id='featured_app_<c:out value="${i}"/>'>
                                    <div id="p<c:out value='${i}'/>" class='dropPlaceHolder' align="center">
                                        <div class="dropPlaceHolderNum"><c:out value="${i}"/></div>
                                        <div class="dropPlaceHolderText">Drag and Drop<br/>an App here</div>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
<%--                    <div class="test">
                            <div id="featured_app_1">
                                <div id="p1" class='dropPlaceHolder' align="center">
                                    <div class="dropPlaceHolderNum">1</div>
                                    <div class="dropPlaceHolderText">Drag and Drop<br/>an App here</div>
                                </div>
                            </div>
                        </div>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>

<div class="row-fluid">
    <div class="span6">
        <div id="alert">0</div>
    </div>
</div>

</div>