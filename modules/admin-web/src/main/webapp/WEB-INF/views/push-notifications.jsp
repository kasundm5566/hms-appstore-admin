<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmtj" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<link rel="stylesheet" href="resources/css/bootstrap-multiselect.css" media="screen"/>

<%
    response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
    response.addHeader("Pragma", "no-cache");
    response.setDateHeader("Expires", 0);
%>

</div>   <%--***don't remove this !**important**! end of bread crumb tag--%>

<div class="page-content" style="padding-left: 40px;">
    <input id="notificationMessageTitleCharacterLimit" type="hidden" value="${notificationMessageTitleCharacterLimit}"/>
    <input id="notificationMessageBodyCharacterLimit" type="hidden" value="${notificationMessageBodyCharacterLimit}"/>

    <form action="sendPushNotification" id="sendNotificationForm" name="sendNotificationForm" method="POST">
        <div class="row-fluid">
            <h4><fmt:message code="admin.appstore.push.notification.header"/></h4>
            <h6><fmt:message code="admin.appstore.push.notification.sub.header"/></h6>
        </div>
        <div class="row-fluid">
            <div class="span12" style="display: inline-flex;">
                <label class="control-label mandatoryFieldLabels" style="font-size: 12px">
                    <b><fmt:message code="admin.appstore.push.notification.form.message.title"/></b>
                </label>
                <label class="control-label" id="notificationMessageTitleErrorLabel"
                       style="color: #B91D47; font-family: sans-serif; font-size:12px; margin-left: 10px;">
                    <fmt:message
                            code="admin.appstore.push.notification.message.title.error.message.part1"/> ${notificationMessageTitleCharacterLimit}
                    <fmt:message code="admin.appstore.push.notification.message.title.error.message.part2"/>
                </label>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span8">
                <input data-charcount-enable="true" id="notificationMessageTitle" type="text"
                       name="notificationMessageTitle" required="true">
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12" style="display: inline-flex;">
                <label class="control-label mandatoryFieldLabels" style="font-size: 12px">
                    <b><fmt:message code="admin.appstore.push.notification.form.message"/></b>
                </label>
                <label class="control-label" id="notificationMessageBodyErrorLabel"
                       style="color: #B91D47; font-family: sans-serif; font-size:12px; margin-left: 10px;">
                    <fmt:message
                            code="admin.appstore.push.notification.message.body.error.message.part1"/> ${notificationMessageBodyCharacterLimit}
                    <fmt:message code="admin.appstore.push.notification.message.body.error.message.part2"/>
                </label>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span8">
                <textarea data-charcount-enable="true" id="notificationMessage" rows="8" required="true"
                          style="width: 100%;" name="notificationMessage"></textarea>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <label class="control-label" style="font-size: 12px">
                    <b><fmt:message code="admin.appstore.push.notification.form.type"/></b>
                </label>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <select id="cmbNotificationType" name="notificationType">
                    <option value="All" selected><fmt:message
                            code="admin.appstore.push.notification.form.type.option1"/></option>
                    <option value="AppSpecific"><fmt:message
                            code="admin.appstore.push.notification.form.type.option2"/></option>
                </select>
            </div>
        </div>

        <div class="row-fluid notifyingAppsBlock">
            <div class="span12">
                <label class="control-label" style="font-size: 12px">
                    <b><fmt:message code="admin.appstore.push.notification.form.apps"/></b>
                </label>
            </div>
        </div>
        <input type="hidden" name="selectedApps" id="selectedAppsHid">

        <div class="row-fluid notifyingAppsBlock">
            <div class="span12">
                <select id="multiAppSelect" multiple="multiple" name="notificationApps">
                    <c:forEach items="${appList}" var="app">
                        <option value="${app.id}">${app.name}</option>
                    </c:forEach>
                </select>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span8">
                <input type="submit" class="hmsSearchButton notificationFormButtons" value="Send">
                <input type="reset" class="hmsSearchButton notificationFormButtons" id="btnFormReset"
                       style="margin-right: 5px;">
            </div>
        </div>
    </form>
</div>

<script src="resources/js/bootstrap-multiselect-1.0.1.js"></script>
<script>

    showHideAppMultiSelect($("#cmbNotificationType"));

    $("#notificationMessageTitleErrorLabel").hide();
    $("#notificationMessageBodyErrorLabel").hide();

    $("#multiAppSelect").multiselect({
        includeSelectAllOption: true,
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        maxHeight: 350,
        buttonWidth: '220px',
        nonSelectedText: 'No apps selected',
        dropUp: true
    });

    $("#cmbNotificationType").change(function () {
        showHideAppMultiSelect($(this))
    });

    $("#btnFormReset").click(function () {
        $(".notifyingAppsBlock").hide();
        $("#notificationMessageTitleErrorLabel").hide();
        $("#notificationMessageBodyErrorLabel").hide();
//        $('#multiAppSelect').multiselect('deselectAll', true);
//        $('#multiAppSelect').multiselect('updateButtonText');
    });

    $("#sendNotificationForm").submit(function (event) {
        var notificationType = $("#cmbNotificationType").val();
        if ($.trim(notificationType) === "AppSpecific" && !$("#multiAppSelect").val()) {
            clientSideAlerts('alert alert-block alert-error', 'red', '380px;', 'icon-remove', 'Please select apps to send push notifications.');
            return false;
        }
        var notificationMessageTitleCharacterLimit = parseInt($("#notificationMessageTitleCharacterLimit").val())
        var notificationMessageBodyCharacterLimit = parseInt($("#notificationMessageBodyCharacterLimit").val())

        var notificationMessageTitle = $("#notificationMessageTitle").val();
        var notificationMessageBody = $("#notificationMessage").val();

        if ($.trim(notificationMessageTitle).length > notificationMessageTitleCharacterLimit) {
            $("#notificationMessageTitleErrorLabel").show();
            return false;
        } else {
            $("#notificationMessageTitleErrorLabel").hide();
        }

        if ($.trim(notificationMessageBody).length > notificationMessageBodyCharacterLimit) {
            $("#notificationMessageBodyErrorLabel").show();
            return false;
        } else {
            $("#notificationMessageBodyErrorLabel").hide();
        }

        event.preventDefault();
        $("#selectedAppsHid").val($("#multiAppSelect").val())
        var formData = new FormData($(this)[0]);
        var baseURL1 = event.currentTarget.action;
        var parameters = $(this).serialize();
        $.ajax({
            type: "POST",
            url: baseURL1,
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {
//                location.reload();
                if (result == "S1000") {
                    $(".alert").hide();
                    clientSideAlerts('alert alert-block alert-success', 'green', '400px;', 'icon-ok', 'Push notification sent successfully!');
                    $("#sendNotificationForm").trigger('reset');
                    $(".notifyingAppsBlock").hide();
                } else {
                    $(".alert").hide();
                    clientSideAlerts('alert alert-block alert-error', 'red', '400px;', 'icon-remove', 'failed to send the push notification.');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".alert").hide();
                clientSideAlerts('alert alert-block alert-error', 'red', '400px;', 'icon-remove', 'failed to send the push notification. ' + errorThrown);
            }
        });
    });

    function showHideAppMultiSelect(field) {
        var notificationType = $(field).val();
        if ($.trim(notificationType) === "All") {
            $(".notifyingAppsBlock").hide();
        } else if ($.trim(notificationType) === "AppSpecific") {
            $(".notifyingAppsBlock").show();
        }
    }
</script>