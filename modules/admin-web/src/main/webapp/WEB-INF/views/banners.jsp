<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmtj" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<style>
    .btn {
        background-color: rgb(7, 55, 99);
        color: white;
        border: none;
        border-radius: 10%;
        cursor: pointer;
        padding: 0px 10px 0px 10px;
        text-decoration: none;

    }

    .swal-button {
        padding: 7px 19px;
        border-radius: 2px;
        background-color: #bf0000;
        font-size: 12px;
        border: 1px solid #bf0000;
        color: #eaedf1;
        text-shadow: 0px -1px 0px rgba(0, 0, 0, 0.3);
    }

</style>
<script type="text/javascript">
    $(document).ready(function () {
        $('.fadeWithTable').fadeIn('slow');

        var getTableRowCount = document.getElementById("AllBannersTable").rows.length;
        if (parseInt(getTableRowCount - 1) == 0) {
            $('#noResultWarning').fadeIn('slow');
        }
    });
</script>
<script>
    function editImageURLBanner(id) {
        var imgName = document.getElementById(id + "textId2").value.replace(/^.*[\\\/]/, '');
        document.getElementById(id + "textId2").value = imgName;
    }
</script>
<div class="nav-search" id="nav-search">
    <div class="hidden-phone visible-desktop">
        <a data-toggle="modal" data-target="#addBanner">
            <span class="btn"><i class="icon-plus icon-large"></i> Add </span>
        </a>
    </div>
</div>

</div>   <%--***don't remove this !**important**! end of bread crumb tag--%>
<div class="page-content">
    <div class="row-fluid">
        <div class="span12">
            <%--TABLE--%>
            <div class="row-fluid">
                <div class="table-header">
                    <fmt:message code="admin.appstore.banner.table.header"/>
                </div>

                <c:forEach var="display" items="${displayLocations}">
                    <input id="${display.display_location}Minw" type="hidden" value="${display.min_width}"/>
                    <input id="${display.display_location}Maxw" type="hidden" value="${display.max_width}"/>
                    <input id="${display.display_location}Minh" type="hidden" value="${display.min_height}"/>
                    <input id="${display.display_location}Maxh" type="hidden" value="${display.max_height}"/>
                    <input id="${display.display_location}Size" type="hidden" value="${display.size}"/>
                </c:forEach>
                <input id="bannerImageCharacterLimit" type="hidden" value="${bannerImagesCharacterCount}"/>
                <input id="selectedDisplayLocation" type="hidden"/>

                <div>
                    <form action="banners" id="Banners" method="GET" accept-charset="UTF-8">
                        <select name="display_location" type="text" id="display_location" onchange="this.form.submit()">
                            <option disabled selected><fmt:message
                                    code="admin.appstore.banner.locations.dropdown.select.location.option"/></option>
                            <option value="all" ${'all' == display_location ? 'selected':''}><fmt:message
                                    code="admin.appstore.banner.locations.dropdown.all.option"/></option>
                            <c:forEach var="display" items="${displayLocations}">
                                <option value="${display.display_location}" ${display == display_location ? 'selected':''}>${display.name}
                                </option>
                            </c:forEach>
                        </select>
                    </form>
                </div>
                <div class="table-responsive">
                    <table id="AllBannersTable" class="table table-striped table-bordered table-hover"
                           style="table-layout: fixed;">
                        <thead>
                        <tr>
                            <th><fmt:message code="admin.appstore.banner.table.header.location"/></th>
                            <th><fmt:message code="admin.appstore.banner.table.header.description"/></th>
                            <th><fmt:message code="admin.appstore.banner.table.header.link"/></th>
                            <th><fmt:message code="admin.appstore.banner.table.header.preview"/></th>
                            <th style="width: 5%;"><fmt:message code="admin.appstore.banner.table.header.action"/></th>
                            <%--app table action column header--%>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${banners}" var="banner">
                            <tr class="fadeWithTable" style="display: none;" class='hidden'
                                id='<c:out value="${banner.id}"/>Row'>
                                <td>
                                    <c:out value="${banner.display_location}"/>
                                </td>
                                <td style="word-wrap: break-word;">
                                    <c:out value="${banner.description}"/>
                                </td>
                                <td style="word-wrap: break-word;">
                                    <c:out value="${banner.link}"/>
                                </td>
                                <td>
                                        <%--<c:out value="${banner.image_url}"/>--%>
                                        <%--<img src="https://172.16.3.197/appstore/images/applications/icons/banners-subscription-apps/GxDev_banner.jpg" width="200" height="100">--%>
                                    <img src="<fmt:message code="admin.appstore.banner.base.url"/>/<c:out value="${banner.image_url}"/>"
                                         width="200"
                                         height="100">
                                </td>
                                <td>
                                    <a class="green tooltip-success" href="#<c:out value='${banner.id}'/>edit"
                                       data-toggle="modal"
                                       data-rel="tooltip"
                                       title="edit" onclick="editImageURLBanner(<c:out value='${banner.id}'/>)">
                                        <i class="icon-pencil bigger-130"></i></a>

                                    <a class="red tooltip-error" href="#<c:out value='${banner.id}'/>delete"
                                       data-toggle="modal"
                                       data-rel="tooltip"
                                       title="delete">
                                        <i class="icon-ban-circle bigger-120"></i></a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>

                <div id="noResultWarning" style="display: none;" align="center">
                    <div class="row-fluid">
                        <div class="span12" style="padding: 15px;">
                            <div class="span12 alert alert-warning" style="">
                                <button type="button" class="close" data-dismiss="alert">
                                    <i class="icon-remove"></i>
                                </button>
                                <fmt:message code="admin.appstore.app.search.no.results.found.message"/>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>

                <%--pagination--%>
                <div align="center">
                    <div class="pagination no-margin" id="paginateDiv">
                        <c:if test="${fn:length(urlList) gt 1}">
                            <ul>
                                <c:forEach items="${urlList}" var="url">
                                    <c:if test="${activePage == url.index}">
                                        <li id="searchResult${url.index+1}" class="active">
                                            <a href=<c:out value="${url.path}"/>>${url.index+1}</a>
                                        </li>
                                    </c:if>
                                    <c:if test="${activePage != url.index}">
                                        <li id="searchResult${url.index+1}">
                                            <a href=<c:out value="${url.path}"/>>${url.index+1}</a>
                                        </li>
                                    </c:if>
                                </c:forEach>
                            </ul>
                        </c:if>
                    </div>
                </div>

            </div>
            <%--row fluid end--%>
        </div>
        <%--span12 end--%>
    </div>
    <%--row fluid end--%>
</div>
<br>
<br>
</div>
</div>

<c:forEach items="${banners}" var="banner">
<%--Edit Banners--%>
<form action="editBanner" id="<c:out value='${banner.id}'/>editBanners" name="editBanners" class="editBanner"
      method="POST"
      accept-charset="UTF-8" enctype="multipart/form-data">
    <div id="<c:out value='${banner.id}'/>edit" style="width:800px; margin-left:-400px;"
         class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="row-fluid vdf_modal_header_bgc">
            <div class="span10">
                <div class="modal-header">
                    <h5 id="myModalLabel">
                        <fmt:message code="admin.appstore.banner.edit.form.title"/>
                    </h5>
                </div>
            </div>
            <div class="span2" style="padding-top: 15px; padding-right: 15px;">
                <button type="button" class="close" data-dismiss="modal" id="<c:out value='${banner.id}'/>close2"
                        aria-hidden="true" onclick="resetFunction2(<c:out value='${banner.id}'/>)">x
                </button>
            </div>
        </div>
        <div class="modal-body">
            <div class="span4">
                <div class="row-fluid">
                    <div class="span12" style="display: none;">
                        <input type="text" name="banner_id" value="<c:out value="${banner.id}"/>"
                               style="display: none"/>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <label class="control-label" style="font-size: 12px;">
                            <b><fmt:message code="admin.appstore.banner.add.form.display.location"/></b>
                        </label>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <input class="txtBannerLocation" type="text" name="display_location"
                               value="${banner.display_location}" style="display: none;">
                        <select class="cmbLocation" type="text"
                                value="<c:out value="${banner.display_location}"/>"
                                id="<c:out value="${banner.id}"/>displayEdit"
                                style="margin-top: -5px; margin-bottom: 20px;" disabled>
                            <c:set var="loc" value="${banner.display_location}"/>
                            <c:forEach var="display" items="${displayLocations}">
                                <c:choose>
                                    <c:when test="${display.display_location eq loc}">
                                        <option selected value="<c:out value="${display.display_location}"/>">
                                                ${display.name}</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${display.display_location}">${display.name}
                                        </option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <label class="control-label mandatoryFieldLabels" style="font-size: 12px;">
                            <b><fmt:message code="admin.appstore.banner.add.form.link"/></b>
                        </label>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <input class="linkVal" type="text" name="link"
                               style="margin-top: -5px; margin-bottom: 20px;"
                               value="<c:out value="${banner.link}"/>"
                               required/>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <label class="control-label mandatoryFieldLabels" style="font-size: 12px;">
                            <b><fmt:message code="admin.appstore.banner.add.form.description"/></b>
                        </label>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <input type="text" name="description" style="margin-top: -5px; margin-bottom: 20px;"
                               value="<c:out value="${banner.description}"/>"
                               required/>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <label class="control-label mandatoryFieldLabels" style="font-size: 12px;">
                            <b><fmt:message code="admin.appstore.banner.add.form.image"/></b>
                        </label>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12" style="display: none;">
                        <input type="text" name="old_image_url" style="display: none;"
                               value="<c:out value="${banner.image_url}"/>" readonly>
                        <input type="text" class="textClass" name="image_url"
                               id="<c:out value='${banner.id}'/>textId2"
                               value="<c:out value="${banner.image_url}"/>" required readonly/>
                    </div>
                    <div class="span12" style="display: none">
                        <input type="text" value="<c:out value='${banner.image_url}'/>" name="image_url_del"/>
                    </div>


                    <input type='file' class="imageClass"
                           style="margin-top: -5px; margin-bottom: 20px; line-height: 0px;" name="multipartFile"
                           id="<c:out value='${banner.id}'/>imgId2"
                           onchange="readURL2(this ,<c:out value='${banner.id}'/>);"/>
                </div>
            </div>
            <div class="span3">
                <img id="<c:out value='${banner.id}'/>blah2"
                     src="<fmt:message code="admin.appstore.banner.base.url"/>/<c:out value="${banner.image_url}"/>"
                     width="200"
                     height="100" style="margin-top: 50px;" alt="your image"/>
            </div>
        </div>

        <div class="modal-footer">
            <button class="hmsSearchButton" id="<c:out value='${banner.id}'/>close" data-dismiss="modal"
                    aria-hidden="true" type="reset" onclick="resetFunction(<c:out value='${banner.id}'/>)">
                <fmt:message code="admin.appstore.app.edit.form.close.button"/>
            </button>
            <button class="hmsSearchButton"
                    style="margin-right:5px;" type="submit"
                    onclick="return validateLink($(this).closest('form').find('.linkVal').val());">
                <fmt:message code="admin.appstore.app.edit.form.save.button"/>
            </button>
        </div>
    </div>
</form>


<%--Delete Banners--%>
<form action="deleteBanner" id="deleteBanners" name="deleteBanners" class="deleteBanner" method="POST"
      accept-charset="UTF-8">
    <div id="<c:out value='${banner.id}'/>delete" style="width: 600px;"
         class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="row-fluid vdf_modal_header_bgc">
            <div class="span10">
                <div class="modal-header">
                    <h5 id="myModalLabel">
                        <fmt:message code="admin.appstore.banner.delete.form.title"/>
                    </h5>
                </div>
            </div>
            <div class="span2" style="padding-top: 15px; padding-right: 15px;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            </div>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <div class="span12" style="display: none">
                    <input type="text" value="<c:out value='${banner.id}'/>" name="banner_id"/>
                </div>
                <div class="span12" style="display: none">
                    <input type="text" value="<c:out value='${banner.image_url}'/>" name="image_url"/>
                </div>
                <div class="span12" style="display: none">
                    <input type="text" value="<c:out value='${banner.display_location}'/>" name="display_location"/>
                </div>
                <div class="span10">
                    <h5 id="myModalLabel">
                        <fmt:message code="admin.appstore.banner.delete.confirmation.text"/>
                    </h5>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span1">

                </div>
                <div class="span3">
                    <label class="control-label" style="font-size: 12px; cursor: default;">
                        <fmt:message code="admin.appstore.banner.delete.form.display.location"/>
                    </label>
                </div>
                <div class="span8">
                    <label class="control-label" style="font-size: 12px; cursor: default;">
                        <c:out value='${banner.display_location}'/>
                    </label>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span1">

                </div>
                <div class="span3">
                    <label class="control-label" style="font-size: 12px; cursor: default;">
                        <fmt:message code="admin.appstore.banner.delete.form.banner.description"/>
                    </label>
                </div>
                <div class="span8" style="word-wrap: break-word;">
                    <label class="control-label" style="font-size: 12px; cursor: default;">
                        <c:out value="${banner.description}"/>
                    </label>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="hmsSearchButton " data-dismiss="modal" aria-hidden="true" type="reset">
                <fmt:message code="admin.appstore.banner.no.option.text"/>
            </button>
            <button class="hmsSearchButton"
                    style="margin-right:5px;" type="submit">
                <fmt:message code="admin.appstore.banner.yes.option.text"/>
            </button>
        </div>
    </div>
</form>

<%--TABLE END--%>
</div>

</c:forEach>


<form action="addBanner" id="addBanners" name="addBanners" method="POST" accept-charset="UTF-8"
      enctype="multipart/form-data">
    <div id="addBanner" style="width:800px; margin-left:-400px;"
         class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="row-fluid vdf_modal_header_bgc">
            <div class="span10">
                <div class="modal-header">
                    <h5>
                        <fmt:message code="admin.appstore.banner.add.form.title"/>
                    </h5>
                </div>
            </div>
            <div class="span2" style="padding-top: 15px; padding-right: 15px;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
                        onclick="resetFunctionAddBanner()">x
                </button>
            </div>
        </div>

        <div class="modal-body">
            <div class="span4">
                <div class="row-fluid">
                    <div class="span12">
                        <label class="control-label" style="font-size: 12px;">
                            <b><fmt:message code="admin.appstore.banner.add.form.display.location"/></b>
                        </label>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <%--<input type="text" name="display_location" required />--%>
                        <select class="cmbLocationAdd" name="display_location"
                                style="margin-top: -5px; margin-bottom: 20px;" type="text" required>
                            <c:forEach var="display" items="${displayLocations}">
                                <c:choose>
                                    <c:when test="${param.display_location eq display.display_location}">
                                        <option selected value="${display.display_location}">${display.name}
                                        </option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${display.display_location}">${display.name}
                                        </option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <label class="control-label mandatoryFieldLabels" style="font-size: 12px;">
                            <b><fmt:message code="admin.appstore.banner.add.form.link"/></b>
                        </label>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <input type="text" name="link" style="margin-top: -5px; margin-bottom: 20px;" required/>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <label class="control-label mandatoryFieldLabels" style="font-size: 12px;">
                            <b><fmt:message code="admin.appstore.banner.add.form.description"/></b>
                        </label>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <input type="text" name="description" style="margin-top: -5px; margin-bottom: 20px;" required/>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <label class="control-label mandatoryFieldLabels" style="font-size: 12px;">
                            <b><fmt:message code="admin.appstore.banner.add.form.image"/></b>
                        </label>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12" style="display: none;">
                        <input type="text" name="image_url" id="texId" required readonly/>
                    </div>

                    <input type='file' name="multipartFile" id="imageId" onchange="readURL(this);"
                           style="margin-top: -5px; margin-bottom: 20px; line-height: 0px;" required/>
                </div>
            </div>
            <div class="span3">
                <img id="blah" src="resources/img/app_store_sample_app_icon.png" alt="your image" width="300"
                     height="300" style="margin-top: 50px; max-height: 300px; max-width: 300px;"/>
            </div>
        </div>

        <div class="modal-footer">
            <button class="hmsSearchButton " data-dismiss="modal" aria-hidden="true" type="reset"
                    onclick="resetFunctionAddBanner()">
                <fmt:message code="admin.appstore.app.edit.form.close.button"/>
            </button>
            <button id="bannerSubmit" class="hmsSearchButton"
                    style="margin-right:5px;" type="submit">
                <fmt:message code="admin.appstore.app.edit.form.save.button"/>
            </button>
        </div>
    </div>
</form>

<script type="text/javascript">
    var dis = '<c:out value="${displayLocation}"/>';
    var pageSize = parseInt(<c:out value="${pageSize}"/>);

    var options = {
        currentPage:<c:out value="${activePage+1}"/>,
        totalPages:<c:out value="${fn:length(urlList)}"/>,
        useBootstrapTooltip: true,
        numberOfPages: 10, //change this value to set number of pagination nodes to show (between prev next buttons)

        pageUrl: function (type, page, current) {
            return "?display_location=" + dis + "&skip=" + ((page - 1) * pageSize) + "&limit=" + pageSize;
        }
    };

    $(function () {
        $(".pagination").bootstrapPaginator(options);
    });
</script>

</div>
<%--app edit window end--%>
<%--MODALS ENDS--%>
<%--</c:forEach>--%>

<%--<script type="text/javascript">--%>
<%--var dis = '<c:out value="${displayLocation}"/>';--%>
<%--var display_location = document.getElementById("display_location").value;--%>
<%--console.log("Display Location is : " + dis)--%>
<%--if(dis != "all")--%>
<%--{--%>
<%--$('#paginateDiv ').hide();--%>
<%--}--%>
<%--else--%>
<%--{--%>
<%--$('#paginateDiv ').show();--%>
<%--}--%>
<%--</script>--%>

<script type="text/javascript">
    var formId = "#addBanners";
    var urlValidationRegex = /^(?:(?:https?):\/\/?)(?:(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
    $(formId).on("submit", function (event) {
        var x = document.forms["addBanners"]["link"].value;
        var y = document.forms["addBanners"]["description"].value;
        var z = document.forms["addBanners"]["image_url"].value;
        if (x == "" || y == "" || z == "") {
//           alert("All the fields must be filled");
            swal("Warning", "All the fields must be filled", "warning");
            return false;
        }
        if (!urlValidationRegex.test(x)) {
            swal("Error", "Invalid link. Link should start with http:// or https://", "error");
            return false;
        }
        event.preventDefault();
        var formData = new FormData($(this)[0]);
        var baseURL1 = event.currentTarget.action;
        var parameters = $(this).serialize();
        $.ajax({
            type: "POST",
            url: baseURL1,
            data: formData,
            enctype: 'multipart/form-data',
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {
                location.reload();
                hideModal();
                $("#addBanners").trigger('reset');
                if (result == "S1000") {
                    clientSideAlerts('alert alert-block alert-success', 'green', '400px;', 'icon-ok', 'Banner configurations saved successfully!');
                } else {
                    clientSideAlerts('alert alert-block alert-error', 'red', '400px;', 'icon-remove', 'failed to add the Banner configurations!');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
    });

    function validateLink(linkValue) {
        if (!urlValidationRegex.test(linkValue)) {
            swal("Error", "Invalid link. Link should start with http:// or https://", "error");
            return false;
        }
    }
</script>

<script>
    function readURL(input) {
        var file = input.files && input.files[0];
        var imageType = /image.*/;

        var loc = $(".cmbLocationAdd").val();
        var validMaxWidth = parseInt($("#" + (loc + "Maxw")).val());
        var validMinWidth = parseInt($("#" + (loc + "Minw")).val());
        var validMaxHeight = parseInt($("#" + (loc + "Maxh")).val());
        var validMinHeight = parseInt($("#" + (loc + "Minh")).val());
        var validSize = parseInt($("#" + (loc + "Size")).val());
        var characterLimit = parseInt($("#bannerImageCharacterLimit").val())

        if (file.type.match(imageType)) {
            if (file) {
                var reader = new FileReader();
                var img = new Image();
                img.src = window.URL.createObjectURL(file);
                img.onload = function () {
                    var width = img.naturalWidth,
                            height = img.naturalHeight,
                            size = input.files[0].size / 1024,
                            name = file.name;
                    window.URL.revokeObjectURL(img.src);
                    if (name.length <= characterLimit) {
                        if (width <= validMaxWidth && width >= validMinWidth && height <= validMaxHeight && height >= validMinHeight) {
                            if (size !== 0 && size <= validSize) {
                                reader.onload = function (e) {
                                    $('#blah')
                                            .attr('src', e.target.result);
                                };

                                reader.readAsDataURL(file);

                                var x = document.getElementById("imageId").value.replace(/^.*[\\\/]/, '');
                                document.getElementById("texId").value = x;
                            }
                            else {
                                swal("Error", "<fmt:message code="admin.appstore.banner.icon.size.error"/>" + " " + validSize + "KB", "error");
                                $('#imageId').replaceWith($('#imageId').val('').clone(true));
                            }
                        }
                        else {
                            swal("Error", "<fmt:message code="admin.appstore.banner.icon.resolution.error.part2"/> " + validMaxWidth + " " + "<fmt:message code="admin.appstore.banner.icon.resolution.error.pixel.wide"/>, " + validMaxHeight + " " + "<fmt:message code="admin.appstore.banner.icon.resolution.error.pixel.high"/>" + " " + "<fmt:message code="admin.appstore.banner.icon.resolution.error.part1"/> " + validMinWidth + " " + "<fmt:message code="admin.appstore.banner.icon.resolution.error.pixel.wide"/>, " + validMinHeight + " " + "<fmt:message code="admin.appstore.banner.icon.resolution.error.pixel.high"/>.", "error");
                            $('#imageId').replaceWith($('#imageId').val('').clone(true));
                        }
                    } else {
                        swal("Error", "<fmt:message code="admin.appstore.banner.character.limit.error.part1"/>" + " " + characterLimit + " " + "<fmt:message code="admin.appstore.banner.character.limit.error.part2"/>", "error");
                        $('#imageId').replaceWith($('#imageId').val('').clone(true));
                    }
                };
            }
        }
        else {
            swal("Error", "<fmt:message code="admin.appstore.banner.icon.type.error"/>", "error");
            $('#imageId').replaceWith($('#imageId').val('').clone(true));
        }
    }
</script>

<style>
    img {
        max-width: 200px;
        max-height: 200px;
    }

    input[type=file] {
        padding: 10px;
        background: #f7f7f7;
    }
</style>

<script>
    function readURL2(input, id) {
        var file = input.files && input.files[0];
        var imageType = /image.*/;

        var loc = $(input).closest('.modal-body').find('.txtBannerLocation').val();
        var validMaxWidth = parseInt($("#" + (loc + "Maxw")).val());
        var validMinWidth = parseInt($("#" + (loc + "Minw")).val());
        var validMaxHeight = parseInt($("#" + (loc + "Maxh")).val());
        var validMinHeight = parseInt($("#" + (loc + "Minh")).val());
        var validSize = parseInt($("#" + (loc + "Size")).val());
        var characterLimit = parseInt($("#bannerImageCharacterLimit").val())

        if (file.type.match(imageType)) {
            if (file) {
                var reader = new FileReader();
                var img = new Image();
                img.src = window.URL.createObjectURL(file);
                img.onload = function () {
                    var width = img.naturalWidth,
                            height = img.naturalHeight,
                            size = input.files[0].size / 1024,
                            name = file.name;

                    window.URL.revokeObjectURL(img.src);
                    if (name.length <= characterLimit) {
                        if (width <= validMaxWidth && width >= validMinWidth && height <= validMaxHeight && height >= validMinHeight) {
                            if (size !== 0 && size <= validSize) {
                                reader.onload = function (e) {
                                    $('#' + id + 'blah2')
                                            .attr('src', e.target.result);
                                };

                                reader.readAsDataURL(file);

                                var x = document.getElementById(id + "imgId2").value.replace(/^.*[\\\/]/, '');
                                document.getElementById(id + "textId2").value = x;
                            }
                            else {
                                swal("Error", "<fmt:message code="admin.appstore.banner.icon.size.error"/>" + " " + validSize + "KB", "error");
                                $('#' + id + 'imgId2').replaceWith($('#' + id + 'imgId2').val('').clone(true));
                            }
                        }
                        else {
                            swal("Error", "<fmt:message code="admin.appstore.banner.icon.resolution.error.part2"/> " + validMaxWidth + " " + "<fmt:message code="admin.appstore.banner.icon.resolution.error.pixel.wide"/>, " + validMaxHeight + " " + "<fmt:message code="admin.appstore.banner.icon.resolution.error.pixel.high"/>" + " " + "<fmt:message code="admin.appstore.banner.icon.resolution.error.part1"/> " + validMinWidth + " " + "<fmt:message code="admin.appstore.banner.icon.resolution.error.pixel.wide"/>, " + validMinHeight + " " + "<fmt:message code="admin.appstore.banner.icon.resolution.error.pixel.high"/>.", "error");
                            $('#' + id + 'imgId2').replaceWith($('#' + id + 'imgId2').val('').clone(true));
                        }
                    } else {
                        swal("Error", "<fmt:message code="admin.appstore.banner.character.limit.error.part1"/>" + " " + characterLimit + " " + "<fmt:message code="admin.appstore.banner.character.limit.error.part2"/>", "error");
                        $('#' + id + 'imgId2').replaceWith($('#' + id + 'imgId2').val('').clone(true));
                    }
                };

            }
        }
        else {
            swal("Error", "<fmt:message code="admin.appstore.banner.icon.type.error"/>", "error");
            $('#' + id + 'imgId2').replaceWith($('#' + id + 'imgId2').val('').clone(true));
        }
    }
</script>

