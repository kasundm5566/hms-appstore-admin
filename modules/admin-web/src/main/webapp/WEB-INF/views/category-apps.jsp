<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmtj" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<script src="resources/js/appstore-admin-config.js"></script>
<script src="resources/js/appstore-admin-1.0.1.js"></script>
</div>  <%--don't remove this, !***important***! end of bread crumbs tag--%>
<div class="page-content" style="padding-left:40px;">

    <div class="row-fluid">
        <div class="span4">
            <h4>
                <input id="categoryNameField" type="hidden" value="${category}">
                <fmt:message
                        code="admin.appstore.category.add.app.page.title.part1"/>&nbsp;${category}&nbsp;<fmt:message
                    code="admin.appstore.category.add.app.page.title.part2"/>
            </h4>
            <h6 style="margin-bottom: 25px;">
                <fmt:message code="admin.appstore.category.add.app.page.sub.title"/>
            </h6>
        </div>
        <div class="span7" style="margin-top: 39px;">
            <label style="display: inline-block; font-size: 13px;"><fmt:message
                    code="admin.appstore.category.add.app.current.apps.label"/></label>
            <input id="btnSaveAddApps" onclick="updateCategoryApps();"
                   style="float: right;font-size: 13px; margin-right: -50px; display: none;" class="hmsSearchButton" type="button"
                   value="<fmt:message code="admin.appstore.category.add.app.save.button.text"/>">
            <c:if test="${fn:length(appList) eq 0}">
                <p id="paraNoApps"><fmt:message code="admin.appstore.category.add.app.no.apps.text"/></p>
            </c:if>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span4">
            <div class="row-fluid">
                <div class="span12">
                    <input id="appSearchKey" type="text" class="span12"
                           placeholder='<fmt:message code="featured.app.search.box.placeholder"/>'>
                </div>
            </div>

            <div class="row-fluid">
                <div class="span12">
                    <div id="appSuggestionList">
                        <ul id="appSearchResList">
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="span6">
            <div class="row-fluid" style="margin-top: -5px;">
                <ul class="dropme grid">
                    <c:set var="imageBasePath" value="${imageBasePath}"/>
                    <c:forEach items="${appList}" var="app">
                        <li class="featureAppSuggestionAppBlock">
                            <div>
                                <button id="btnClose" onclick="$(this).closest('li').remove();updateCategoryApps();"
                                        class="appLiClose">
                                    &#10006;</button>
                            </div>
                            <div>
                                <input class="appIdHidden" type="hidden" value="${app.id}">

                                <div>
                                    <img src="${imageBasePath}${app.appIcon}"
                                         style="float: left; width: 60px; height: 60px; margin-right: 5px;">
                                </div>
                                <div class="appsToBeFeaturedTitle">
                                    <c:set var="appender" value="..."/>
                                    <c:choose>
                                        <c:when test="${fn:length(app.name) gt 12}">
                                            <c:set var="appName" value="${fn:substring(app.name,0 ,12)}${appender}"/>
                                        </c:when>
                                        <c:otherwise>
                                            <c:set var="appName" value="${app.name}"/>
                                        </c:otherwise>
                                    </c:choose>
                                    <label class="appName" style="font-size: 12px;">${appName}</label>
                                </div>
                                <div class="appsToBeFeaturedCategory appsToBeFeaturedTitle">
                                    <c:choose>
                                        <c:when test="${fn:length(app.category) gt 12}">
                                            <c:set var="appCategory" value="${fn:substring(app.category,0 ,12)}${appender}"/>
                                        </c:when>
                                        <c:otherwise>
                                            <c:set var="appCategory" value="${app.category}"/>
                                        </c:otherwise>
                                    </c:choose>
                                    <label class="appCategory" style="font-size: 12px; margin-top: -5px;">${appCategory}</label>
                                    <c:set var="rateVal" value="${app.rating}"/>
                                    <c:set var="appRating" value="${fn:substringBefore(rateVal, '.')}"/>
                                    <label style="font-size: 12px; margin-top: -5px;">${appRating}<i class='icon-star' style='color: #FFA200'></i></label>
                                    <label style="font-size: 12px; display: none;">${app.id}</label>
                                </div>
                            </div>
                        </li>
                    </c:forEach>
                </ul>
            </div>
        </div>
    </div>
</div>

</div>

<div class="row-fluid">
    <div class="span6">
        <div id="alert">0</div>
    </div>
</div>

</div>

<script>
    //    $('.dropme').dropme('enable');
    $('.dropme').dropme('enable').bind('sortupdate', function (e, elm) {
        updateCategoryApps();
    });
</script>