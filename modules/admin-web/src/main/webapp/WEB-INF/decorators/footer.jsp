<%@taglib prefix="format" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div id="footer" align="center" style="margin-top:30px;">
    <jsp:useBean id="date" class="java.util.Date"/>
    <p class="muted credit copyright">
        <fmt:message code="admin.appstore.copy.right"/>&nbsp;
        <format:formatDate value="${date}" pattern="yyyy"/>.&nbsp;
        <a href="http://www.hsenidmobile.com"> <fmt:message code="admin.appstore.company.name"/> </a>
        <fmt:message code="admin.appstore.all.rights.reserved"/>
    </p>
</div>