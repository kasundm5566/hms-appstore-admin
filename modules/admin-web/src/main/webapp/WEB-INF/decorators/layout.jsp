<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html
PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<html lang="en-US">
<head profile="http://www.w3.org/2005/10/profile">
    <link rel="icon" type="image/png" href="resources/img/favicon.ico">

    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <title>
        <fmt:message code="admin.appstore.title"/>
        <sitemesh:write property='title'/>
    </title>
    <meta name="description" content="overview &amp; stats"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!--basic styles-->
    <link href="resources/css/bootstrap.css" rel="stylesheet" media="screen"/>
    <link href="resources/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen"/>
    <link rel="stylesheet" href="resources/css/font-awesome.min.css" media="screen"/>


    <c:if test="${fn:contains(header['User-Agent'],'MSIE 8.0')}">
        <link rel="stylesheet" href="resources/css/font-awesome-ie7.min.css"/>
        <link rel="stylesheet" href="resources/css/ace-ie.min.css"/>
    </c:if>

    <!--page specific plugin styles-->
    <!--fonts-->
    <link rel="stylesheet" href="resources/css/google-font-api.css"/>

    <!--ace styles-->
    <link rel="stylesheet" href="resources/css/ace.min.css" media="screen"/>
    <link rel="stylesheet" href="resources/css/ace-responsive.min.css" media="screen"/>
    <link rel="stylesheet" href="resources/css/ace-skins.min.css" media="screen"/>

    <%--hms customizations--%>
        <link rel="stylesheet" href="resources/css/hms-appstore-custom.css" media="screen"/>
    <%--hml customizations--%>

    <link rel="stylesheet" href="resources/css/dropMe.css" media="screen"/>

    <script src="resources/js/ace-extra.min.js"></script>

    <sitemesh:write property='head'/>

   <%--script for fade effect in table loading/start--%>
    <script src="resources/js/jquery-1.9.1.js"></script>
    <script src="resources/js/jquery-ui.js"></script>
    <%--script for fade effect in table loading/end--%>
</head>

<body onload="layoutFixed()">
<div id="alertArea" class="pull-right"
     style="width: 180px; position: fixed; margin-left: 63%; z-index: 10000; float: right; margin-top: 5px;"></div>
<%@ include file="headerbar.jsp" %>

<div class="main-container container-fluid">
    <a class="menu-toggler" id="menu-toggler" href="#">
        <span class="menu-text"></span>
    </a>

        <%@ include file="sidebar.jsp" %>
    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="/appstore-admin">Home</a>

                            <span class="divider">
                                <i class="icon-angle-right arrow-icon"></i>
                            </span>
                </li>
                <li class="active"><fmt:message code="admin.appstore.side.bar.app.management"/></li>
            </ul>
            <!--.breadcrumb-->

                <%--</div> this is end of bread crumb, this closing tag will placed in other pages--%>
            <sitemesh:write property='body'/>

                <%--footer--%>

        </div>
            <%--main content--%>
    </div>
    <%@ include file="footer.jsp" %>
        <%--main container end--%>
        <%--java scripts relative to hms appstore admin panel--%>
    <!--basic scripts-->
    <!--ace settings handler-->

        <%--importent java scripts--%>
    <script type="text/javascript">
        try {
            ace.settings.check('navbar', 'fixed')
        } catch (e) {
        }
        /*--used to put header fixed (originally came with ace theme)--*/
        try {
            ace.settings.check('breadcrumbs', 'fixed')
        } catch (e) {
        }
        /*--used to put bread crumbs fixed (originally came with ace theme)--*/
        try {
            ace.settings.check('sidebar', 'fixed')
        } catch (e) {
        }
        /*--used to put side bar fixed (originally came with ace theme)--*/
        try {
            ace.settings.check('sidebar', 'collapsed')
        } catch (e) {
        }
        /*--checking for side bar collapsed--*/
    </script>



    <!--[if IE]>
    <script src="resources/js/jquery_1.10.2.min.js"></script>
    <![endif]-->

    <!--[if !IE]>-->
    <script src="resources/js/jquery_2.0.3.min.js"></script>
    <!--<![endif]-->

    <!--[if !IE]>-->
    <script type="text/javascript">
        window.jQuery || document.write("<script src='resources/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
    </script>
    <!--<![endif]-->
    <!--[if IE]>
    <script type="text/javascript">
        window.jQuery || document.write("<script src='resources/js/jquery-1.10.2.min.js'>" + "<" + "/script>");
    </script>
    <![endif]-->
    <script type="text/javascript">
        if ("ontouchend" in document) document.write("<script src='resources/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
    </script>
    <script src="resources/js/bootstrap.min.js"></script>
    <script src="resources/js/dropMe.js"></script>
    <script src="resources/js/appstore-admin-1.0.1.js"></script>
    <script src="resources/js/appstore-admin-config.js"></script>
    <!--page specific plugin scripts-->
    <!--[if lte IE 8]>
    <!--<script src="resources/js/excanvas.min.js"></script>-->
    <![endif]-->
    <script src="resources/js/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="resources/js/jquery.ui.touch-punch.min.js"></script>
    <script src="resources/js/jquery.flot.min.js"></script>
    <script src="resources/js/jquery.flot.resize.min.js"></script>
    <script src="resources/js/bootstrap-tooltip.js"></script>
    <script src="resources/js/bootstrap-paginator.js"></script>
    <!--ace scripts-->
    <script src="resources/js/ace-elements.min.js"></script>
    <script src="resources/js/ace.min.js"></script>

    <%--validation plugin--%>
    <script src="resources/js/jquery.validate.js"></script>

    <script type="text/javascript">
        jQuery(function ($) {  /*--relative to table sorting and table tool tips--/--currently not working,
         need to check and fix--- P.S. there may be unnecessary scripts, need to check. (originally came with ace theme)*/

            $('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
            function tooltip_placement(context, source) {
                var $source = $(source);
                var $parent = $source.closest('table')
                var off1 = $parent.offset();
                var w1 = $parent.width();

                var off2 = $source.offset();
                var w2 = $source.width();

                if (parseInt(off2.top) < parseInt(off1.top) + parseInt(w1 / 2)) return 'top';
                return 'bottom';
            }

            <%--tool tip for app edit start--%>
            $('[data-rel="tooltip1"]').tooltip({placement: tooltip_placement1});
            function tooltip_placement1(context, source) {
                var $source = $(source);
                var $parent = $source.closest('modal hide fade')
                var off1 = $parent.offset();
                var w1 = $parent.width();

                var off2 = $source.offset();
                var w2 = $source.width();

                if (parseInt(off2.top) < parseInt(off1.top) + parseInt(w1 / 2)) return 'right';
                return 'right';
            }
            <%--tool tip for app edit end--%>

            $('#modal-form').on('shown.bs.modal', function () {
                $(this).find('.chosen-container').each(function () {
                    $(this).find('a:first-child').css('width', '210px');
                    $(this).find('.chosen-drop').css('width', '210px');
                    $(this).find('.chosen-search input').css('width', '200px');
                });
            })


            $('#id-input-file-1 , #id-input-file-2').ace_file_input({
                no_file:'No File ...',
                btn_choose:'Choose',
                btn_change:'Change',
                droppable:false,
                onchange:null,
                thumbnail:false //| true | large
            });


        });
    </script>

</body>
</html>