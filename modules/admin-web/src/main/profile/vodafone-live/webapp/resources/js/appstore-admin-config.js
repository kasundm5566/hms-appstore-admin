var config = {
    "discovery-api-base-url": "https://ctap.vodafone.com/discovery-api/v2/",
    "app-image-base-url": "https://ctap.vodafone.com/appstore-admin/",
    "app.name.max.size":15,
    "discovery-api-featured-app-url": "https://ctap.vodafone.com/discovery-api/v2/featured-apps/start/0/limit/8",
    "live-search-limit":20
};