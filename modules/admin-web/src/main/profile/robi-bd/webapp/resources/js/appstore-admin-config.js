var config = {
    "discovery-api-base-url": "https://apps.sdp.hsenidmobile.com/discovery-api/v2/",
    "app-image-base-url": "https://apps.sdp.hsenidmobile.com/appstore-admin/",
    "app.name.max.size":13,
    "discovery-api-featured-app-url": "https://apps.sdp.hsenidmobile.com/discovery-api/v2/featured-apps/start/0/limit/4",
    "live-search-limit":20
};