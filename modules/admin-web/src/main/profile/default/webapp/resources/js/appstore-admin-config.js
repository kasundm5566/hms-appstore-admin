var config = {
    "discovery-api-base-url": "http://core.appstore:6578/discovery-api/v2/",
    "app-image-base-url": "http://web.appstore:8080/appstore-admin/",
    "app.name.max.size":15,
    "discovery-api-featured-app-url": "http://core.appstore:6578/discovery-api/v2/featured-apps/start/0/limit/8",
    "live-search-limit":20,
    "num-of-featured-apps":8
};