<div class="sidebar" id="sidebar">
    <ul class="nav nav-list">
        <li>
            <a href="/appstore-admin" class="dropdown-toggle">
                <i class="icon-cogs"></i>
                <span class="menu-text"><fmt:message code="admin.appstore.side.bar.app.management"/></span>
                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li>
                    <a href="/appstore-admin">
                        <i class="icon-double-angle-right"></i>
                        <fmt:message code="admin.appstore.side.bar.all.apps"/>
                    </a>
                </li>

                <li>
                    <a href="featured">
                        <i class="icon-double-angle-right"></i>
                        <fmt:message code="admin.appstore.side.bar.featured.apps"/>
                    </a>
                </li>

                <li>
                    <a href="new">
                        <i class="icon-double-angle-right"></i>
                        <fmt:message code="admin.appstore.side.bar.new.requests"/>
                    </a>
                </li>
            </ul>
        </li>

        <li>
            <a href="panels">
                <i class="icon-th"></i>
                <span class="menu-text"> <fmt:message code="admin.appstore.side.bar.panel.selection"/></span>
            </a>
        </li>

        <li>
            <a href="advertisements">
                <i class="icon-bullhorn"></i>
                <span class="menu-text"> <fmt:message code="admin.appstore.side.bar.advertisements"/></span>
            </a>
        </li>
    </ul>
    <!--/.nav-list-->

    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>
</div>