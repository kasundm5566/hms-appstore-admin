var config = {
    "discovery-api-base-url": "https://vdfapps.hsenidmobile.com/discovery-api/v2/",
    "app-image-base-url": "https://vdfapps.hsenidmobile.com/appstore-admin/",
    "app.name.max.size":12,
    "category.name.max.size":25,
    "app.name.max.size.category.apps.add":12,
    "app.name.max.size.category.add.apps.search":15,
    "category.name.max.size.category.add":12,
    "category.name.max.size.category.add.apps.search":15,
    "discovery-api-featured-app-url": "https://vdfapps.hsenidmobile.com/discovery-api/v2/featured-apps/start/0/limit/8",
    "live-search-limit":20
};