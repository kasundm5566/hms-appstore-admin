<div class="navbar" id="navbar">
    <script type="text/javascript">
        try {
            ace.settings.check('navbar', 'fixed')
        } catch (e) {
        }
    </script>
    <div class="navbar-inner">
        <div class="container-fluid">
            <a href='<fmt:message code="admin.appstore.base.url"/>' class="brand<%-- hidden-480--%>">
                <small>
                    <fmt:message code="admin.appstore.header.bar.page.heading"/>
                </small>
            </a><!--/.brand-->

            <ul class="nav ace-nav pull-right">
                <li class="pending-count">
                    <a href="new">
                        <span class="badge badge-custom">
                            <c:if test="${pendingCount < 1}">
                                <i class="icon-bell-alt"></i>
                            </c:if>
                            <c:if test="${pendingCount > 0}">
                                <c:out value="${pendingCount}"/>
                            </c:if>
                        </span>
                    </a>
                </li>

                <li class="vodafone-red">
                    <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                        <i class="icon-sitemap"></i>
                    </a>

                    <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-closer" style="min-width: 140px">
                        <li class="grey">
                            <a href="<fmt:message code='admin.app.navigation.registration.home'/>">
                                <span><fmt:message code="admin.app.navigation.registration"/></span>
                            </a>
                        </li>

                        <li class="grey">
                            <a href="<fmt:message code='admin.app.navigation.provisioning.home'/>">
                                <span><fmt:message code="admin.app.navigation.provisioning"/></span>
                            </a>
                        </li>

                        <li class="grey">
                            <a target="_blank" href="<fmt:message code='admin.app.navigation.appstore.home'/>">
                                <span><fmt:message code="admin.app.navigation.appstore"/></span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="vodafone-red">
                    <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                        <fmt:message code="admin.appstore.admin.welcome.drop.down.title"/> <c:out value='${userId}'/>
                        <i class="icon-caret-down"></i>
                    </a>

                    <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-closer" style="min-width: 140px">
                        <li>
                            <a href='/appstore-admin/logout'>
                                <i class="icon-off"></i>
                                <fmt:message code="admin.appstore.admin.welcome.drop.down.log.out"/>
                            </a>
                        </li>
                    </ul>
                </li>

            </ul>
            <!--/.ace-nav-->
        </div>
        <!--/.container-fluid-->
    </div>
    <!--/.navbar-inner-->
</div>