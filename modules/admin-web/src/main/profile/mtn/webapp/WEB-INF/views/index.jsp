<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmtj" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>

<c:set var="req_url" value="${requestScope['javax.servlet.forward.servlet_path']}"/>
<script type="text/javascript">
    $(document).ready(function () {
        $('.fadeWithTable').fadeIn('slow');

        var getTableRowCount = document.getElementById("AllAppsTable").rows.length;
        if (parseInt(getTableRowCount - 1) == 0) {
            $('#noResultWarning').fadeIn('slow');
        }
    });
</script>


<div class="nav-search" id="nav-search">
    <div class="hidden-phone visible-desktop">

        <form class="form-search" action="search" onsubmit="return validateAppSearch('appSearchDesktop')"
              name="appSearchDesktop" method="POST" accept-charset="UTF-8">

            <%--new search criteria--%>
            <a class="dropdown-toggle" href="#" data-toggle="dropdown">
              <span class="label label-grey"><i class="icon-plus icon-large"></i> <fmt:message code="admin.app.navigation.adv.search"/> </span>
            </a>
            <ul id="extra_search" class="user-menu dropdown-menu dropdown-yellow dropdown-caret dropdown-closer form-horizontal">
                <li>
                    <div class="control-group">
                        <label class="control-label" for="search-type">
                            <fmt:message code="admin.appstore.search.bar.app.sdp.type"/>
                        </label>
                        <div class="controls">
                            <select name="search-type" id="search-type" class="span2 hmsSearchListBox">
                                <option selected="selected" disabled="disabled"></option>
                                <option value="<fmt:message code='search.type.sdp.value'/>"><fmt:message code="search.type.sdp.diaplay"/></option>
                                <option value="<fmt:message code='search.type.soltura.value'/>"><fmt:message code="search.type.soltura.diaplay"/></option>
                            </select>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="control-group">
                        <label class="control-label" for="search-sp">
                            <fmt:message code="admin.appstore.search.bar.app.sp"/>
                        </label>
                        <div class="controls">
                            <input type="text" id="search-sp" name="search-sp" class="input-small nav-search-input"
                                   style="margin-left:0px; margin-right:10px;" value="">
                        </div>
                    </div>
                </li>
                <li>
                    <div class="control-group">
                        <label class="control-label" for="form-field-select-3">
                            <fmt:message code="admin.appstore.search.bar.app.type"/>
                        </label>
                        <div class="controls">
                            <select id="form-field-select-3" class="span-2 hmsSearchListBox" name="search-category">
                                <option selected="selected" disabled="disabled"></option>
                                <c:forEach items="${categories}" var="category">
                                    <option><c:out value="${category}"/></option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </li>
            </ul>
        <%--end new criteria--%>

            <fmt:message code="admin.appstore.search.bar.app.id"/>
            <input type="text" id="search-id" name="search-id" class="input-small nav-search-input"
                   style="margin-left:0px; margin-right:10px;" value="">


            <fmt:message code="admin.appstore.search.bar.app.name"/> <%--App Name label--%>
            <input type="text" id="search-name" name="search-name" class="input-small nav-search-input"
                       style="margin-left:0px; margin-right:10px;" value="">

            <fmt:message code="admin.appstore.search.bar.app.state"/> <%--App State label--%>
            <select id="form-field-select-4" class="span-2 hmsSearchListBox" name="search-status">
                <option selected="selected" disabled="disabled"></option>
                <c:forEach items="${searchStatuses}" var="status">
                    <option><c:out value="${status}"/></option>
                </c:forEach>
            </select>

            <button class="hmsSearchButton">
                <i class="icon-search"></i> <fmt:message code="admin.appstore.search.bar.app.search.button"/>
            </button>
        </form>
    </div>


        <%--Mobile Search--%>
    <div class="hidden-desktop visible-phone">
        <div class="inline position-relative">
            <button class="hmsSearchButton dropdown-toggle" data-toggle="dropdown">
                <i class="icon-search"></i> <i class="icon-caret-down icon-only bigger-120"></i>
            </button>

            <ul class="dropdown-menu dropdown-icon-only dropdown-yellow pull-right dropdown-caret dropdown-close"
                style="padding:10px;">
                <form class="form-search" action="search" onsubmit="return validateAppSearch('appSearchMobile')"
                      name="appSearchMobile" method="POST" accept-charset="UTF-8">

                    <li>
                        <fmt:message code="admin.appstore.search.bar.app.name"/> <%--App Name label--%>
                        <input type="text" id="search-name" name="search-name"
                               class="input-small span12 nav-search-input" value="">
                    </li>

                    <li>
                            <fmt:message code="admin.appstore.search.bar.app.type"/> <%--App Type label--%>
                        <select id="form-field-select-3" class="span-2 hmsSearchListBoxMobile" name="search-category">
                            <option selected="selected" disabled="disabled"></option>
                            <c:forEach items="${categories}" var="category">
                                <option><c:out value="${category}"/></option>
                            </c:forEach>
                        </select>
                    <li>

                    <li>
                            <fmt:message code="admin.appstore.search.bar.app.state"/> <%--App State label--%>
                        <select id="form-field-select-4" class="span-2 hmsSearchListBoxMobile" name="search-status">
                            <option selected="selected" disabled="disabled"></option>
                            <c:forEach items="${searchStatuses}" var="status">
                                <option><c:out value="${status}"/></option>
                            </c:forEach>
                        </select>
                    <li>

                        <button class="hmsSearchButton" style="margin-top:5px;">
                            <i class="icon-search"></i> <fmt:message code="admin.appstore.search.bar.app.search.button"/>
                        </button>

                </form>
            </ul>
        </div>
    </div>
        <%--Mobile Search--%>

</div>

<!--#nav-search-->
</div>   <%--***don't remove this !**important**! end of bread crumb tag--%>

<div class="page-content">
<div class="row-fluid">
<div class="span12">

    <%--TABLE--%>
<div class="row-fluid">
<div class="table-header">

    <c:set var="isNewAppPage" value="false"/>
    <c:forEach items="${searchStatuses}" var="status">
        <c:if test="${status == 'New'}">
            <c:set var="isNewAppPage" value="true"/>
        </c:if>
    </c:forEach>

    <c:if test="${searchKw == null}">
        <c:if test="${isNewAppPage == false}">
            Showing all apps
        </c:if>

        <c:if test="${isNewAppPage == true}">
            Showing all apps
        </c:if>
    </c:if>

    <c:if test="${searchKw != null ||searchFilterCat != null || searchFilterStatus != null }">
        <fmt:message code="admin.appstore.table.header.text"/> "<b><c:out
            value="${searchKw}"></c:out></b>" filtered by category as "<b><c:out
            value="${searchFilterCat}"></c:out></b>" and state as "<b><c:out value="${searchFilterStatus}"></c:out></b>"
    </c:if>
</div>

<div class="table-responsive">
    <table id="AllAppsTable" class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th><fmt:message code="admin.app.details.table.app.name.col.id"/></th>

            <th><fmt:message code="admin.app.details.table.app.name.col.name"/></th>
                <%--app table name column header--%>
            <th><fmt:message code="admin.app.details.table.app.type.col.name"/></th>
                <%--app table type column header--%>
            <th class="hidden-480">
                <c:if test="${fn:contains(req_url, 'new')==true}">
                    <fmt:message code="admin.app.details.table.app.req.date.col.name"/>
                </c:if>
                <c:if test="${fn:contains(req_url, 'new')==false}">
                    <fmt:message code="admin.app.details.table.app.pub.date.col.name"/>
                </c:if>
            </th>
                <%--app table date column header--%>
            <th class="hidden-480"><fmt:message code="admin.app.details.table.app.sp.col.name"/></th>
                <%--app table developer column header--%>
            <th><fmt:message code="admin.app.details.table.app.state.col.name"/></th>
                <%--app table state column header--%>
            <th><fmt:message code="admin.app.details.table.app.action.col.name"/></th>
                <%--app table action column header--%>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${appList}" var="app">
            <tr class="fadeWithTable" style="display: none;" class='hidden' id='<c:out value="${app.id}"/>Row'>
                <td>
                    <c:out value="${app.id}"/>
                </td>
                <td>
                    <c:out value="${app.name}"/>
                </td>
                <td>
                    <c:out value="${app.category}"/>
                </td>
                <td class="hidden-480">
                    <fmtj:formatDate value="${app.requestedDate}" pattern="yyyy-MM-dd hh:mm a"/>
                </td>
                <td class="hidden-480">
                    <c:out value="${app.developer}"/>
                </td>
                <td>

            <span id='<c:out value="${app.id}"/>AppStateLabel'
                  class="label <c:if test="${app.status=='Unpublish'}">label-warning </c:if>
                                              <c:if test="${app.status=='Publish'}">label-success </c:if>">
                <c:if test="${app.status != 'New'}">
                    <c:out value="${app.status}"/>ed
                </c:if>
                <c:if test="${app.status == 'New'}">
                    <c:out value="${app.status}"/>
                </c:if>
            </span>

                </td>
                <td>
                    <div class="hidden-phone visible-desktop action-buttons">
                        <a class="blue tooltip-info" href="appView/?app-id=<c:out value='${app.id}'/>"
                           data-target="#<c:out value='${app.id}'/>view" data-toggle="modal" data-rel="tooltip"
                           title="<fmt:message code='admin.app.details.table.app.action.view.icon.tool.tip'/>">
                            <i class="icon-zoom-in bigger-130"></i>
                        </a>

                        <a class="green tooltip-success" href="appEdit/?app-id=<c:out value='${app.id}'/>"
                           data-target="#<c:out value='${app.id}'/>edit" data-toggle="modal"
                           data-rel="tooltip"
                           title="<fmt:message code='admin.app.details.table.app.action.edit.icon.tool.tip'/>">
                            <i class="icon-pencil bigger-130"></i>
                        </a>

                        <c:if test="${app.status=='Unpublish'}">
                            <a id='<c:out value="${app.id}"/>pubUnpubLink' class="green tooltip-success"
                               href="#<c:out value="${app.id}"/>pub"
                               data-toggle="modal" data-rel="tooltip"
                               title="<fmt:message code='admin.app.details.table.app.action.Publish.icon.tool.tip'/>">
                                <i id='<c:out value="${app.id}"/>pubUnpubIcon' class="icon-link bigger-130"></i>
                            </a>
                        </c:if>

                        <c:if test="${app.status=='Publish'}">
                            <a id='<c:out value="${app.id}"/>pubUnpubLink' class="red tooltip-error"
                               href="#<c:out value="${app.id}"/>upub"
                               data-toggle="modal" data-rel="tooltip"
                               title="<fmt:message code='admin.app.details.table.app.action.Unpublish.icon.tool.tip'/>">
                                <i id='<c:out value="${app.id}"/>pubUnpubIcon' class="icon-unlink bigger-130"></i>
                            </a>
                        </c:if>

                        <c:if test="${app.status=='New'}">
                            <a class="green tooltip-success" id='<c:out value="${app.id}"/>pubUnpubLink' href='#<c:out value='${app.id}'/>approveModal'
                               data-toggle="modal"
                               data-rel="tooltip"
                               title="<fmt:message code='admin.app.details.table.app.action.Approve.icon.tool.tip'/>">
                                <i class="icon-ok-circle bigger-130" id='<c:out value="${app.id}"/>pubUnpubIcon'></i>
                            </a>

                            <a class="red tooltip-error" href='#<c:out value='${app.id}'/>rejectModal'
                               id="<c:out value='${app.id}'/>rejectIcon"
                               data-toggle="modal"
                               data-rel="tooltip"
                               title="<fmt:message code='admin.app.details.table.app.action.Reject.icon.tool.tip'/>">
                                <i class="icon-ban-circle bigger-130"></i>
                            </a>
                        </c:if>

                    </div>

                    <div class="hidden-desktop visible-phone">
                        <div class="inline position-relative">
                            <button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown">
                                <i class="icon-caret-down icon-only bigger-120"></i>
                            </button>
                            <ul class="dropdown-menu dropdown-icon-only dropdown-yellow pull-right dropdown-caret dropdown-close">
                                <li>
                                    <a class="blue tooltip-info" href="appView/?app-id=<c:out value='${app.id}'/>"
                                       data-target="#<c:out value='${app.id}'/>view" data-toggle="modal"
                                       data-rel="tooltip"
                                       title="<fmt:message code='admin.app.details.table.app.action.view.icon.tool.tip'/>">
                                        <i class="icon-zoom-in bigger-120"></i>
                                    </a>
                                </li>
                                <li>
                                    <a class="green tooltip-success" href="appEdit/?app-id=<c:out value='${app.id}'/>"
                                       data-target="#<c:out value='${app.id}'/>edit" data-toggle="modal"
                                       data-rel="tooltip"
                                       title="<fmt:message code='admin.app.details.table.app.action.edit.icon.tool.tip'/>">
                                        <i class="icon-pencil bigger-120"></i>
                                    </a>
                                </li>

                                <c:if test="${app.status=='Unpublish'}">
                                    <a id='<c:out value="${app.id}"/>pubUnpubLink' class="green tooltip-success"
                                       href="#<c:out value="${app.id}"/>pub"
                                       data-toggle="modal" data-rel="tooltip"
                                       title="<fmt:message code='admin.app.details.table.app.action.Publish.icon.tool.tip'/>">
                                        <i id='<c:out value="${app.id}"/>pubUnpubIcon' class="icon-link bigger-120"></i>
                                    </a>
                                </c:if>

                                <c:if test="${app.status=='Publish'}">
                                    <a id='<c:out value="${app.id}"/>pubUnpubLink' class="red tooltip-error"
                                       href="#<c:out value="${app.id}"/>upub"
                                       data-toggle="modal" data-rel="tooltip"
                                       title="<fmt:message code='admin.app.details.table.app.action.Unpublish.icon.tool.tip'/>">
                                        <i id='<c:out value="${app.id}"/>pubUnpubIcon'
                                           class="icon-unlink bigger-120"></i>
                                    </a>
                                </c:if>

                                <c:if test="${app.status=='New'}">
                                    <a class="green tooltip-success" href='#<c:out value='${app.id}'/>approveModal'
                                       data-toggle="modal"
                                       data-rel="tooltip"
                                       title="<fmt:message code='admin.app.details.table.app.action.Approve.icon.tool.tip'/>">
                                        <i class="icon-ok-circle bigger-120"></i>
                                    </a>

                                    <a class="red tooltip-error" href='#<c:out value='${app.id}'/>rejectModal'
                                       id="<c:out value='${app.id}'/>rejectIcon"
                                       data-toggle="modal"
                                       data-rel="tooltip"
                                       title="<fmt:message code='admin.app.details.table.app.action.Reject.icon.tool.tip'/>">
                                        <i class="icon-ban-circle bigger-120"></i>
                                    </a>
                                </c:if>
                            </ul>
                        </div>
                    </div>


                </td>
            </tr>

        </c:forEach>
        </tbody>
    </table>
        <%--TABLE END--%>
</div>

<div id="noResultWarning" style="display: none;" align="center">
    <div class="row-fluid">
        <div class="span12" style="padding: 15px;">
            <div class="span12 alert alert-warning" style="">
                <button type="button" class="close" data-dismiss="alert">
                    <i class="icon-remove"></i>
                </button>
                <fmt:message code="admin.appstore.app.search.no.results.found.message"/>
                <br>
            </div>
        </div>
    </div>
</div>

    <%--pegination--%>
<div align="center">
    <div class="pagination no-margin">
        <c:if test="${fn:length(urlList) gt 1}">
            <ul>
                <c:forEach items="${urlList}" var="url">
                    <c:if test="${activePage == url.index}">
                        <li id="searchResult${url.index+1}" class="active">
                            <a href=<c:out value="${url.path}"/>>${url.index+1}</a>
                        </li>
                    </c:if>
                    <c:if test="${activePage != url.index}">
                        <li id="searchResult${url.index+1}">
                            <a href=<c:out value="${url.path}"/>>${url.index+1}</a>
                        </li>
                    </c:if>
                </c:forEach>
            </ul>
        </c:if>
    </div>
</div>

</div>
    <%--row fluid end--%>
</div>
    <%--span12 end--%>
</div>
    <%--row fluid end--%>
</div>
<%--page content tag end--%>


<%--MODALS BEGINS--%>
<c:forEach items="${appList}" var="app">
<%--remarks for unpublishing windows start--%>
<form action="unpublish" method="POST" class="appstatus" accept-charset="UTF-8">
    <div id="<c:out value="${app.id}"/>upub" class="modal hide fade"
         tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header vdf_modal_header_bgc">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="font-size:20px;">x
            </button>
            <fmt:message code='admin.appstore.app.confirmation.title'/> of unpublishing <b>
            <c:out value="${app.name}"/></b>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <div class="span12">
                    <fmt:message code='admin.appstore.app.unpublishing.confirmation.content'/>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <b><fmt:message code='admin.appstore.app.confirmation.remarks'/></b>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <textarea id="form-field-11" class="remarkstextBox" name="remarks"  required></textarea>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="text" value="${app.id}" class="hidden" name="appId" style="width: 0px;"/>
            <input type="text" value="${app.name}" class="hidden" name="appName" style="width: 0px;"/>
            <button class="btn btn-primary btn-small" style="margin-right:5px;" type="submit">
                <fmt:message code='admin.appstore.app.upub.confirmation.button'/>
            </button>
            <button class="btn btn-default btn-small " data-dismiss="modal" aria-hidden="true" type="reset">
                <fmt:message code="admin.appstore.app.view.form.close.button"/>
            </button>
        </div>
    </div>
</form>
<%--remarks for unpublishing windows end--%>

<%--remarks for publishing windows start--%>
<form action="publish" method="POST" class="appstatus" accept-charset="UTF-8">
    <div id="<c:out value="${app.id}"/>pub" class="modal hide fade"
         tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header vdf_modal_header_bgc">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="font-size:20px;">x
            </button>
            <fmt:message code='admin.appstore.app.confirmation.title'/> of publishing
            <b><c:out value="${app.name}"/></b>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <div class="span12">
                    <fmt:message code='admin.appstore.app.publishing.confirmation.content'/>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <b><fmt:message code='admin.appstore.app.confirmation.remarks'/></b>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <textarea id="form-field-12" class="remarkstextBox" name="remarks"  required></textarea>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="text" value="${app.name}" class="hidden" name="appName" style="width: 0px;"/>
            <input type="text" value="${app.id}" class="hidden" name="appId" style="width: 0px;"/>
            <button class="btn btn-small btn-primary" style="margin-right:5px;" type="submit">
                <fmt:message code='admin.appstore.app.pub.confirmation.button'/>
            </button>
            <button class="btn btn-small btn-default" data-dismiss="modal" aria-hidden="true" type="reset">
                <fmt:message code="admin.appstore.app.view.form.close.button"/>
            </button>
        </div>
    </div>
</form>
<%--remarks for publishing windows end--%>

<%--fix application approve issue with special character in name--%>
<%--remarks for approve windows start--%>
<form action="approve" method="POST" class="approveReject" accept-charset="UTF-8">
    <div id="<c:out value='${app.id}'/>approveModal" class="modal hide fade"
         tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header vdf_modal_header_bgc">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="font-size:20px;">x
            </button>
            <fmt:message code='admin.appstore.app.confirmation.title'/> of approve <b><c:out value="${app.name}"/></b>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <div class="span12">
                    <fmt:message code='admin.appstore.app.approve.confirmation.content'/>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <b><fmt:message code='admin.appstore.app.confirmation.remarks'/></b>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <textarea id="form-field-11" class="remarkstextBox" name="remarks"  required></textarea>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="text" value="${app.id}" class="hidden" name="appId" style="width: 0px;"/>
            <input type="text" value="${app.name}" class="hidden" name="appName" style="width: 0px;"/>
            <button class="btn btn-small btn-primary" style="margin-right:5px;" type="submit">
                <fmt:message code='admin.appstore.app.pub.approve.button'/>
            </button>
            <button class="btn btn-small btn-default" data-dismiss="modal" aria-hidden="true" type="reset">
                <fmt:message code="admin.appstore.app.view.form.close.button"/>
            </button>
        </div>
    </div>
</form>
<%--remarks for approve windows end--%>

<%--fix application reject issue with special character in name--%>
<%--remarks for app reject window start--%>
<form action="reject" method="POST" class="approveReject" accept-charset="UTF-8">
    <div id="<c:out value='${app.id}'/>rejectModal" class="modal hide fade"
         tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header vdf_modal_header_bgc">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="font-size:20px;">x
            </button>
            <fmt:message code='admin.appstore.app.confirmation.title'/> of reject <b><c:out value="${app.name}"/></b>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <div class="span12">
                    <fmt:message code='admin.appstore.app.reject.confirmation.content'/>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <b><fmt:message code='admin.appstore.app.confirmation.remarks'/></b>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <textarea id="form-field-12" class="remarkstextBox" name="remarks"  required></textarea>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="text" value="${app.id}" class="hidden" name="appId" style="width: 0px;"/>
            <input type="text" value="${app.name}" class="hidden" name="appName" style="width: 0px;"/>
            <button class="btn btn-small btn-primary" style="margin-right:5px;" type="submit">
                <fmt:message code='admin.appstore.app.pub.reject.button'/>
            </button>
            <button class="btn btn-small btn-default" data-dismiss="modal" aria-hidden="true" type="reset">
                <fmt:message code="admin.appstore.app.view.form.close.button"/>
            </button>
        </div>
    </div>
</form>
<%--remarks for app reject window end--%>

<%--app view window start--%>
<div id="<c:out value='${app.id}'/>view" style="width:800px; margin-left:-400px;"
     class="modal hide fade floatingmodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">

    <div class="modal-body floatingmodelbody" style="padding-left:35px;">
        <p>Loading Content</p>
    </div>
</div>
<%--app view window end--%>

<%--app edit window start--%>
<div id="<c:out value='${app.id}'/>edit" style="width:800px; margin-left:-400px;"
     class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="row-fluid vdf_modal_header_bgc">
        <div class="span10">
            <div class="modal-header">
                <h5 id="myModalLabel">
                    <fmt:message code="admin.appstore.app.edit.form.edit.word"/>
                    <fmt:message code="admin.appstore.app.edit.form.title.part"/>
                    <fmt:message code="admin.appstore.app.edit.form.of.word"/>
                    <b><c:out value="${app.name}"/></b>
                </h5>
            </div>
        </div>
        <div class="span2" style="padding-top: 15px; padding-right: 15px;">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        </div>
    </div>

    <div class="modal-body" style="padding-left:35px;">
        <p>Loading Content</p>
    </div>

    <div class="modal-footer">
        <button id="modal-form-submit" name="<c:out value="${app.id}"/>editApp"
                class="btn btn-small btn-primary"
                style="margin-right:5px;" type="submit">
            <fmt:message code="admin.appstore.app.edit.form.save.button"/>
        </button>
        <button class="btn btn-small btn-default" data-dismiss="modal" aria-hidden="true" type="reset">
            <fmt:message code="admin.appstore.app.edit.form.close.button"/>
        </button>
    </div>

        <%--END OF MODALS--%>

    <script type="text/javascript">
        var buttonName = "button[name=<c:out value="${app.id}"/>editApp]";
        var formId = '#<c:out value="${app.id}"/>editAppModal';

        $(buttonName).on('click', function (e) {
            // We don't want this to act as a link so cancel the link action
            e.preventDefault();
            $(formId).validate();

            if ($(formId).valid()) {
                $(formId).submit();
            }
        });

        var pageSize = parseInt(<c:out value="${pageSize}"/>);

        var searchKey = '<c:out value="${searchKw}"/>';
        var searchCategory = '<c:out value="${searchFilterCat}"/>';
        var searchStatus = '<c:out value="${searchFilterStatus}" />';
        var searchType = '<c:out value="${searchFilterType}" />';
        var searchId = '<c:out value="${searchFilterId}" />';
        var searchSp= '<c:out value="${searchFilterSp}" />';

        var options = {

            currentPage:<c:out value="${activePage+1}"/>,
            totalPages:<c:out value="${fn:length(urlList)}"/>,
            useBootstrapTooltip: true,
            numberOfPages :10, //change this value to set number of pagination nodes to show (between prev next buttons)

            pageUrl: function (type, page, current) {
                var url = document.baseURI || document.URL;
                if (url.indexOf("search") > -1) {

                    return ("?skip=" + ((page - 1) * pageSize)
                            + "&limit=" + pageSize
                            + ((searchKey.length > 0) ? ("&search-name="+searchKey) : "")
                            + ((searchCategory.length > 0) ? ("&search-category="+searchCategory) : "")
                            + ((searchStatus.length > 0) ? ("&search-status=" + searchStatus) : ""))
                            + ((searchId.length > 0) ? ("&search-id=" + searchId) : "")
                            + ((searchSp.length > 0) ? ("&search-sp=" + searchSp) : "")
                            + ((searchType.length > 0) ? ("&search-type=" + searchType) : "");
                }
                return "?skip=" + ((page - 1) * pageSize) + "&limit=" + pageSize;
            }
        };

        $(function () {
            $(".pagination").bootstrapPaginator(options);
        });
    </script>

</div>
<%--app edit window end--%>
</c:forEach>
<%--MODALS ENDS--%>

<%--end visible content--%>
